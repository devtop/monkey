﻿using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using Monkey.Component;
using Monkey.Kernel;

namespace Monkey.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            M.FreeMode = true;
            M.Init(debug: true);

            Hook.Binding(HookEventPoint.Signin, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.Signin, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.Signined, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.Signup, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.Signuped, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.Logout, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.ModifyUserMeta, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.ModifyUserPassword, (sender, args) => { return true; });
            Hook.Binding(HookEventPoint.RedirectUrl, (sender, args) => { return true; });
        }
    }
}
