﻿String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (m, i) {
        return args[i];
    });
}
    ;
String.prototype.trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
    ;
String.prototype.ltrim = function () {
    return this.replace(/(^\s*)/g, "");
}
    ;
String.prototype.rtrim = function () {
    return this.replace(/(\s*$)/g, "");
}
    ;
String.prototype.startWith = function (str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
}
    ;
String.prototype.endWith = function (str) {
    var reg = new RegExp(str + "$");
    return reg.test(this);
}
    ;

(function ($) {
    $.fn.parseForm = function () {
        var serializeObj = {};
        var array = this.serializeArray();
        var str = this.serialize();
        $(array).each(function () {
            if (serializeObj[this.name]) {
                if ($.isArray(serializeObj[this.name])) {
                    serializeObj[this.name].push(this.value);
                } else {
                    serializeObj[this.name] = [serializeObj[this.name], this.value];
                }
            } else {
                serializeObj[this.name] = this.value;
            }
        });
        return serializeObj;
    }
        ;
    $.cachedScript = function (url, callback) {
        return $.ajax({
            type: 'GET',
            url: url,
            data: undefined,
            success: callback,
            dataType: 'script',
            cache: true
        });
    }
        ;
}
)(jQuery);

var $config = {
    uploadUrl: "/uploadfile?rc=true",
    cookiePrefix: "_m_"
};

var $cookie = function (name, value, key) {
    name = key || $config.cookiePrefix + name;
    if (typeof (value) !== "undefined" && value !== null) {
        return $.cookie(name, JSON.stringify(value));
    }
    var cookieValue = $.cookie(name);
    return eval("(" + cookieValue + ")");
};

var $cookieStartWith = function (prefix, callback) {
    prefix = $config.cookiePrefix + prefix;
    for (var name in $.cookie()) {
        if (name.startWith(prefix)) {
            callback && callback(name, $cookie(null, null, name));
        }
    }
};

var $request = function (url, data, callback, options) {

    if (data) {
        switch (typeof (data)) {
            case "string":
                data += "&__submit=true";
                break;
            case "object":
                data.__submit = true;
                break;
            default:
                break;
        }
    }

    options = options || {};

    options = $.extend({
        type: "post",
        dataType: "json",
        loadingLayer: true,
        loadingDelay: 300,
    }, options);

    return $.ajax({
        url: url,
        data: data,
        type: options.type,
        dataType: options.dataType,
        success: function (result) {

            if (typeof result === "string") {
                try {
                    result = JSON.parse(result);
                } catch (e) {
                    return callback && callback(result);
                }
            }

            var handler = function () {
                try {
                    callback && callback(result.obj || result);
                } catch (e) {
                    console.log(e);
                    $ui.error('' + e);
                }
            };

            if (result.status === 0 || result.code === 0) {

                callback = callback || function (obj) {
                    if (obj) {
                        if (obj.redirect) {
                            window.document.location = result.redirect;
                        }
                        if (obj.reload) {
                            window.document.location.reload();
                        }
                    }
                }
                    ;

                if (result.msg) {
                    $ui.success(result.msg);
                    setTimeout(function () {
                        handler();
                    }, 500);
                } else {
                    handler();
                }

            } else {
                $ui.error(result.msg || result.code);
                handler();
            }
        },
        error: function (e) {
            $ui.loaded();
            options.error || layer.msg("请求异常，请重试", {
                shift: 6
            });
        },
        beforeSend: function () {
            if (options.loadingLayer) {
                $ui.loading(options.loadingDelay);
            }
        },
        complete: function () {
            $ui.loaded();
        }
    });
};

var $upload = function (success, error) {
    var element = document.createElement("input");
    element.type = "file";
    element.name = "file";
    var $element = $(element);
    $element.change(function () {
        $uploadFile(element, success, error);
    });
    $element.click();
};

var $uploadFile = function (element, success, fail, error) {
    var form = document.createElement("form");
    var $parent = $($(element).parent());
    form.appendChild(element);
    $.ajax({
        url: $config.uploadUrl,
        type: "post",
        data: new FormData(form),
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        error: error || function (e) {
            layer.msg("请求异常，请重试", {
                shift: 6
            });
        }
        ,
        success: function (res) {
            if (res.code === 0) {
                success && success(res.obj);
            } else {
                layer.msg(res.msg || res.code, {
                    shift: 6
                });
                fail && fail(res);
            }
        },
        beforeSend: function () {
            $ui.loading();
        },
        complete: function () {
            $parent.append(element);
            $ui.loaded();
        },
    });
};

var $uploadBase64File = function (fname, fcontent, success, error) {
    var index = fcontent.indexOf(",");
    if (index >= 0) {
        fcontent = fcontent.substring(++index);
    }
    $.ajax({
        url: $config.uploadUrl,
        type: "post",
        data: {
            base64: true,
            fname: fname,
            fcontent: fcontent
        },
        dataType: "json",
        cache: false,
        error: error || function (e) {
            layer.msg("请求异常，请重试", {
                shift: 6
            });
        }
        ,
        success: function (res) {
            if (res.code === 0) {
                success && success(res.obj);
            } else {
                layer.msg(res.msg || res.code, {
                    shift: 6
                });
            }
        },
        beforeSend: function () {
            $ui.loading();
        },
        complete: function () {
            $ui.loaded();
        },
    });
};

var $ui = {
    config: {},

    //消息
    msg: function (content) {
        layer.msg(content);
    },

    //成功
    success: function (content) {
        layer.msg(content, {
            icon: 1
        });
    },

    //错误
    error: function (content) {
        layer.msg(content, {
            shift: 6
        });
    },

    //警告
    alert: function (content, close, btn, shade) {
        layer.open({
            title: "",
            content: '<div class="m-t-xs">{0}</div>'.format(content),
            closeBtn: close || true,
            btn: btn || null,
            shade: shade || 0.1,
        });
    },

    //确认
    confirm: function (content, callback) {
        return layer.confirm("确定要删除吗?", callback);
    },

    //开始加载
    loading: function (delay) {
        var self = this;
        self.config.loadingLayerIndex = 0;
        delay = delay || 0;
        if (delay <= 0) {
            if (self.config.loadingLayerIndex === 0) {
                self.config.loadingLayerIndex = layer.load(2);
            }
        } else {
            setTimeout(function () {
                if (self.config.loadingLayerIndex === 0) {
                    self.config.loadingLayerIndex = layer.load(2);
                }
            }, delay);
        }
    },

    //加载完成
    loaded: function () {
        layer.close(this.config.loadingLayerIndex);
        this.config.loadingLayerIndex = -1;
    },

    //输入框
    inputform: function (form, inputs) {

        form = form || {};
        form.title = form.title || "表单";
        form.btn = form.btn || "提交";
        form.id = form.id || "ib_" + new Date().getTime();
        form.html = "";
        inputs = inputs || [{
            name: "输入框1",
            placeholder: "",
            type: "hidden"
        }];
        form.html += '<form style="min-width: 300px;" id="{0}" title="{1}" class="m-t">'.format(form.id, form.title);
        for (var i = 0; i < inputs.length; i++) {
            var field = inputs[i];
            field.type = field.type || "text";
            field.value = field.value || "";
            field.required = field.required || "required";
            field.placeholder = field.placeholder || "请输入{0}".format(field.name);
            form.html += '<div class="form-group"><input type="{0}" name="{1}" value="{4}" class="layui-input m-b" placeholder="{2}" autocomplete="off" {3}></div>'.format(field.type, field.name, field.placeholder, field.required, field.value);
        }
        form.html += '<input type="submit" value="{0}" class="layui-btn">'.format(form.btn);
        form.html += "</form>";
        console.log(form, form.html);

        var layer_index = window.layer.open({
            title: "",
            content: form.html,
            closeBtn: true,
            btn: []
        });

        window.$("#" + form.id).submit(function () {
            var the = window.$(this);
            if (form.callback) {
                return form.callback(the, the.serialize());
            }
            return false;
        });

        return form.id;
    },

};

var $form = {
    request: function (form, callback) {
        var action = form.attr("action");
        var method = form.attr("method");
        var data = form.serialize();
        var handler = callback || function (obj) {
            if (obj) {
                if (obj.redirect) {
                    window.document.location = obj.redirect;
                }
                if (obj.reload) {
                    window.document.location.reload();
                }
            }
        }
            ;
        $request(action, data, handler, {
            type: method
        });
        return false;
    },
    bind: function (name, callback) {
        var self = this;
        name = name || "form[async]";
        $(name).submit(function () {
            try {
                return self.request($(this), callback);
            } catch (e) {
                layer.msg("请求异常，请重试", {
                    shift: 6
                });
                return false;
            }
        });
    },
};

var $event = {
    config: {
        pre: "[mevent={0}]"
    },
    callback: function (element, callback) {
        var args = {};
        args.name = element.attr("mevent");
        args.data = element.attr("mdata");
        args.type = element.attr("mtype");
        args.text = element.text().replace(/(^\s*)|(\s*$)/g, "");
        args.element = element;
        return callback(args);
    },
    click: function (name, callback) {
        var self = this;
        name = this.config.pre.format(name);
        window.$(name).click(function () {
            return self.callback(window.$(this), callback);
        });
    },
    change: function (name, callback) {
        var self = this;
        name = this.config.pre.format(name);
        window.$(name).change(function () {
            return self.callback(window.$(this), callback);
        });
    }
};

var $file = {
    bind: function (callback, setting) {
        $("input[type=file][async]").change(function (event) {
            var $this = $(this);
            var postfix = $this.attr("data-postfix");
            var re = new RegExp("\.(" + postfix + ")$", "gi");
            //console.log(event, event.target, re);
            if (!re.test(event.target.value)) {
                $ui.error("文件类型错误，请重新选择");
                return false;
            }
            $uploadFile(this, function (result) {
                var $element = $("#" + result.key);
                if ($element) {
                    $element.val(JSON.stringify(result));
                }
            }, function () {
                event.target.value = null;
            });
        });
    }
};

var $uiLoad = function (srcs, path, callback) {
    srcs = $.isArray(srcs) ? srcs : srcs.split(/\s+/);
    var head = document.getElementsByTagName('head')[0];
    var scripts = [];
    for (var i = 0; i < srcs.length; i++) {
        var src = (path || "") + srcs[i];
        if (src.indexOf('.css') >= 0) {
            var link = document.createElement('link');
            link.href = src;
            link.rel = 'stylesheet';
            link.type = 'text/css';
            head.appendChild(link);
        } else {
            scripts.push(src);
        }
    }
    $loadJS(scripts, function () {
        callback && callback();
    });
};

var $loadJS = function (scripts, done, index) {
    var i = index | 0;
    $.cachedScript(scripts[i], function () {
        i++;
        if (scripts.length === i) {
            done && done();
        } else {
            $loadJS(scripts, done, i);
        }
    });
};
