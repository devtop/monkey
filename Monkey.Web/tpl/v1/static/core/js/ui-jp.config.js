/// <reference path="D:\dev\workspace\c#\src\monkey\Monkey.Web\bower_components/angular/angular.min.js" />
// lazyload config

var jp_config = {
    easyPieChart: [
        '/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'
    ],
    sparkline: [
        '/bower_components/jquery.sparkline/dist/jquery.sparkline.retina.js'
    ],
    flot: [
        '/bower_components/flot/jquery.flot.js',
        '/bower_components/flot/jquery.flot.pie.js',
        '/bower_components/flot/jquery.flot.resize.js',
        '/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js',
        '/bower_components/flot.orderbars/js/jquery.flot.orderBars.js',
        '/bower_components/flot-spline/js/jquery.flot.spline.min.js'
    ],
    moment: [
        '/bower_components/moment/moment.js'
    ],
    screenfull: [
        '/bower_components/screenfull/dist/screenfull.min.js'
    ],
    slimScroll: [
        '/bower_components/slimscroll/jquery.slimscroll.min.js'
    ],
    sortable: [
        '/bower_components/html5sortable/jquery.sortable.js'
    ],
    nestable: [
        '/bower_components/nestable/jquery.nestable.js',
        '/bower_components/nestable/jquery.nestable.css'
    ],
    filestyle: [
        '/bower_components/bootstrap-filestyle/src/bootstrap-filestyle.js'
    ],
    slider: [
        '/bower_components/bootstrap-slider/bootstrap-slider.js',
        '/bower_components/bootstrap-slider/bootstrap-slider.css'
    ],
    chosen: [
        '/bower_components/chosen/chosen.min.js',
        '/bower_components/chosen/chosen.css'
    ],
    TouchSpin: [
        '/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
        '/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'
    ],
    wysiwyg: [
        '/bower_components/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
        '/bower_components/bootstrap-wysiwyg/external/jquery.hotkeys.js'
    ],
    dataTable: [
        '/bower_components/datatables/media/js/jquery.dataTables.min.js',
        '/bower_components/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
        '/bower_components/plugins/integration/bootstrap/3/dataTables.bootstrap.css'
    ],
    vectorMap: [
        '/bower_components/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
        '/bower_components/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
        '/bower_components/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
        '/bower_components/bower-jvectormap/jquery-jvectormap-1.2.2.css'
    ],
    footable: [
        '/bower_components/footable/dist/footable.all.min.js',
        '/bower_components/footable/css/footable.core.css'
    ],
    fullcalendar: [
        '/bower_components/moment/moment.js',
        '/bower_components/fullcalendar/dist/fullcalendar.min.js',
        '/bower_components/fullcalendar/dist/fullcalendar.css',
        '/bower_components/fullcalendar/dist/fullcalendar.theme.css'
    ],
    daterangepicker: [
        '/bower_components/moment/moment.js',
        '/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        '/bower_components/bootstrap-daterangepicker/daterangepicker.css'
    ],
    tagsinput: [
        '/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
        '/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'
    ],
    treeview: [
        '/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js',
        '/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css'
    ],
    treegrid: [
        '/bower_components/jquery-treegrid/js/jquery.treegrid.min.js',
        '/bower_components/jquery-treegrid/css/jquery.treegrid.css',
    ],
    treeselect: [
        '/bower_components/tree-multiselect/dist/jquery.tree-multiselect.js',
        '/bower_components/tree-multiselect/dist/jquery.tree-multiselect.css',
        '/bower_components/tree-select/treeselect.css',
        '/bower_components/tree-select/treeselect.js'
    ],
    treemultiselect: [
        '/bower_components/tree-multiselect/dist/jquery.tree-multiselect.js',
        '/bower_components/tree-multiselect/dist/jquery.tree-multiselect.css',
    ],
    wysiwyg: [
        "/bower_components/froala-wysiwyg-editor/css/froala_editor.pkgd.min.css",
        "/bower_components/froala-wysiwyg-editor/js/froala_editor.pkgd.min.js",
        "/bower_components/froala-wysiwyg-editor/js/languages/zh_cn.js"
    ],
    cropper: [
        "/bower_components/cropper/dist/cropper.min.css",
        "/bower_components/cropper/dist/cropper.min.js",
    ],
    cropperimage: [
        "/bower_components/cropper/dist/cropper.min.css",
        "/bower_components/cropper/dist/cropper.min.js",
        "/bower_components/cropper-image/cropper-image.css",
        "/bower_components/cropper-image/cropper-image.js",
    ],
    uploadmultiplefile: [
        "/bower_components/upload-multiplefile/upload-multiplefile.css",
        "/bower_components/upload-multiplefile/upload-multiplefile.js",
    ],
    angular: [
         "/bower_components/angular/angular.min.js",
    ]
};
