﻿
app.datatable = {}

app.controller("datatable", function ($scope) {

    var self = this;

    $scope.hidden = {
        "args": "",
        "export": false,
        "retrieve": true,
        "page": 1,
    };

    $scope.page = function (current) {
        $scope.hidden.page = current;
        $scope.$applyAsync(function () {
            setTimeout(function () {
                $scope.reload();
            },
                10);
        });
        return false;
    };

    $scope.eventclick = function (event) {
        var $target = $(event.target);
        if (!$target.attr("data-event")) {
            $target = $target.parent();
        }
        var dataids = $target.attr("data-id") || "";
        var name = $target.attr("data-event");
        var type = $target.attr("data-type");
        var minmax = $target.attr("data-minmax");
        var width = $target.attr("data-width");
        var height = $target.attr("data-height");
        var title = $target.attr("title") || $target.text();
        title = title.replace(/(^\s*)|(\s*$)/g, "");
        var $tfmethod = self.$tempform.find('input[name=method]');
        var $tfids = self.$tempform.find('input[name=ids]');
        var $fids = self.$form.find('[type="checkbox"][name="__ids"]');

        if (!dataids) {
            $fids.each(function () {
                var $this = $(this);
                if ($this.prop("checked") && $this.val()) {
                    dataids += $this.val() + ",";
                }
            });
        }

        $tfids.val(dataids);
        $tfmethod.val(name);

        if (name === "retrieve") {
            $scope.hidden.page = null;
            $scope.hidden.export = null;
            $scope.hidden.retrieve = true;
            $scope.$applyAsync(function () {
                setTimeout(function () { self.$form.submit(); }, 10);
            });
            return false;
        }

        if (name === "export") {
            $scope.hidden.retrieve = null;
            $scope.hidden.export = true;
            $scope.$applyAsync(function () {
                setTimeout(function () { self.$form.submit(); }, 10);
            });
            return false;
        }

        if (type === "todo") {
            layer.confirm('确定要' + title + '吗?', function (index) {
                self.$tempform.submit();
                layer.close(index);
            });
        }

        if (type === "popup") {
            var data = self.$tempform.serialize();
            var url = document.location.pathname + "/" + name;
            layer.open({
                title: title,
                type: 2,
                maxmin: minmax ? true : false,
                content: url + '?' + app.datatable.popupArgs + '&' + data,
                area: [width, height],
                end: function () {
                    //console.log(this);
                }
            });
        }

        return true;
    }

    $scope.fulltext = function (event) {
        layer.alert($(event.target).text(), { title: null, btn: null, area: ['600px', 'auto'] });
    }

    $scope.sortable = function (callback) {
        var sortfields = $(".dataTable>thead>tr>th[data-sort=true]");
        sortfields.click(function () {
            var $this = $(this);
            var name = $this.attr("data-name");
            var sort = "asc";
            if ($this.hasClass("sorting")) {
                sortfields.removeClass("sorting")
                    .removeClass("sorting_asc")
                    .removeClass("sorting_desc")
                    .addClass("sorting");
                $this.removeClass("sorting").addClass("sorting_asc");
                sort = "asc";
            } else if ($this.hasClass("sorting_asc")) {
                $this.removeClass("sorting_asc").addClass("sorting_desc");
                sort = "desc";
            } else if ($this.hasClass("sorting_desc")) {
                $this.removeClass("sorting_desc").addClass("sorting_asc");
                sort = "asc";
            } else {
                $this.addClass("sorting");
            }
            callback && callback(name, sort);
            $scope.hidden.__sort.val(name + ":" + sort);
            $scope.reload();
        });
        /*sortfields.addClass("sorting");
        for (var i = 0; i < sortfields.length; i++) {
            var sortfield = $(sortfields[i]);
            var sortby = sortfield.attr("data-sortby");
            if (sortby) {
                sortfield.removeClass("sorting").addClass("sorting_" + sortby);
            }
        }*/
    };

    this.$form = $('.datatableform');

    this.$tempform = $('.datatabletempform');

    this.$columnchecked = this.$form.find('.column-checked');

    this.$pagination = this.$form.find(".pagination");


    this.$columnchecked.change(function () {
        var $this = $(this);
        self.$form.find('.column-ids').prop("checked", $this.prop("checked"));
        return true;
    });

    this.$form.submit(function () {
        var $this = $(this);

        if ($scope.hidden.export) {
            return true;
        }
        var method = $this.find('input[name=method]').val();
        var url = document.location.pathname + "/" + method;
        $request(url, $this.serialize(), function (rdata) {
            if (rdata.pagination.pages.length > 1) {
                self.$pagination.removeClass("none");
            } else {
                self.$pagination.addClass("none");
            }
            self.$columnchecked.prop("checked", false);
            $scope.datatable = rdata;
            $scope.$applyAsync(function () {
                setTimeout($scope.sortable, 100);
            });
        }, { loadingDelay: 0 });

        return false;
    });

    this.$tempform.submit(function () {
        var $this = $(this);
        var method = $this.find('input[name=method]').val();
        var url = document.location.pathname + "/" + method;
        $request(url, $this.serialize() + "&" + app.datatable.todoArgs, function (rdata) {
            if (rdata.redirect) {
                window.document.location = rdata.redirect;
            }
            if (rdata.reload) {
                $scope.reload();
            }
        }, { loadingDelay: 0 });
        return false;
    });

    $scope.hidden.__sort = this.$form.find("[name=__sort]");

    $scope.reload = function () {
        $scope.hidden.export = null;
        $scope.hidden.retrieve = true;
        $scope.$applyAsync(function () {
            setTimeout(function () { self.$form.submit(); }, 10);
        });
    };

    app.datatable.reload = $scope.reload;

});