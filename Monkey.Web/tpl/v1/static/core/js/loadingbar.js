﻿// loadingbar
var loadingbar = function (speed) {
    speed = speed || 1000;
    var progress = $(".loadingbar");
    progress.removeClass("done");
    progress.animate({ width: "100%" }, speed, function () {
        progress.addClass("done");
        $('#content').hide();
        $('#content').css("opacity", 1);
        $('#content').fadeIn(500);
    });
}
