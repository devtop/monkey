﻿var cropperimage = function ($element) {
    var cropper = {
        defaultAspectRatio: 16 / 9,
        init: function ($element) {
            this.$container = $element.find('.cropper-image-body');

            this.$avatarUploadHidden = this.$container.find('[type="hidden"]');

            this.$avatarUpload = this.$container.find('.cropper-image-upload');
            this.$avatarUploadBtn = this.$avatarUpload.find('button');
            this.$avatarUploadName = this.$avatarUpload.find('span');
            this.$avatarUploadFile = this.$avatarUpload.find('[type="file"]');

            this.$avatarWrapper = this.$container.find('.cropper-image-wrapper');
            this.$avatarPreview = this.$container.find('.cropper-image-preview');
            this.$avatarCtrl = this.$container.find('.cropper-image-ctrl');
            this.$avatarCtrlSmallArea = this.$container.find('.cropper-image-ctrl-small-area');
            this.$avatarSave = this.$container.find('.cropper-image-save');
            this.$avatarDelete = this.$container.find('.cropper-image-delete');
            this.$avatarBtns = this.$container.find('.cropper-image-btns');
            this.$ratioBtns = this.$container.find('[data-aspect-ratio]');


            var self = this;

            this.$avatarUploadFile.on('change', $.proxy(this.change, this));
            this.$avatarBtns.on('click', $.proxy(this.rotate, this));
            this.$avatarSave.on('click', $.proxy(this.save, this));
            this.$avatarDelete.on('click', $.proxy(this.del, this));
            this.$ratioBtns.on('click', $.proxy(this.aspectRatio, this));
            this.$avatarUploadBtn.click(function () {
                self.$avatarUploadFile.click();
            });
        },
        start: function () {
            if (this.active) {
                this.$img.cropper('replace', this.link);
            } else {
                this.$img = $('<img src="' + this.link + '">');
                this.$avatarWrapper.empty().html(this.$img);
                this.$img.cropper({
                    aspectRatio: this.defaultAspectRatio,
                    /*preview: this.$avatarPreview.selector,*/
                    strict: false,
                });
                this.active = true;
            }

            this.$avatarCtrl.removeClass("hide");
        },

        change: function (element) {
            var filemaxsize = 1024 * 5; //5M
            var target = $(element.target)[0];
            if (target.files[0].size / 1024 > filemaxsize) {
                $ui.error('图片过大，请重新选择!');
                return false;
            }
            if (!target.files[0].type.match(/image.*/)) {
                $ui.error('请选择正确的图片!');
                return false;
            } else {
                var fname = this.$avatarUploadFile.val().match(/[^\\]+\.[^\(]+/i);
                this.$avatarUploadName.html(fname);
            }
            var self = this;
            $uploadFile(element.target, function (rdata) {
                self.file = rdata;
                self.url = rdata.src;
                self.link = rdata.link;
                self.start();
                self.$avatarCtrlSmallArea.removeClass("hide");
                element.target.value = null;
                //console.log(rdata);
            });
        },

        rotate: function (e) {
            if (this.active) {
                var data = $(e.target).data();
                if (data.method) {
                    this.$img.cropper(data.method, data.option);
                }
            }
        },

        save: function () {
            if (this.active) {
                var fname = this.file.name;
                var fcontent = this.$img.cropper("getCroppedCanvas", { maxWidth: 4096, maxHeight: 4096 }).toDataURL("image/jpeg", 0.9);
                var self = this;
                $uploadBase64File(fname, fcontent, function (rdata) {
                    self.active = false;
                    self.$img.attr("src", rdata.link);
                    self.$img.cropper("destroy");
                    self.$avatarUploadHidden.val(JSON.stringify(rdata));
                    self.$avatarCtrlSmallArea.addClass("hide");
                });
            } else {
                $ui.error("请先选择图片");
                return false;
            }

        },

        del: function () {
            this.active = false;
            if (this.$img) {
                this.$avatarUploadName.html(null);
                this.$img.cropper("destroy");
                this.$img.remove();
            }
            this.$avatarUploadHidden.val(null);
            this.$avatarCtrl.addClass("hide");
        },

        aspectRatio: function (event) {

            if (!this.active) {
                $ui.error("请先选择图片");
                return false;
            }

            var $target = $(event.target);
            this.$img.cropper("setAspectRatio", $target.attr("data-aspect-ratio"));
            this.$ratioBtns.removeClass("active");
            $target.addClass("active");
        }
    }
    cropper.init($element);
    return cropper;
}