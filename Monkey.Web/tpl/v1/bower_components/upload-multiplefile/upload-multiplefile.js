﻿

uploadmultiplefile = function ($container) {
    var upload = {
        init: function ($container) {
            //var $container = $(".upload-multiplefile");
            var self = this;
            this.files = [];
            this.$hidden = $container.find("[type=hidden]");
            this.$list = $container.find(".list-group");
            this.$select = $container.find(".upload-select");
            this.$delete = $container.find(".upload-delete");
            this.$select.click(function () {
                $upload(function (file) {
                    self.addfile(file);
                });
            });
        },
        removefile: function ($this) {
            var src = $this.attr("data-src");
            for (var i = 0; i < this.files.length; i++) {
                if (this.files[i].src === src) {
                    this.files.splice(i, 1);
                    break;
                }
            }
            $this.parent().remove();
            this.$hidden.val(JSON.stringify(this.files));
            $request("/deletefile", { src: src });
        },
        addfile: function (file) {
            var self = this;
            var type = "glyphicon glyphicon-file";
            var html = '<div class="list-group-item"><span class="badge upload-delete"><i class="glyphicon glyphicon-remove"></i></span><i class="' + type + '"></i> ' + file.name + '</div>';
            this.files.push(file);
            this.$list.append(html);
            var item = self.$list.find(".list-group-item").last().find('.upload-delete');
            item.attr("data-src", file.src);
            item.click(function () {
                self.removefile($(this));
            });
            this.$hidden.val(JSON.stringify(self.files));
        }
    };
    upload.init($container);
    return upload;
}