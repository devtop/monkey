﻿$.fn.extend({
    treeselect: function (options) {

        var treeContainer = this;

        var switchDrop = function () {
            if (treeContainer.find('.treeselect-drop').offset().left < 0) {
                treeContainer.find('.treeselect-drop').css('left', 0);
                treeContainer.addClass('treeselect-container-active');
            } else {
                treeContainer.find('.treeselect-drop').css('left', -9000);
                treeContainer.removeClass('treeselect-container-active');
            }
        };

        var span = treeContainer.find('.treeselect-single span');
        var hidden = treeContainer.find('.treeselect-single input');
        options = options || {
            sortable: false,
            startCollapsed: true,
            searchable: true,
            hideSidePanel: true,
            onlySelectedSingle: false
        }
        options.internalClick = false;
        options.onChange = options.onChange || function (allSelectedItems, addedItems, removedItems) {
            var tree = multiselect.tree;
            if (options.onlySelectedSingle && addedItems.length > 0) {
                tree.selectedNodes = [];
                tree.selectedKeys = [];
                treeContainer.find('.item :checkbox').prop('checked', false);
                if (addedItems.length === 1) {
                    allSelectedItems = [addedItems[0]];
                    var input = treeContainer.find('.item[data-key=' + addedItems[0].id + '] :checkbox');
                    input.prop('checked', true);
                }
                tree.render(true);
            }
            var selectedText = "";
            var selectedValue = "";
            for (var index in allSelectedItems) {
                selectedValue += allSelectedItems[index].value + ",";
                selectedText += allSelectedItems[index].text + " ";
            }
            if (!selectedText) {
                selectedText = "请选择";
            }
            options.change && options.change(selectedValue);
            span.html(selectedText);
            hidden.val(selectedValue);
            hidden.change();
        }
        var multiselect = treeContainer.find('.treeselect-results').treeMultiselect(options)[0];
        multiselect.tree.render(true);

        treeContainer.find('.treeselect-single').click(function () {
            switchDrop();
            return false;
        });

        treeContainer.find('input').click(function () {
            options.internalClick = true;
            return true;
        });

        $('body').click(function () {
            if (!options.internalClick && treeContainer.find('.treeselect-drop').offset().left >= 0) {
                switchDrop();
            }
            options.internalClick = false;
        });
        if (!span.text().ltrim()) {
            span.html("请选择");
        }
    }
});