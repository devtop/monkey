﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Monkey.Kernel;
using Monkey.Kernel.Data;
using Monkey.Kernel.Quick;
using Monkey.Web.Model;

namespace Monkey.Web.Controllers
{
    public class DemoController : Controller, IControllerFilter
    {
        public void Init()
        {
            Navigation.Lable("视图");
            Navigation.Link("视图", "/demo/curd", "增删查改", "/");
        }

        [ValidateInput(false)]
        [Description("测试,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        public ActionResult CURD(string method)
        {
            var curd = new CURD(Request, Response, method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "test", sort: "ORDER BY `id` ASC")
            {
                //MustQueryWhere = $"WHERE `appid` = '{appid}'",
            };

            var select = ":请选择,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细";
            var check = "retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细";

            curd.AddField("id", "编号", required: true, width: 100);
            curd.AddField("CheckBox", "CheckBox", FieldType.CheckBox, required: false, config: check);
            var treeChosenField = curd.AddField("ChosenTreeView", "ChosenTreeView", FieldType.TreeSelect, required: false);
            treeChosenField.StringStringDL = curd.Quick.GetTreeSelect("test_tree");
            treeChosenField.Init();

            curd.AddField("Date", "Date", FieldType.Date, required: false);
            curd.AddField("DateTime", "DateTime", FieldType.DateTime, required: false);
            curd.AddField("Decimal", "Decimal", FieldType.Decimal, required: false);
            curd.AddField("FroalaWYSIWYG", "FroalaWYSIWYG", FieldType.WYSIWYG, required: false);
            curd.AddField("Hidden", "Hidden", FieldType.Hidden, required: false);
            curd.AddField("Image", "Image", FieldType.Image, required: false);
            curd.AddField("File", "File", FieldType.File, required: false);
            curd.AddField("MultiFile", "FileAny", FieldType.FileAny, required: false);
            curd.AddField("Number", "Number", FieldType.Number, required: false);
            curd.AddField("Password", "Password", FieldType.Password, required: false);
            curd.AddField("Radio", "Radio", FieldType.Radio, required: false, config: check);
            curd.AddField("Select", "Select", FieldType.Select, required: false, config: select);
            curd.AddField("MultiChosen", "SelectAny", FieldType.SelectAny, required: false, config: check);
            curd.AddField("Spinner", "Spinner", FieldType.Spinner, required: false);
            curd.AddField("Switch", "Switch", FieldType.Switch, required: false, config: check);
            curd.AddField("Tags", "Tags", FieldType.Tags, required: false);
            curd.AddField("Text", "Text", FieldType.Text, required: false);
            curd.AddField("Textarea", "Textarea", FieldType.Textarea, required: false);
            curd.AddField("Toff", "Toff", FieldType.Toff, required: false);
            curd.AddField("Linkage1", "Linkage1", FieldType.Select, required: true, config: curd.Quick.GetSelectStringData("test_tree", appendSql: "AND parentid = 0")).SetValueBefore = (sender, args) =>
            {
                sender.ValidationValue = false;
                sender.RetrieveValue = false;
                sender.LinkageValue = (field, valueArgs) =>
                {
                    var items = new List<LinkageValueItem>();
                    if (string.IsNullOrEmpty(valueArgs.Value))
                    {
                        items = field.StringStringD.Select(m => new LinkageValueItem { Text = m.Value, Value = m.Key }).ToList();
                    }
                    else
                    {
                        items = curd.Quick.GetSelectLinkageItems("test_tree", appendSql: $"AND parentid = {valueArgs.Value}");
                    }
                    return new List<LinkageValueResult>() {
                        new LinkageValueResult
                        {
                            Name = "Linkage2",
                            Items = items
                        }};
                };
            };
            curd.AddField("Linkage2", "Linkage2", FieldType.Select, required: true, config: ":请选择").SetValueBefore = (sender, args) =>
            {
                sender.ValidationValue = false;
                sender.RetrieveValue = false;
                sender.LinkageValue = (field, valueArgs) =>
                {
                    var items = new List<LinkageValueItem>();
                    if (string.IsNullOrEmpty(valueArgs.Value))
                    {
                        items = field.StringStringD.Select(m => new LinkageValueItem { Text = m.Value, Value = m.Key }).ToList();
                    }
                    else
                    {
                        items = curd.Quick.GetSelectLinkageItems("test_tree", appendSql: $"AND parentid = {valueArgs.Value}");
                    }
                    return new List<LinkageValueResult>() {
                        new LinkageValueResult
                        {
                            Name = "Linkage3",
                            Items = items
                        }};

                };
            };
            curd.AddField("Linkage3", "Linkage3", FieldType.Select, required: true, config: ":请选择").SetValueBefore = (sender, args) =>
            {
                sender.ValidationValue = false;
                sender.RetrieveValue = false;
                /*sender.LinkageValue = (field, valueArgs) =>
                {
                    var items = new List<LinkageValueItem>();
                    if (string.IsNullOrEmpty(valueArgs.Value))
                    {
                        items = field.StringStringD.Select(m => new LinkageValueItem { Text = m.Value, Value = m.Key }).ToList();
                    }
                    else
                    {
                        items = curd.Quick.GetSelectLinkageItems("test_tree", sql: $"AND parentid = {valueArgs.Value}");
                    }
                    return new LinkageValueResult
                    {
                        Name = "[name='Linkage3']",
                        Items = items
                    };
                };*/
            };

            //设置检索字段
            curd.Copy("R", "id", required: false);
            curd.Copy("R", "CheckBox");
            curd.Copy("R", "ChosenTreeView");
            curd.Copy("R", "Date");
            curd.Copy("R", "DateTime");
            curd.Copy("R", "Decimal");
            curd.Copy("R", "File");
            curd.Copy("R", "FroalaWYSIWYG");
            curd.Copy("R", "Hidden");
            curd.Copy("R", "Image");
            curd.Copy("R", "MultiChosen");
            curd.Copy("R", "MultiFile");
            curd.Copy("R", "Number");
            curd.Copy("R", "Password");
            curd.Copy("R", "Radio");
            curd.Copy("R", "Select");
            curd.Copy("R", "Spinner");
            curd.Copy("R", "Switch");
            curd.Copy("R", "Tags");
            curd.Copy("R", "Text");
            curd.Copy("R", "Textarea");
            curd.Copy("R", "Toff");


            curd.Copy("F", "CheckBox", required: false);
            curd.Copy("F", "Date", type: FieldType.DateRanges, required: false);
            curd.Copy("F", "DateTime", type: FieldType.DateTimeRanges, required: false);
            curd.Copy("F", "Text", required: false);
            curd.Copy("F", "Tags", required: false);
            curd.Copy("F", "Number", required: false);
            curd.Copy("F", "Linkage1", required: false);
            curd.Copy("F", "Linkage2", required: false);
            curd.Copy("F", "Linkage3", required: false);

            curd.Copy("C", "Linkage1", required: false);
            curd.Copy("C", "Linkage2", required: false);
            curd.Copy("C", "Linkage3", required: false);
            curd.Copy("C", "CheckBox");
            curd.Copy("C", "ChosenTreeView");
            curd.Copy("C", "Date");
            curd.Copy("C", "DateTime");
            curd.Copy("C", "Decimal");
            curd.Copy("C", "File");
            curd.Copy("C", "MultiFile");
            curd.Copy("C", "FroalaWYSIWYG");
            curd.Copy("C", "Image");
            curd.Copy("C", "Number");
            curd.Copy("C", "Password");
            curd.Copy("C", "Radio");
            curd.Copy("C", "Select");
            curd.Copy("C", "MultiChosen");
            curd.Copy("C", "Spinner");
            curd.Copy("C", "Switch");
            curd.Copy("C", "Tags");
            curd.Copy("C", "Text");
            curd.Copy("C", "Textarea");
            curd.Copy("C", "Toff");

            curd.Copy("U", "Linkage1", required: false);
            curd.Copy("U", "Linkage2", required: false);
            curd.Copy("U", "Linkage3", required: false);
            curd.Copy("U", "CheckBox");
            curd.Copy("U", "Date");
            curd.Copy("U", "DateTime");
            curd.Copy("U", "Decimal");
            curd.Copy("U", "File", required: false).NullNotUpdate = true;
            curd.Copy("U", "MultiFile");
            curd.Copy("U", "FroalaWYSIWYG");
            curd.Copy("U", "Image");
            curd.Copy("U", "Number");
            curd.Copy("U", "Password", required: false).NullNotUpdate = true;
            curd.Copy("U", "Radio");
            curd.Copy("U", "Select");
            curd.Copy("U", "MultiChosen");
            curd.Copy("U", "Spinner");
            curd.Copy("U", "Switch");
            curd.Copy("U", "Tags");
            curd.Copy("U", "Text");
            curd.Copy("U", "Textarea");
            curd.Copy("U", "Toff");


            return curd.View(((sender, args) =>
            {
                var rand = new Random();
                sender.RetrieveObj.Stat.Add($"统计金额：{rand.Next()}");
                sender.RetrieveObj.Stat.Add($"统计利润：{rand.Next()}");
                sender.RetrieveObj.Stat.Add($"统计用户：{rand.Next()}");
            }));
        }

        public ActionResult __Test()
        {
            return null;
        }
    }
}