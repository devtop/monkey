﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Sharper.ExtensionMethods;
using Sharper.Http;

namespace Monkey.Test
{
    class Program
    {
        static void Main(string[] args)
        {

            var url = "https://smsapi.vaptcha.com/sms/verifycode";
            var body = JsonConvert.SerializeObject(new
            {
                vid = "5b4307aaa485e5041018d732",
                countrycode = "86",
                label = "asd",
                expiretime = "10",
                phone = "13389600005",
                smskey = "2e3e8e3781ff4e5787ca6a4d58718b40",
                time = $"{DateTime.Now.GetUnixTimestamp(1000)}",
                token = "",
                version = "1.0",
            });
            //var content = new StringContent(body, Encoding.UTF8, "application/json");
            //var response = new HttpClient().PostAsync(url, content);
            //var responseCode2 = response.Result.Content.ReadAsStringAsync();

            var responseCode = HttpClient.Request(url, "POST", body, requestHeader: new Dictionary<string, string>
            {
                {"Content-Type","application/json; charset=utf-8"},
            }).GetHtml().ToInt();

            var ss = 15324449738906;
            var bb = DateTime.Now.GetUnixTimestamp(1000);

            ;

            //Captcha.aa();

            var xml = new XmlDocument();
            xml.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><!DOCTYPE xdsec [    <!ELEMENT methodname ANY>    <!ENTITY xxe SYSTEM \"file:///c:/windows/win.ini\">]><methodcall><methodname>&xxe;</methodname></methodcall>");

            System.Timers.Timer aa = new System.Timers.Timer();
            //aa.Elapsed
            //var s = "ToMainProgramMonkey.Test".UnderlineCase();
            var pr = Process.GetCurrentProcess();

            //var webform = new WebForm(null,null);
            //webform[""].

            PerformanceCounter pp = new PerformanceCounter();
            pp.CategoryName = "Process";
            pp.CounterName = "% Processor Time";
            pp.InstanceName = pr.ProcessName;
            pp.MachineName = ".";
            for (int i = 0; ; i++)
            {
                var info = pr.ProcessName + "内存：" + (Convert.ToInt64(pr.WorkingSet64.ToString()) / 1024).ToString();//得到进程内存
                Console.WriteLine(info + "    CPU使用情况：" + Math.Round(pp.NextValue(), 2).ToString() + "%");
                Thread.Sleep(1000);
            }
        }
    }
}
