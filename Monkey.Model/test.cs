//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Monkey.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class test
    {
        public int id { get; set; }
        public string CheckBox { get; set; }
        public string Chosen { get; set; }
        public string ChosenTreeView { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public Nullable<decimal> Decimal { get; set; }
        public string File { get; set; }
        public string FroalaWYSIWYG { get; set; }
        public string Hidden { get; set; }
        public string Image { get; set; }
        public string MultiChosen { get; set; }
        public string MultiFile { get; set; }
        public Nullable<int> Number { get; set; }
        public string Password { get; set; }
        public string Radio { get; set; }
        public string Select { get; set; }
        public string Spinner { get; set; }
        public string Switch { get; set; }
        public string Tags { get; set; }
        public string Text { get; set; }
        public string Textarea { get; set; }
        public string Toff { get; set; }
        public string Linkage1 { get; set; }
        public string Linkage2 { get; set; }
        public string Linkage3 { get; set; }
    }
}
