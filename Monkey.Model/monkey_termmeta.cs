//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Monkey.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class monkey_termmeta
    {
        public int id { get; set; }
        public int termid { get; set; }
        public string meta_key { get; set; }
        public string meta_value { get; set; }
        public string meta_desc { get; set; }
        public string meta_type { get; set; }
    
        public virtual monkey_term monkey_term { get; set; }
    }
}
