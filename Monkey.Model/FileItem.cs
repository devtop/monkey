﻿namespace Monkey.Model
{
    public class FileItem
    {
        public string key { get; set; }
        public string name { get; set; }
        public string src { get; set; }
        public string link { get; set; }
        public string extension { get; set; }
        public long length { get; set; }

    }
}
