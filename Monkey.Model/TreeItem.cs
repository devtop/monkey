﻿using System.Collections.Generic;

namespace Monkey.Model
{
    public class TreeItem
    {
        public string ParentId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public List<TreeItem> Childs { get; set; }
    }
}
