﻿namespace Monkey.Model
{
    public class KVItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
