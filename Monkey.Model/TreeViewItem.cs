﻿using System.Collections.Generic;

namespace Monkey.Model
{
    public class TreeViewItem
    {
        public string text { get; set; }
        public string href { get; set; }
        public List<TreeViewItem> nodes { get; set; }
        public List<string> tags { get; set; }
        public string color { get; set; }
        public string backColor { get; set; }
        public TreeViewState state { get; set; } = new TreeViewState();
    }
}
