﻿using System.Collections.Generic;
using System.Dynamic;
using System.Web;
using System.Web.Routing;

namespace Monkey.Kernel
{
    public class HookEventArgs
    {
        /// <summary>
        /// 动态参数
        /// </summary>
        public dynamic DP { get; } = new ExpandoObject();

        public string RedirectUrl { get; set; }
        public HttpRequestBase Request { get; set; }
        public RouteData RouteData { get; set; }
        public HttpResponseBase Response { get; set; }
        public Dictionary<string, object> MetaData { get; set; }
    }

}
