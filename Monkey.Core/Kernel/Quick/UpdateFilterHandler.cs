namespace Monkey.Kernel.Quick
{
    public delegate void UpdateFilterHandler(QuickComplete sender, UpdateFilterArgs args);
}