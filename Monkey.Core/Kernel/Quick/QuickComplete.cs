﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Donkey.Data.MySql;
using Monkey.Kernel.Data;
using Monkey.Model;
using Monkey.Utility;
using Newtonsoft.Json;

namespace Monkey.Kernel.Quick
{
    /// <summary>
    /// 快速完成
    /// </summary>
    public class QuickComplete : MySqlClient
    {
        public RetrieveItemFilterHandler RetrieveItemBefore { get; set; }
        public RetrieveFilterHandler RetrieveBefore { get; set; }
        public UpdateFilterHandler UpdateBefore { get; set; }
        public DeleteFilterHandler DeleteBefore { get; set; }
        public CreateFilterHandler CreateBefore { get; set; }

        /// <summary>
        /// 选项
        /// </summary>
        public Dictionary<string, string> Options { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// 必须的查询条件
        /// "WHERE TRUE"
        /// </summary>
        public string MustQueryWhere { get; set; } = "WHERE TRUE";

        /// <summary>
        /// 自动完成
        /// 依赖字段，自动生成sql语句的CURD
        /// </summary>
        public QuickComplete(DbContext db, string table, string primary = "id", string sort = "ORDER BY `id` DESC", string support = "CURDE")
            : base(db.Database.Connection.ConnectionString)
        {
            db.Database.Connection.Close();
            db.Dispose();
            KeepAlive = false;
            Init(table, primary, sort, support);
        }

        /// <summary>
        /// 自动完成
        /// 依赖字段，自动生成sql语句的CURD
        /// </summary>
        public QuickComplete(string connectionString, string table, string primary = "id", string sort = "ORDER BY `id` DESC", string support = "CURDE")
            : base(connectionString)
        {
            Init(table, primary, sort, support);
        }

        /// <summary>
        /// 自动完成
        /// 依赖字段，自动生成sql语句的CURD
        /// </summary>
        public QuickComplete(string connectionString) : base(connectionString)
        {
            KeepAlive = false;
        }

        public QuickComplete(DbContext db) : base(db.Database.Connection.ConnectionString)
        {
            db.Database.Connection.Close();
            db.Dispose();
            KeepAlive = false;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init(string table, string primary = "id", string sort = "ORDER BY `id` DESC", string support = "CURDE")
        {
            //PrimaryKey = primaryKey;
            //TableName = tableName.StartsWith("`") ? tableName : $"`{tableName}`";
            //RetrieveSort = retrieveSort;
            Options["primary"] = primary;
            Options["table"] = table.StartsWith("`") ? table : $"`{table}`";
            Options["sort"] = sort;
            Options["support"] = support.ToUpper();
        }

        /// <summary>
        /// 自动完成检索单条
        /// </summary>
        public Dictionary<string, object> RetrieveSingle(string id)
        {
            var querysql = $"SELECT * FROM {Options["table"]} WHERE `{Options["primary"]}` = '{id}'";
            return QueryOne(querysql);
        }

        /// <summary>
        /// 自动完成检索
        /// </summary>
        public List<Dictionary<string, object>> Retrieve(RetrieveForm sender, RetrieveEventArgs args)
        {
            var retrieveFields = sender.ColumnFields.Where(m => m.RetrieveValue).ToList();
            var queryWhere = MakeRetrieveWhereSql(sender);
            var queryFileds = retrieveFields.Select(m => m.Name).Aggregate("", (x, y) => $"{x},`{y}`").Trim(',');
            var count = GetValue<int>($"SELECT COUNT(0) FROM {Options["table"]} {queryWhere}");
            var sortSql = Options["sort"];
            var sort = retrieveFields.Where(m => !string.IsNullOrEmpty(sender.GetRetrieveSort(m.Name))).Select(m => new
            {
                SortFiled = m.Name,
                SortType = sender.GetRetrieveSort(m.Name),
            }).FirstOrDefault();
            if (sort != null)
            {
                sortSql = $"ORDER BY `{sort.SortFiled}` {sort.SortType.Trim('_').ToUpper()}";
            }
            sender.Pagination.Compute(count);
            var queryData = $"SELECT {queryFileds} FROM {Options["table"]} {queryWhere} {sortSql} LIMIT {sender.Pagination.Skip},{sender.Pagination.Take}";
            RetrieveBefore?.Invoke(this, new RetrieveFilterArgs
            {
                Sql = $"FROM {Options["table"]} {queryWhere}",
                RetrieveObj = sender,
            });
            return Query(queryData);
        }

        /// <summary>
        /// 自动完成导出
        /// </summary>
        public void Export(RetrieveForm sender, ExportEventArgs args)
        {
            var queryWhere = MakeRetrieveWhereSql(sender);
            var queryFileds = sender.ColumnFields.Where(m => m.RetrieveValue).Select(m => m.Name).Aggregate("", (x, y) => $"{x},`{y}`").Trim(',');
            Query($"SELECT {queryFileds} FROM {Options["table"]} {queryWhere}", (item =>
             {
                 args.WriteLine(item);
                 return true;
             }));
        }

        /// <summary>
        /// 自动完成删除
        /// </summary>
        public ActionResult Delete(DeleteForm sender, DeleteEventArgs args)
        {
            var ret = ExecuteNonQuery($"DELETE FROM {Options["table"]} WHERE `{Options["primary"]}` IN ({sender.IDs.Aggregate("", (x, y) => $"{x},{y}").Trim(',')})");
            return ret > 0 ? null : ResultConvert.Respond(1, "删除失败");
        }

        /// <summary>
        /// 自动完成更新
        /// </summary>
        public ActionResult Update(UpdateForm sender, UpdateEventArgs args)
        {
            var data = new Dictionary<string, object>();
            foreach (var field in sender.ColumnFields)
            {
                if (string.IsNullOrEmpty(field.Value) && field.NullNotUpdate)
                {
                   continue;
                }
                if (sender.ValidateField(field.Name) || field.GetValueAfter != null)
                {
                    data[field.Name] = ConvertSqlValue(field);
                }
                else if (!field.NullNotUpdate)
                {
                    data[field.Name] = null;
                }
            }
            data[Options["primary"]] = sender.ID;
            ExecuteUpdate(Options["table"], data, Options["primary"]);
            return null;
        }

        /// <summary>
        /// 自动完成创建
        /// </summary>
        public ActionResult Create(CreateForm sender, CreateEventArgs args)
        {
            var data = new Dictionary<string, object>();
            foreach (var field in sender.ColumnFields)
            {
                if (sender.ValidateField(field.Name) || field.GetValueAfter != null)
                {
                    data[field.Name] = ConvertSqlValue(field);
                }
            }
            ExecuteInsert(Options["table"], data);
            return null;
        }

        private string ConvertSqlValue(Field field)
        {
            if (field.GetValue() == null)
            {
                return null;
            }

            switch (field.Type)
            {
                case FieldType.Date:
                case FieldType.DateTime:
                    return field.GetValue().ToString("yyyy-MM-dd HH:mm:ss");

                case FieldType.File:
                case FieldType.FileAny:
                case FieldType.Image:
                    return ConvertUnsafeChar(JsonConvert.SerializeObject(field.GetValue()));

                default:
                    return ConvertUnsafeChar(field.GetValue().ToString());
            }
        }

        private string MakeRetrieveWhereSql(RetrieveForm sender)
        {
            var queryWhere = MustQueryWhere;
            foreach (var field in sender.FilterFields.Where(m => m.RetrieveValue))
            {
                if (sender.FilterValidate(field.Name) || field.GetValueAfter != null)
                {
                    var tsql = field.RetrieveSql;
                    switch (field.Type)
                    {
                        case FieldType.DateRanges:
                            var dateRanges = field.GetValue();
                            tsql = $"AND (`{field.Name}` >= '{dateRanges[0]:yyyy-MM-dd} 00:00:00' AND `{field.Name}` < '{dateRanges[1]:yyyy-MM-dd} 24:00:00' )";
                            break;
                        case FieldType.DateTimeRanges:
                            var dateTimeRanges = field.GetValue();
                            tsql = $"AND (`{field.Name}` > '{dateTimeRanges[0]:yyyy-MM-dd HH:mm:ss}' AND `{field.Name}` < '{dateTimeRanges[1]:yyyy-MM-dd HH:mm:ss}' )";
                            break;
                        default:
                            tsql = tsql.Replace("{key}", field.Name);
                            tsql = tsql.Replace("{value}", ConvertUnsafeChar($"{field.GetValue()}"));
                            break;
                    }
                    if (RetrieveItemBefore != null)
                    {
                        tsql = RetrieveItemBefore(this, new RetrieveItemFilterArgs
                        {
                            Sql = tsql,
                            field = field
                        });
                    }
                    queryWhere += $" {tsql}";
                }
            }
            return queryWhere;
        }

        public List<LinkageValueItem> GetSelectLinkageItems(
            string table,
            string seed = ":请选择,",
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            int limit = 6666,
            int expire = 0)
        {
            var querySql = $"SELECT `{keyField}` AS `Key`,`{valueField}`  AS `Value` FROM `{table}` WHERE  TRUE  { appendSql} LIMIT {limit}";
            var str = MemCache.Get<string>(querySql, () =>
            {
                return Query<KVItem>(querySql).Aggregate("", (current, treeNode) => current + $"{treeNode.Key}:{treeNode.Value},").TrimEnd(',');
            }, "GetSelectLinkageItems", expire);
            return $"{seed}{str}".ToDict().Select(m => new LinkageValueItem { Text = m.Value, Value = m.Key }).ToList();
        }

        public string GetSelectStringData(
            string tableName,
            string seed = ":请选择,",
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            int limit = 6666,
            int cacheTime = 60)
        {
            var querySql = $"SELECT `{keyField}` AS `Key`,`{valueField}`  AS `Value` FROM `{tableName}` WHERE  TRUE  { appendSql} LIMIT {limit}";
            var str = MemCache.Get<string>(querySql, () =>
            {
                var items = Query<KVItem>(querySql);
                return items.Aggregate("", (current, treeNode) => current + $"{treeNode.Key}:{treeNode.Value},").TrimEnd(',');
            }, "GetSelectList", cacheTime);
            return $"{seed}{str}";
        }

        public string GetSelectTreeData(
            string tableName,
            string seed = ":请选择,",
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            string parentId = "0",
            string parentField = "parentid",
            int level = 0,
            string prechar = "—",
            int cacheTime = 60)
        {
            var parentWhereSql = $"`{parentField}` = ' {parentId}'";
            var parentFieldSql = $"`{parentField}` AS `ParentId`";
            if (string.IsNullOrEmpty(parentField))
            {
                parentWhereSql = "TRUE";
                parentFieldSql = "'' AS `ParentId`";
            }
            var querySql = $"SELECT {parentFieldSql}, `{keyField}` AS `Key`,`{valueField}`  AS `Value` FROM `{tableName}` WHERE {parentWhereSql} { appendSql}";
            return MemCache.Get<string>(querySql, () =>
            {
                var treeData = "";
                foreach (var treeNode in Query<TreeItem>(querySql))
                {
                    var prefix = "";
                    for (var i = 0; i < level; i++)
                    {
                        prefix += prechar;
                    }
                    prefix = string.IsNullOrEmpty(prefix) ? "" : $"{prefix} ";
                    treeData += $"{treeNode.Key}:{prefix}{treeNode.Value},";
                    if (string.IsNullOrEmpty(parentField))
                    {
                        continue;
                    }
                    treeData += GetSelectTreeData(tableName, "", keyField, valueField, appendSql, treeNode.Key, parentField, level + 1, prechar, 0);
                }
                return $"{seed}{treeData}";
            }, "GetSelectTree", cacheTime);
        }

        public List<Dictionary<string, string>> GetTreeSelect(
            string tableName,
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            string parentId = "0",
            string parentField = "parentid",
            string prefix = "",
            int cacheTime = 60,
            int limit = 0,
            int current = 0)
        {
            var querySql = $"SELECT `{parentField}` AS `ParentId`, `{keyField}` AS `Key`,`{valueField}`  AS `Value` FROM `{tableName}` WHERE `{parentField}` = ' {parentId}' { appendSql}";
            return MemCache.Get<List<Dictionary<string, string>>>(querySql, () =>
            {
                var trees = new List<Dictionary<string, string>>();
                foreach (var tree in Query<TreeItem>(querySql))
                {
                    var dict = new Dictionary<string, string>();
                    if (tree.ParentId == "0") prefix = "";
                    var childs = GetTreeSelect(tableName, keyField, valueField, appendSql, tree.Key, parentField, $"{prefix}/{tree.Value}", 0, limit, current + 1);
                    dict["value"] = tree.Key;
                    dict["text"] = tree.Value;
                    dict["path"] = prefix;
                    dict["depth"] = $"{current}";
                    dict["ischild"] = $"{childs.Any()}".ToLower();
                    trees.Add(dict);
                    if (childs.Any())
                    {
                        trees.AddRange(childs);
                    }
                }
                return trees;
            }, "GetTreeSelect", cacheTime);
        }

        public List<TreeItem> GetTreeList(
            string tableName,
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            string parentId = "0",
            string parentField = "parentid",
             int cacheTime = 60)
        {
            var querySql = $"SELECT `{parentField}` AS `ParentId`, `{keyField}` AS `Key`,`{valueField}`  AS `Value` FROM `{tableName}` WHERE `{parentField}` = ' {parentId}' { appendSql}";
            return MemCache.Get<List<TreeItem>>(querySql, () =>
            {
                var treeData = new List<TreeItem>();
                foreach (var treeNode in Query<TreeItem>(querySql))
                {
                    treeData.Add(treeNode);
                    treeNode.Childs = GetTreeList(tableName, keyField, valueField, appendSql, treeNode.Key, parentField, 0);
                    treeNode.Childs = treeNode.Childs.Any() ? treeNode.Childs : null;
                }
                return treeData;
            }, "GetTreeList", cacheTime);
        }


        public List<TreeViewItem> GetTreeViewList(
            string tableName,
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            string parentId = "0",
            string parentField = "parentid",
            string selected = "",
            List<TreeViewItem> trees = null,
            int cacheTime = 60)
        {
            var querySql = $"SELECT `{keyField}` AS `href`,`{valueField}`  AS `text` FROM `{tableName}` WHERE `{parentField}` = ' {parentId}' {appendSql}";
            trees = trees ?? MemCache.Get<List<TreeViewItem>>(querySql, () => Query<TreeViewItem>(querySql), "GetTreeViewList", cacheTime);
            foreach (var tree in trees)
            {
                if (!string.IsNullOrEmpty(selected))
                {
                    tree.state.selected = selected.Split(',').Any(m => m == tree.href);
                    tree.state.expanded = tree.state.selected;
                }
                tree.nodes = GetTreeViewList(tableName, keyField, valueField, appendSql, tree.href, parentField, selected, tree.nodes, cacheTime);
                if (!tree.state.expanded && tree.nodes.Any())
                {
                    tree.state.expanded = tree.nodes.Any(m => m.state.expanded);
                }
                tree.nodes = tree.nodes.Any() ? tree.nodes : null;
            }
            return trees;
        }

        public List<TreeViewItem> GetTreeViewSelect(
            string tableName,
            string keyField = "id",
            string valueField = "name",
            string appendSql = "",
            string selected = "",
            int cacheTime = 60)
        {
            var querySql = $"SELECT `{keyField}` AS `href`,`{valueField}`  AS `text` FROM `{tableName}` WHERE  TRUE  { appendSql}";

            return MemCache.Get<List<TreeViewItem>>(querySql, () =>
            {
                var trees = Query<TreeViewItem>(querySql);
                foreach (var treeViewObject in trees)
                {
                    if (!string.IsNullOrEmpty(selected))
                    {
                        treeViewObject.state.selected = selected.Split(',').Any(m => m == treeViewObject.href);
                    }
                }
                return trees;
            }, "GetTreeViewSelect", cacheTime);

        }

    }
}
