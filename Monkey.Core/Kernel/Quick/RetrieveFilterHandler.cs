namespace Monkey.Kernel.Quick
{
    public delegate void RetrieveFilterHandler(QuickComplete sender, RetrieveFilterArgs args);
}