using Monkey.Kernel.Data;

namespace Monkey.Kernel.Quick
{
    public class DeleteFilterArgs
    {
        public string Sql { get; set; }
        public DeleteForm DeleteObj { get; set; }
        //, string deleteSql, DataDelete deleteObject
    }
}