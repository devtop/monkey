namespace Monkey.Kernel.Quick
{
    public delegate void DeleteFilterHandler(QuickComplete sender, DeleteFilterArgs args);
}