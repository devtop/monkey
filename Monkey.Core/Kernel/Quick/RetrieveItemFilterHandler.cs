namespace Monkey.Kernel.Quick
{
    public delegate string RetrieveItemFilterHandler(QuickComplete sender, RetrieveItemFilterArgs args);
}