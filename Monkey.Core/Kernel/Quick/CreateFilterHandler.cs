namespace Monkey.Kernel.Quick
{
    public delegate void CreateFilterHandler(QuickComplete sender, CreateFilterArgs args);
}