using Monkey.Kernel.Data;

namespace Monkey.Kernel.Quick
{
    public class UpdateFilterArgs
    {
        public string Sql { get; set; }
        public UpdateForm UpdateObj { get; set; }
        //, string updateSql, DataUpdate updateObject
    }
}