﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Monkey.Component;
using Monkey.Model;
using Newtonsoft.Json;

namespace Monkey.Kernel
{
    /// <summary>
    /// 错误日志（Controller发生异常时会执行这里）
    /// </summary>
    public class ExceptionFilterAttribute : HandleErrorAttribute
    {

        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnException(ExceptionContext filterContext)
        {
            var error = filterContext.Exception;
            var request = HttpContext.Current.Request;
            using (var db = new MonkeyEntities())
            {
                var logger = new monkey_access_log();
                logger.message = error.ToString();
                logger.request_url = HttpContext.Current.Request.RawUrl;
                logger.request_data = JsonConvert.SerializeObject(request.Form.AllKeys.Select(m => new { Key = m, Value = request.Form[m] }).ToList());
                logger.request_date = DateTime.Now;
                logger.status = 500;
                db.monkey_access_log.Add(logger);
                db.SaveChanges();
            }

            if (M.DebugMode) return;

            filterContext.ExceptionHandled = true;
            filterContext.Result = new RedirectResult("/b/_error/?s=500&m=" + error.Message);
        }
    }
}