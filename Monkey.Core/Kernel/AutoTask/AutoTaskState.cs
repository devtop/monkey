﻿namespace Monkey.Kernel.AutoTask
{
    public enum AutoTaskState
    {
        WorkDone,
        Started,
        Sleep,
        Error
    }
}