﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Monkey.Kernel.AutoTask
{
    /// <summary>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AutoTaskAttribute : Attribute
    {
        public static readonly Dictionary<AutoTaskAttribute, Timer> AutoTasks = new Dictionary<AutoTaskAttribute, Timer>();
        public string Method { get; set; } = "Start";
        public int Interval { get; set; }
        public int Month { get; set; } = 0;
        public int Day { get; set; } = 0;
        public int Hour { get; set; } = 0;
        public int Minute { get; set; } = 0;
        private AutoTaskState State { get; set; }

        /// <summary>
        /// </summary>
        /// <param name="method"></param>
        /// <param name="instance"></param>
        public void Execute(MethodInfo method, object instance)
        {
            var current = DateTime.Now;
            if (Month != 0 && current.Month != Month || Day != 0 && current.Day != Day || Hour != 0 && current.Hour != Hour || Minute != 0 && current.Minute != Minute)
            {
                State = AutoTaskState.Sleep;
                return;
            }

            if (State == AutoTaskState.Started)
            {
                return;
            }

            if (State == AutoTaskState.WorkDone && Interval == 0)
            {
                return;
            }

            try
            {
                State = AutoTaskState.Started;
                method.Invoke(instance, null);
                State = AutoTaskState.WorkDone;
            }
            catch (Exception)
            {
                State = AutoTaskState.Error;
            }
        }

        /// <summary>
        /// </summary>
        public static void Start(params Assembly[] assemblys)
        {
            foreach (var assembly in assemblys)
            {
                foreach (var t in assembly.ExportedTypes.Where(t => IsDefined(t, typeof(AutoTaskAttribute))))
                {
                    var task = (AutoTaskAttribute)GetCustomAttribute(t, typeof(AutoTaskAttribute));
                    if (task == null) continue;

                    var instance = Activator.CreateInstance(t);
                    var method = t.GetMethod(task.Method);

                    if (method == null) throw new Exception("入口方法错误！Method");
                    task.Interval = task.Interval < 0 ? 1 : task.Interval;
                    AutoTasks[task] = new Timer(state => { task.Execute(method, instance); }, instance, 0, task.Interval * 1000);
                }
            }
        }
    }
}