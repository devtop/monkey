﻿using System;

namespace Monkey.Kernel.AutoTask
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class AutoTasker
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public AutoTasker(string name)
        {
            Name = name;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 启用
        /// </summary>
        public bool Enable { get; set; } = true;

        /// <summary>
        /// 执行间隔（秒），默认30秒
        /// </summary>
        public int Interval { get; set; } = 30;

        /// <summary>
        /// 最大执行时间（毫秒），默认30秒
        /// </summary>
        public long MaxExecuteTime { get; set; } = 30000;


        /// <summary>
        /// 最后执行时间
        /// </summary>
        public DateTime LastExecuteTime { get; set; }

        /// <summary>
        /// 下次执行时间
        /// </summary>
        public DateTime NextExecuteTime { get; set; }


        /// <summary>
        /// 执行次数
        /// </summary>
        public int ExecuteCount { get; set; }

        /// <summary>
        /// 执行时间
        /// </summary>
        public long ExecuteTime { get; set; }

        /// <summary>
        /// 执行错误
        /// </summary>
        public int ExecuteError { get; set; }


        /// <summary>
        /// 执行
        /// </summary>
        public abstract void Execute();

        /// <summary>
        /// 杀死
        /// </summary>
        public abstract void Kill();
    }
}
