﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using Exception = System.Exception;
using ThreadState = System.Threading.ThreadState;

namespace Monkey.Kernel.AutoTask
{
    /// <summary>
    /// 
    /// </summary>
    public class AutoTaskDispatcher
    {

        /// <summary>
        /// 
        /// </summary>
        public static List<AutoTasker> AutoTasks { get; set; } = new List<AutoTasker>();

        /// <summary>
        /// 
        /// </summary>
        public static bool Enabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asms"></param>
        public static void Start(params Assembly[] asms)
        {
            foreach (var asm in asms)
            {
                var types = asm.GetTypes().Where(t => t.IsPublic && t.BaseType == typeof(AutoTasker));
                foreach (var type in types)
                {
                    if (type.FullName != null)
                    {
                        if (!(asm.CreateInstance(type.FullName) is AutoTasker tasker))
                        {
                            continue;
                        }
                        AutoTasks.Add(tasker);
                    }
                }
            }

            Enabled = true;

            new Thread(() =>
            {
                while (Enabled)
                {
                    var taskers = GetNeedRunTaskers();
                    if (!taskers.Any())
                    {
                        Thread.Sleep(1000);
                        continue;
                    }

                    foreach (var tasker in taskers)
                    {
                        var sw = new Stopwatch();
                        sw.Start();
                        try
                        {
                            var exec = new Thread(() => { tasker.Execute(); });
                            exec.Start();
                            while (exec.ThreadState != ThreadState.Stopped)
                            {
                                if (tasker.MaxExecuteTime < sw.ElapsedMilliseconds)
                                {
                                    exec.Abort();
                                }

                                Thread.Sleep(10);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            tasker.ExecuteError++;
                        }
                        finally
                        {
                            tasker.ExecuteCount++;
                            tasker.ExecuteTime += sw.ElapsedMilliseconds;
                            tasker.LastExecuteTime = DateTime.Now;
                            tasker.NextExecuteTime = DateTime.Now.AddSeconds(tasker.Interval);
                        }
                    }
                }
            }).Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<AutoTasker> GetNeedRunTaskers()
        {
            return AutoTasks.Where(tasker => tasker.Enable && tasker.NextExecuteTime <= DateTime.Now).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Stop()
        {
            Enabled = false;
        }
    }
}
