using System.Collections.Generic;
using Monkey.Component;
using Monkey.Kernel.Data;

namespace Monkey.Kernel
{
    public delegate List<Dictionary<string, object>> RetrieveEventHandler(RetrieveForm sender, RetrieveEventArgs args);
}