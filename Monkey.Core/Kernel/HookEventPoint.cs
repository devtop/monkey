﻿namespace Monkey.Kernel
{
    public enum HookEventPoint
    {
        /// <summary>
        /// 登录
        /// </summary>
        Signin,
        /// <summary>
        /// 登录后
        /// </summary>
        Signined,
        /// <summary>
        /// 注册
        /// </summary>
        Signup,
        /// <summary>
        /// 注册后
        /// </summary>
        Signuped,
        /// <summary>
        /// 修改密码
        /// </summary>
        ModifyUserPassword,
        /// <summary>
        /// 修改用户数据
        /// </summary>
        ModifyUserMeta,
        /// <summary>
        /// 登出
        /// </summary>
        Logout,
        /// <summary>
        /// 重定向地址
        /// </summary>
        RedirectUrl,

    }
}