using System.Web.Mvc;
using Monkey.Kernel.Data;

namespace Monkey.Kernel
{
    public delegate ActionResult UpdateEventHandler(UpdateForm sender, UpdateEventArgs args);
}