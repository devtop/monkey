﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Monkey.Component;
using Monkey.Utility;

namespace Monkey.Kernel
{
    /// <summary>
    /// 
    /// </summary>
    public class RouteFilterAttribute : ActionFilterAttribute
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        public override void OnActionExecuting(ActionExecutingContext content)
        {

            var httpContext = content.HttpContext;
            var controller = content.RouteData.Values["controller"];
            var action = content.RouteData.Values["action"];
            var method = content.RouteData.Values["method"];
            var routeUrl = $"/{controller}/{action}";
            var requestUrl = $"{content.HttpContext.Request.GetXUri()}";
            //httpContext.Response.Headers["Access-Control-Allow-Origin"] = "*";

            method = method ?? content.HttpContext.Request["method"];
            routeUrl += method == null ? "" : $"/{method}";
            routeUrl = routeUrl.ToLower();


            var ignoreRequestUrl = M.GetConfig("system", "IgnoreRequestUrl", new[] { "^__[a-zA-Z0-9_\\-]" });

            if (ignoreRequestUrl.Any(pattern => Regex.IsMatch($"{controller}", pattern) || Regex.IsMatch($"{action}", pattern)))
            {
                return;
            }

            var javascriptAccept = httpContext.Request.AcceptTypes == null || httpContext.Request.AcceptTypes.Any(m => m.ToLower() == "text/javascript");
            if (M.RequestAuth(httpContext.Session, "/basic/page") != "success")
            {
                content.Result = new HttpUnauthorizedResult();
                return;
            }
            switch (M.RequestAuth(httpContext.Session, routeUrl))
            {
                case "success":
                    break;
                case "fail":
                    content.Result = javascriptAccept ? ResultConvert.Respond(1001, "权限不足") : new RedirectResult($"/b/forbidden?ref={requestUrl}");
                    break;
                case "signin":
                    content.Result = javascriptAccept ? ResultConvert.Respond(1002, "登录超时", new { redirect = $"/signin?ref={requestUrl}" }) : new RedirectResult($"/signin?ref={requestUrl}");
                    break;
            }
            //content.HttpContext.
        }
    }
}
