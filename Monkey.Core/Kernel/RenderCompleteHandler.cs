using Monkey.Component;

namespace Monkey.Kernel
{
    public delegate void RenderCompleteHandler(CURD sender, RenderCompleteArgs args);
}