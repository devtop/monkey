﻿using System.Collections.Generic;

namespace Monkey.Kernel
{
    public class NavigationItem
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 文本
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 顺序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 子集
        /// </summary>
        public List<NavigationItem> Childs { get; set; } = new List<NavigationItem>();

        /// <summary>
        /// 激活
        /// </summary>
        public bool Active { get; set; }
    }
}
