using System.Collections.Generic;
using Monkey.Component;
using Monkey.Kernel.Data;

namespace Monkey.Kernel
{
    public delegate Dictionary<string, object> DisplayEventHandler(DisplayForm sender, DisplayEvenArgs args);
}