﻿using System.Collections.Generic;
using System.Linq;

namespace Monkey.Kernel
{
    public class Navigation
    {
        /// <summary>
        /// 导航集合
        /// </summary>
        public static List<NavigationItem> Maps { get; set; } = new List<NavigationItem>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="sort"></param>
        public static void Lable(string text, int sort = 0)
        {
            text = text.ToLower();
            var navlab = Maps.FirstOrDefault(m => m.Text == text);
            if (navlab != null) return;
            navlab = new NavigationItem { Text = text, Sort = sort };
            Maps.Add(navlab);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Link(string lable, string url, string text, string baseUrl = "", string icon = "fa fa-circle-o", string path = null, int sort = 0, bool active = false)
        {
            lable = lable.ToLower();
            url = url.ToLower();
            baseUrl = baseUrl.ToLower();
            var navlab = Maps.FirstOrDefault(m => m.Text == lable);
            if (navlab == null) return;
            var addnav = new NavigationItem
            {
                Path = path ?? url,
                Url = url,
                Text = text,
                Icon = icon,
                Sort = sort,
                Active = active,
            };
            var nav = navlab.Childs.FirstOrDefault(m => m.Path == baseUrl);
            if (nav != null)
            {
                nav.Childs.Add(addnav);
                return;
            }
            navlab.Childs.Add(addnav);
        }
    }
}
