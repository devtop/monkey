using System.Web.Mvc;
using Monkey.Component;
using Monkey.Kernel.Data;

namespace Monkey.Kernel
{
    public delegate ActionResult CreateEventHandler(CreateForm sender, CreateEventArgs args);
}