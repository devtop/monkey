using System.Collections.Generic;

namespace Monkey.Kernel
{
    public delegate Dictionary<string, object> RetrieveItemEventHandler(string itemid);
}