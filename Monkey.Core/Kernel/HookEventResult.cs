﻿namespace Monkey.Kernel
{
    public class HookEventResult
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }
    }
}