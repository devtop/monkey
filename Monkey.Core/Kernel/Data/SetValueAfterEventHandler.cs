namespace Monkey.Kernel.Data
{
    public delegate void SetValueAfterEventHandler(Field sender, SetValueAfterEventArgs args);
}