namespace Monkey.Kernel.Data
{
    public class LinkageTreeChosenValueItem : LinkageValueItem
    {

        public string Path { get; set; }
        public string Depth { get; set; }
        public string Ischild { get; set; }
    }
}