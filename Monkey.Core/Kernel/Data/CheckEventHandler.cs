using System.Collections.Generic;
using System.Web.Mvc;

namespace Monkey.Kernel.Data
{
    public delegate ActionResult CheckEventHandler(RetrieveForm sender, List<Field> fields);
}