using System.Collections.Generic;

namespace Monkey.Kernel.Data
{
    public delegate void ExportEventHandler(RetrieveForm sender, ExportEventArgs args);
}