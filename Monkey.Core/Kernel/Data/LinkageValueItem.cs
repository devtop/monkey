namespace Monkey.Kernel.Data
{
    public class LinkageValueItem
    {
        public string Value { get; set; }
        public string Text { get; set; }

    }
}