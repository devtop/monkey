namespace Monkey.Kernel.Data
{
    public class ExportEventArgs
    {
        public ExportWriteLineEventHandler WriteLine { get; set; }
    }
}