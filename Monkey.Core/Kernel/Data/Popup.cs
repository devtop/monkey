﻿using System.Web.Mvc;

namespace Monkey.Kernel.Data
{

    public delegate ActionResult PopupCallbackHandler(Popup popup);

    public class Popup : RetrieveToolBox
    {
        public Popup(string text, PopupCallbackHandler callback)
        {
            this.MultiText = text;
            this.SingleText = text;
            this.Callback = callback;
        }

        public bool Minmax { get; set; } = true;
        public string Title { get; set; } = " ";
        public string Width { get; set; } = "500px";
        public string Height { get; set; } = "500px";
        public PopupCallbackHandler Callback { get; set; }
    }
}
