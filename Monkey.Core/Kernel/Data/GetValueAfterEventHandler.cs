namespace Monkey.Kernel.Data
{
    public delegate void GetValueAfterEventHandler(Field sender, GetValueAfterEventArgs args);
}