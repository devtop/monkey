﻿namespace Monkey.Kernel.Data
{
    public class RetrieveToolBox
    {

        public string ID { get; set; }

        public string[] IDs { get; set; }

        /// <summary>
        /// 必须选择
        /// </summary>
        public bool MustSelect { get; set; } = true;


        /// <summary>
        /// 多选
        /// </summary>
        public bool Multi { get; set; } = true;
        public string MultiText { get; set; }
        public string MultiTitle { get; set; }
        public string MultiIcon { get; set; } = "fa fa-circle-o";
        public string MultiClass { get; set; } = "btn btn-sm btn-default btn-addon";


        /// <summary>
        /// 单选
        /// </summary>
        public bool Single { get; set; }
        public string SingleText { get; set; }
        public string SingleTitle { get; set; }
        public string SingleIcon { get; set; }
        public int SingleWidth { get; set; } = 60;
        public string SingleStyle { get; set; } = "";

    }
}
