using System.Collections.Generic;
using System.Web.Mvc;

namespace Monkey.Kernel.Data
{
    public delegate List<LinkageValueResult> LinkageValueHandler(Field sender, LinkageValueArgs args);
}