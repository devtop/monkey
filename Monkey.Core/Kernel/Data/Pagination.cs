﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Monkey.Kernel.Data
{
    public class Pagination
    {
        /// <summary>
        /// 数据数量
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// 分页数量
        /// </summary>
        public int Display { get; set; } = 2;

        /// <summary>
        /// 当前页码
        /// </summary>
        public int Current { get; set; }

        /// <summary>
        /// 分页数量
        /// </summary>
        public int PageCount { get; set; }

        /// <summary>
        /// 页码总数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 上一页
        /// </summary>
        public int Previous { get; set; }

        /// <summary>
        /// 下一页
        /// </summary>
        public int Next { get; set; }

        /// <summary>
        /// 存在上一页
        /// </summary>
        public bool HasPrevious { get; set; }

        /// <summary>
        /// 存在下一页
        /// </summary>
        public bool HasNext { get; set; }

        /// <summary>
        /// 分页集合
        /// </summary>
        public List<int> Pages { get; set; } = new List<int>();

        /// <summary>
        /// 
        /// </summary>
        public int Skip { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Take { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<T> GetPageData<T>(IQueryable<T> queryable, int current = 0, int pageSize = 0)
        {
            Compute(queryable.Count(), current, pageSize);
            return queryable.Skip(Skip).Take(Take).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Compute(int total, int current = 0, int pageSize = 0)
        {
            this.Total = total;
            this.Current = current == 0 ? this.Current : current;
            this.PageSize = pageSize == 0 ? this.PageSize : pageSize;

            current = this.Current;
            pageSize = this.PageSize;
            Skip = (current - 1) * pageSize;
            Take = pageSize;
            PageCount = (int)(Math.Ceiling((decimal)total / pageSize));
            PageCount = PageCount == 0 ? 1 : PageCount;

            if (current > 1)
            {
                HasPrevious = true;
                Previous = current - 1;
            }

            if (current < PageCount)
            {
                HasNext = true;
                Next = current + 1;
            }


            var numCount = 2 * Display + 1;
            if (PageCount < numCount)
            {
                for (int i = 1; i < PageCount + 1; i++)
                {
                    Pages.Add(i);
                }
            }
            else
            {
                Pages.Add(current);
                for (int i = 1; i < Display + 1; i++)
                {
                    if (current - i <= 0)
                    {
                        Pages.Add((numCount + current) - i);
                    }
                    else
                    {
                        Pages.Add(current - i);
                    }
                    if (current + i <= PageCount)
                    {
                        Pages.Add(current + i);
                    }
                    else
                    {
                        Pages.Add((current + i) - numCount);
                    }
                }
                Pages = Pages.OrderBy(m => m).ToList();
            }


        }
    }

}
