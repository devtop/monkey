namespace Monkey.Kernel.Data
{
    public class LinkageValueArgs
    {
        public string Value { get; set; }
    }
}