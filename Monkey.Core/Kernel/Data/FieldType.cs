﻿namespace Monkey.Kernel.Data
{
    public enum FieldType
    {
        /// <summary>
        /// 文本
        /// </summary>
        Text,
        /// <summary>
        /// 数字
        /// </summary>
        Number,
        /// <summary>
        /// 数字
        /// </summary>
        Decimal,
        /// <summary>
        /// 是和否
        /// </summary>
        Toff,
        /// <summary>
        /// 时间选择器
        /// </summary>
        Date,
        /// <summary>
        /// 时间选择器
        /// </summary>
        DateTime,
        /// <summary>
        /// 时间范围选择器
        /// </summary>
        DateRanges,
        /// <summary>
        /// 时间范围选择器
        /// </summary>
        DateTimeRanges,
        /// <summary>
        /// 富文本编辑器
        /// </summary>
        WYSIWYG,
        /// <summary>
        /// 选择
        /// </summary>
        Select,
        /// <summary>
        /// 
        /// </summary>
        SelectAny,
        /// <summary>
        /// 长文本框
        /// </summary>
        Textarea,
        /// <summary>
        /// 密码框
        /// </summary>
        Password,
        /// <summary>
        /// 文件上传
        /// StringStringD["postfix"]="[jpg|txt|exe]"
        /// </summary>
        File,
        /// <summary>
        /// 文件上传
        /// StringStringD["postfix"]="jpg|txt|exe"
        /// </summary>
        FileAny,
        /// <summary>
        /// 标签输入
        /// </summary>
        Tags,
        /// <summary>
        /// 数字范围选择
        /// config["data-min"]=0
        /// config["data-max"]=100
        /// config["data-step"]=1
        /// config["data-decimals"]=2
        /// config["data-prefix"]=""
        /// </summary>
        Spinner,
        /// <summary>
        /// 切换
        /// </summary>
        Switch,
        /// <summary>
        /// 多选框
        /// </summary>
        CheckBox,
        /// <summary>
        /// 单选框
        /// </summary>
        Radio,
        /// <summary>
        /// 分割线
        /// </summary>
        Line,
        /// <summary>
        /// 图片
        /// StringStringD["postfix"]="[jpg|txt|exe]"
        /// </summary>
        Image,
        /// <summary>
        /// 隐藏字段
        /// </summary>
        Hidden,
        /// <summary>
        /// 选择树
        /// </summary>
        TreeSelect,
        /// <summary>
        /// 分割线
        /// </summary>
        Lable,

    }
}