using System.Collections.Generic;

namespace Monkey.Kernel.Data
{
    public class LinkageValueResult
    {
        public string Name { get; set; }
        public string Type { get; set; } = FieldType.Select.ToString();
        public List<LinkageValueItem> Items { get; set; }
    }
}