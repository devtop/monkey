namespace Monkey.Kernel.Data
{
    public enum RetrieveMode
    {
        View,
        Export,
        Check,
        Body,
        Header
    }
}