﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Monkey.Component;
using Monkey.Model;
using Monkey.Utility;

namespace Monkey.Kernel.Data
{
    public class RetrieveForm : Form
    {
        public RetrieveForm()
        {
            Init();
        }

        public CheckEventHandler Check { get; set; }

        public ExportEventHandler Export { get; set; }

        /// <summary>
        /// 视图风格
        /// </summary>
        public RetrieveStyle Style { get; set; } = RetrieveStyle.DataGrid;

        public string TreeSelectedRootValue { get; set; } = "";

        public string TreeFieldKey { get; set; } = "parentid";

        public List<TreeViewItem> TreeData { get; set; }

        public string GetTreeSelectedRootValue(bool multi = false)
        {
            if (Request[TreeFieldKey] == null) return "";

            if (multi) return Request[TreeFieldKey];

            return Request[TreeFieldKey].Split(',')[0];
        }
        /// <summary>
        /// 
        /// </summary>
        public string DataTableTreeKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DataTableTreeSortKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool DataTableTreeInitialCollapsed { get; set; } = true;
        /// <summary>
        /// 
        /// </summary>
        public string DataTableTreeRootValue { get; set; } = "0";
        /// <summary>
        /// PopupArgs
        /// </summary>
        public string PopupArgs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TodoArgs { get; set; }

        /// <summary>
        /// 按钮图标
        /// </summary>
        public string ButtonIcon { get; set; } = "fa fa-filter";

        /// <summary>
        /// 模式
        /// </summary>
        public RetrieveMode GetCurrentMode()
        {
            if (Convert.ToBoolean(NameValueCollection.GetValue("__export", "false", true)))
            {
                return RetrieveMode.Export;
            }

            if (Convert.ToBoolean(NameValueCollection.GetValue("__body", "false", true)))
            {
                return RetrieveMode.Body;
            }

            if (Convert.ToBoolean(NameValueCollection.GetValue("__header", "false", true)))
            {
                return RetrieveMode.Header;
            }

            return RetrieveMode.View;
        }

        /// <summary>
        /// 分页
        /// </summary>
        public Pagination Pagination { get; set; } = new Pagination();

        /// <summary>
        /// 页脚统计
        /// </summary>
        public List<string> Stat { get; set; } = new List<string>();

        /// <summary>
        /// 检索排序字段名称
        /// </summary>
        public string RetrieveSortField { get; set; }

        /// <summary>
        /// 检索排序类型
        /// </summary>
        public string RetrieveSortType { get; set; }

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            Pagination.Current = Convert.ToInt32(NameValueCollection.GetValue("__page", "1", true));
            Pagination.PageSize = Convert.ToInt32(NameValueCollection.GetValue("__num", M.GetConfig("system", "retrieve_default_page_size", 10).ToString(), true));
            var sort = NameValueCollection.GetValue("__sort", "");
            var sortRegex = new Regex("^(?<name>[a-z0-9-_]+):(?<type>(asc|desc))$", RegexOptions.IgnoreCase);
            var match = sortRegex.Match(sort);
            if (!string.IsNullOrEmpty(sort) && match.Success)
            {
                this.RetrieveSortField = match.Groups["name"].Value;
                this.RetrieveSortType = match.Groups["type"].Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public string GetRetrieveSort(string fieldname)
        {
            return fieldname == RetrieveSortField ? $"_{RetrieveSortType}" : "";
        }

        /// <summary>
        /// 数据列表树排序
        /// </summary>
        public List<Dictionary<string, object>> TreeSort(List<Dictionary<string, object>> dataList, List<Dictionary<string, object>> rows = null, string parentId = null, string parentPath = "", int level = 0, string sortKey = "id")
        {
            rows = rows ?? new List<Dictionary<string, object>>();
            parentId = parentId ?? DataTableTreeRootValue;
            foreach (var row in dataList.Where(m => m[DataTableTreeKey].ToString() == parentId).OrderBy(m => m[sortKey]))
            {
                var primaryKey = row[PrimaryKey].ToString();
                rows.Add(row);
                var count = rows.Count;
                row["__primarykey"] = primaryKey;
                row["__parent"] = $"{parentPath}";
                row["__path"] = $"{parentPath}/{primaryKey}";
                row["__level"] = level;
                TreeSort(dataList, rows, primaryKey, $"{row["__path"]}", level + 1, sortKey);
                row["__count"] = rows.Count - count;
                row["__ischild"] = ((int)row["__count"] > 0);
            }
            return rows;
        }

        public bool FilterValidate()
        {
            return Validate("filter");
        }

        public bool FilterValidate(string key)
        {
            return ValidateField(key, "filter");
        }

        public string GetFilterValidateError()
        {
            return GetValidateError("filter");
        }

        public string GetFilterValidateError(string key)
        {
            return GetValidateFieldError(key, "filter");
        }

        public Field GetFilterField(string key)
        {
            return GetField(key, "filter");
        }

        public dynamic GetFilterData(string key)
        {
            return GetFieldData(key, "filter");
        }

    }

}
