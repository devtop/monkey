﻿using System.Web.Mvc;

namespace Monkey.Kernel.Data
{
    public delegate ActionResult ToDoCallbackHandler(Todo todo);

    public class Todo : RetrieveToolBox
    {

        public Todo(string text, ToDoCallbackHandler callback)
        {
            this.MultiText = text;
            this.SingleText = text;
            this.Callback = callback;
        }
        public ToDoCallbackHandler Callback { get; set; }
    }
}
