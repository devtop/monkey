namespace Monkey.Kernel.Data
{
    public delegate void SetValueBeforeEventHandler(Field sender, SetValueBeforeEventArgs args);
}