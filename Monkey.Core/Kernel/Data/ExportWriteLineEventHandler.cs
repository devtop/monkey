using System.Collections.Generic;

namespace Monkey.Kernel.Data
{
    public delegate void ExportWriteLineEventHandler(Dictionary<string, object> item);
}