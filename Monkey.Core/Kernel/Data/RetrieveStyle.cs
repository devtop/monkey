﻿namespace Monkey.Kernel.Data
{
    public enum RetrieveStyle
    {
        /// <summary>
        /// 数据列表
        /// </summary>
        DataGrid = 1,

        /// <summary>
        /// 分屏数据列表
        /// </summary>
        TreeGridSplitScreen = 2,
    }
}
