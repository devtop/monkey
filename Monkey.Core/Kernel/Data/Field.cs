﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using Monkey.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Monkey.Kernel.Data
{

    [Serializable]
    public class Field
    {
        /// <summary>
        /// 局部定义当前常规单元格的最小宽度（默认：60），一般用于列宽自动分配的情况。其优先级高于基础参数中的 cellMinWidth
        /// </summary>
        public int minWidth { get; set; } = 60;

        /// <summary>
        /// 设定列类型。可选值有：normal（常规列，无需设定）checkbox（复选框列）radio（单选框列，layui 2.4.0 新增）numbers（序号列）space（空列）
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 固定列。可选值有：left（固定在左）、right（固定在右）。一旦设定，对应的列将会被固定在左或右，不随滚动条而滚动。 
        /// 注意：如果是固定在左，该列必须放在表头最前面；如果是固定在右，该列必须放在表头最后面。
        /// </summary>
        public String @fixed { get; set; }

        /// <summary>
        /// 是否初始隐藏列，默认：false。layui 2.4.0 新增
        /// </summary>
        public bool hide { get; set; } = false;

        /// <summary>
        /// 是否开启该列的自动合计功能，默认：false。layui 2.4.0 新增
        /// </summary>
        public bool totalRow { get; set; } = false;

        /// <summary>
        /// 用于显示自定义的合计文本。layui 2.4.0 新增
        /// </summary>
        public string totalRowText { get; set; }

        /// <summary>
        /// 是否允许排序（默认：false）。如果设置 true，则在对应的表头显示排序icon，从而对列开启排序功能。注意：不推荐对值同时存在“数字和普通字符”的列开启排序，因为会进入字典序比对。比如：'贤心' > '2' > '100'，这可能并不是你想要的结果，但字典序排列算法（ASCII码比对）就是如此。
        /// </summary>
        public bool sort { get; set; } = false;

        /// <summary>
        /// 是否禁用拖拽列宽（默认：false）。默认情况下会根据列类型（type）来决定是否禁用，如复选框列，会自动禁用。而其它普通列，默认允许拖拽列宽，当然你也可以设置 true 来禁用该功能。
        /// </summary>
        public bool unresize { get; set; } = false;

        /// <summary>
        /// 单元格编辑类型（默认不开启）目前只支持：text（输入框）
        /// </summary>
        public string edit { get; set; }

        /// <summary>
        /// 自定义单元格点击事件名，以便在 tool 事件中完成对该单元格的业务处理
        /// </summary>
        public string @event { get; set; }

        /// <summary>
        /// 自定义单元格样式。即传入 CSS 样式
        /// </summary>
        public string style { get; set; }

        /// <summary>
        /// 单元格排列方式。可选值有：left（默认）、center（居中）、right（居右）
        /// </summary>
        public string align { get; set; }

        /// <summary>
        /// 单元格所占列数（默认：1）。一般用于多级表头
        /// </summary>
        public int colspan { get; set; } = 1;

        /// <summary>
        /// 单元格所占行数（默认：1）。一般用于多级表头
        /// </summary>
        public int rowspan { get; set; } = 1;

        /// <summary>
        /// 自定义列模板，模板遵循 laytpl 语法。这是一个非常实用的功能，你可借助它实现逻辑处理，以及将原始数据转化成其它格式，如时间戳转化为日期字符等
        /// </summary>
        public String templet { get; set; }

        /// <summary>
        /// 绑定列工具条。设定后，可在每行列中出现一些自定义的操作性按钮
        /// </summary>
        public String toolbar { get; set; }

        public Field()
        {

        }

        public Field(string name,
            string text = "",
            FieldType type = FieldType.Text,
            string value = "",
            bool required = false,
            string pattern = "",
            string placeholder = "",
            string defaultValue = "",
            Dictionary<string, string> config = null,
            int width = 0,
            int height = 76)
        {
            this.Type = type;
            this.Name = name;
            this.Value = value;
            this.DefaultValue = defaultValue;

            this.Text = text;
            this.Width = width;
            this.Height = height;
            this.Placeholder = placeholder;

            this.Required = required;
            this.Pattern = pattern;

            this.StringStringD = config ?? this.StringStringD;

            Init();

        }


        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            switch (Type)
            {
                case FieldType.Number:
                    Pattern = "^\\d+$";
                    break;
                case FieldType.Decimal:
                    Pattern = "^(\\d+)|(\\d+\\.\\d+)$";
                    break;
                case FieldType.Toff:
                    Pattern = "^True|true|False|false$";
                    break;
                case FieldType.Textarea:
                    Height = 70;
                    break;
                case FieldType.WYSIWYG:
                    Height = 300;
                    break;
                case FieldType.Image:
                    Pattern = "^\\{.*?\\}$";
                    StringStringD["allowsuffix"] = "jpeg,jpg,png,gif";
                    break;
                case FieldType.File:
                    StringStringD["allowsuffix"] = "zip,7z,rar,jpeg,jpg,png,gif";
                    Pattern = "^\\{.*?\\}$";
                    break;
                case FieldType.FileAny:
                    StringStringD["allowsuffix"] = "zip,7z,rar,jpeg,jpg,png,gif";
                    Pattern = "^\\[.*?\\]$";
                    break;
                case FieldType.Select:
                case FieldType.Radio:
                case FieldType.Switch:

                    #region StringStringD

                    if (Required && StringStringD.Any())
                    {
                        Value = StringStringD.FirstOrDefault().Key;
                    }

                    #endregion

                    break;
                case FieldType.DateRanges:

                    #region DateRanges

                    Pattern = "^\\d{4}[-/]\\d{1,2}[-/]\\d{1,2} - \\d{4}[-/]\\d{1,2}[-/]\\d{1,2}$";
                    if (DateTimeRange.All(m => m != null))
                    {
                        Value = $"{DateTimeRange[0]:yyyy-MM-dd} - {DateTimeRange[1]:yyyy-MM-dd}";
                    }

                    #endregion

                    break;
                case FieldType.DateTimeRanges:

                    #region DateTimeRanges

                    Pattern = "^\\d{4}[-/]\\d{1,2}[-/]\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} - \\d{4}[-/]\\d{1,2}[-/]\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}$";
                    if (DateTimeRange.All(m => m != null))
                    {
                        Value = $"{DateTimeRange[0]:yyyy-MM-dd HH:mm:ss} - {DateTimeRange[1]:yyyy-MM-dd HH:mm:ss}";
                    }

                    #endregion

                    break;
                case FieldType.Date:

                    #region Date

                    Pattern = "^\\d{4}[-/]\\d{1,2}[-/]\\d{1,2}$";
                    if (DateTime != null)
                    {
                        Value = $"{DateTime?.ToString("yyyy-MM-dd")}";
                    }

                    #endregion

                    break;
                case FieldType.DateTime:

                    #region DateTime

                    Pattern = "^\\d{4}[-/]\\d{1,2}[-/]\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}$";
                    if (DateTime != null)
                    {
                        Value = $"{DateTime?.ToString("yyyy-MM-dd")}";
                    }

                    #endregion

                    break;

                case FieldType.Text:
                    break;

                case FieldType.Hidden:
                    Width = 0;
                    Height = 0;
                    break;

                case FieldType.TreeSelect:

                    #region TreeSelect

                    StringL = Value?.Split(',').Where(s => !string.IsNullOrEmpty(s.Trim())).ToList();
                    foreach (var ssd in StringStringDL)
                    {
                        StringStringD[ssd["value"]] = ssd["text"];
                        ssd["selected"] = StringL != null && StringL.Any(s => s == ssd["value"]) ? "selected" : "";
                    }

                    RetrieveSql = " AND `{key}` IN ({value})";

                    #endregion

                    break;
            }
        }

        /// <summary>
        /// 克隆
        /// </summary>
        public Field Clone()
        {
            var setValueBefore = SetValueBefore;
            var setValueAfter = SetValueAfter;
            var getValueAfter = GetValueAfter;
            var linkageValue = LinkageValue;
            LinkageValue = null;
            SetValueAfter = null;
            SetValueBefore = null;
            GetValueAfter = null;
            try
            {
                using (var ms = new MemoryStream())
                {
                    var bf = new BinaryFormatter();
                    bf.Serialize(ms, this);
                    ms.Seek(0, SeekOrigin.Begin);
                    var cloneField = (Field)bf.Deserialize(ms);
                    cloneField.SetValueBefore = setValueBefore;
                    cloneField.SetValueAfter = setValueAfter;
                    cloneField.GetValueAfter = getValueAfter;
                    cloneField.LinkageValue = linkageValue;
                    return cloneField;
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                LinkageValue = linkageValue;
                SetValueAfter = setValueAfter;
                SetValueBefore = setValueBefore;
                GetValueAfter = getValueAfter;
            }
        }

        /// <summary>
        /// 填充值
        /// </summary>
        public void SetValue(string value = null, string[] values = null)
        {
            Value = (value ?? Value).Trim();
            Error = "";
            Valid = true;

            var beforeArgs = new SetValueBeforeEventArgs();
            SetValueBefore?.Invoke(this, beforeArgs);


            #region 预处理

            switch (Type)
            {
                case FieldType.Toff:
                    Value = string.IsNullOrEmpty(Value) ? "false" : Value;
                    Value = Value.ToLower();
                    break;
            }

            #endregion

            #region 验证


            if (Required && string.IsNullOrEmpty(Value))
            {
                Valid = false;
                Error = "字段不能为空";
                return;
            }

            if (!string.IsNullOrEmpty(Value) && Minimum != 0 && Value.Length < Minimum)
            {
                Valid = false;
                Error = "字段长度过短";
                return;
            }

            if (!string.IsNullOrEmpty(Value) && Maximum != 0 && Value.Length > Maximum)
            {
                Valid = false;
                Error = "字段长度过长";
                return;
            }

            if (!string.IsNullOrEmpty(Value) && !string.IsNullOrEmpty(Pattern))
            {
                if (!Regex.IsMatch(Value, Pattern, RegexOptions.Singleline))
                {
                    Valid = false;
                    Error = "字段格式不正确";
                    return;
                }
            }


            if (string.IsNullOrEmpty(Value)) return;

            #endregion

            #region 填充

            switch (Type)
            {
                case FieldType.Toff:
                    Object = Convert.ToBoolean(Value);
                    return;

                case FieldType.TreeSelect:
                    Value = Value.Trim(',');
                    return;

                case FieldType.DateRanges:
                case FieldType.DateTimeRanges:
                    var tmp = GetValue();
                    DateTimeRange = new[] { (DateTime?)tmp[0], (DateTime?)tmp[1] };
                    return;

                case FieldType.Date:
                    DateTime = GetValue();
                    Value = Convert.ToDateTime(DateTime).ToString("yyyy-MM-dd");
                    return;

                case FieldType.DateTime:
                    DateTime = GetValue();
                    Value = Convert.ToDateTime(DateTime).ToString("yyyy-MM-dd HH:mm:ss");
                    return;

                case FieldType.Select:
                case FieldType.Radio:
                case FieldType.Switch:

                    #region

                    if (ValidationValue && StringStringD.All(m => Value.Split(',').All(v => v != m.Key)))
                    {
                        Value = "";
                        Valid = false;
                        Error = "无效的值";
                    }

                    #endregion

                    break;


                case FieldType.Image:
                case FieldType.File:

                    #region File

                    try
                    {
                        Object = JsonConvert.DeserializeObject(Value);
                        return;
                    }
                    catch (Exception ex)
                    {
                        Value = "";
                        Valid = false;
                        Error = ex.Message;
                        return;
                    }

                #endregion

                case FieldType.FileAny:

                    #region MultiFile

                    ObjectL = new List<object>();
                    try
                    {
                        ObjectL = JsonConvert.DeserializeObject<JArray>(Value).Select(token => (object)token).ToList();
                        Value = JsonConvert.SerializeObject(ObjectL);
                        return;
                    }
                    catch (Exception ex)
                    {
                        Value = "";
                        Valid = false;
                        Error = ex.Message;
                        return;
                    }

                #endregion

                default:
                    return;

            }

            #endregion


            var afterArgs = new SetValueAfterEventArgs();
            SetValueAfter?.Invoke(this, afterArgs);

            GetValueAftered = false;

        }

        /// <summary>
        ///  获取值
        /// </summary>
        /// <returns></returns>
        public dynamic GetValue(bool rawValue = true, string value = "")
        {
            if (!string.IsNullOrEmpty(value))
            {
                SetValue(value);
            }

            //|| string.IsNullOrEmpty(Value)
            if (!Valid) return null;

            var afterArgs = new GetValueAfterEventArgs
            {
                DynamicValue = ConvertValue(rawValue),
                StringValue = Value
            };
            if (!GetValueAftered)
            {
                GetValueAfter?.Invoke(this, afterArgs);
                Value = afterArgs.StringValue;
                GetValueAftered = true;
            }
            /*if (((object)afterArgs.DynamicValue)?.GetType().Name == "String" && string.IsNullOrEmpty(afterArgs.DynamicValue))
            {
                afterArgs.DynamicValue = afterArgs.StringValue;
            }*/
            return afterArgs.DynamicValue;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawValue"></param>
        /// <returns></returns>
        public dynamic ConvertValue(bool rawValue = true)
        {
            switch (Type)
            {
                case FieldType.Number:
                    return Convert.ToInt32(Value);
                case FieldType.Decimal:
                    return Convert.ToDecimal(Value);
                case FieldType.Toff:
                    return Convert.ToBoolean(Value);
                case FieldType.Image:
                case FieldType.File:
                    return (JObject)Object;
                case FieldType.FileAny:
                    return ObjectL;
                case FieldType.Select:
                case FieldType.Radio:
                case FieldType.Switch:
                    return rawValue ? Value : StringStringD.GetValue(Value, Value);
                case FieldType.DateRanges:
                case FieldType.DateTimeRanges:
                    var sc = Value.Split(new[] { " - " }, StringSplitOptions.None);
                    var t1 = Convert.ToDateTime(sc[0].Trim());
                    var t2 = Convert.ToDateTime(sc[1].Trim());
                    return new[] { t1, t2 };
                case FieldType.Date:
                case FieldType.DateTime:
                    return Convert.ToDateTime(Value);
                case FieldType.TreeSelect:
                    return Value.TrimEnd(',');
                default:
                    return Value;
            }
        }

        /// <summary>
        /// 赋值前
        /// </summary>
        public SetValueBeforeEventHandler SetValueBefore { get; set; }

        /// <summary>
        /// 赋值后
        /// </summary>
        public SetValueAfterEventHandler SetValueAfter { get; set; }

        /// <summary>
        /// 取值后
        /// </summary>
        public GetValueAfterEventHandler GetValueAfter { get; set; }

        /// <summary>
        /// 联动值
        /// </summary>
        public LinkageValueHandler LinkageValue { get; set; }

        #region 属性

        /// <summary>
        /// 
        /// </summary>
        public bool GetValueAftered { get; set; }

        /// <summary>
        /// 字段键
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 字段值
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 字段类型
        /// </summary>
        public FieldType Type { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue { get; set; }
        /// <summary>
        /// 验证通过
        /// </summary>
        public bool Valid { get; set; } = true;
        /// <summary>
        /// 验证错误信息
        /// </summary>
        public string Error { get; set; }
        /// <summary>
        /// 验证表达式
        /// </summary>
        public string Pattern { get; set; }
        /// <summary>
        /// 验证必填
        /// </summary>
        public bool Required { get; set; }
        /// <summary>
        /// 验证最小长度
        /// </summary>
        public int Minimum { get; set; }
        /// <summary>
        /// 验证最大长度
        /// </summary>
        public int Maximum { get; set; }
        /// <summary>
        /// 验证值
        /// </summary>
        public bool ValidationValue { get; set; } = true;
        /// <summary>
        /// 显示名称
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 显示宽度
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// 显示高度
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// 显示风格
        /// </summary>
        public string Style { get; set; }

        /// <summary>
        /// 颜色
        /// </summary>
        public Dictionary<string, string> Color { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// 显示信息
        /// </summary>
        public string Placeholder { get; set; }
        /// <summary>
        /// 隐藏值
        /// </summary>
        public bool HideValue { get; set; } = false;
        /// <summary>
        /// 多个值
        /// </summary>
        public bool MultiValue { get; set; } = false;
        /// <summary>
        /// 检索字段
        /// </summary>
        public bool RetrieveValue { get; set; } = true;
        /// <summary>
        /// 数据排序
        /// </summary>
        public bool RetrieveSort { get; set; } = true;
        /// <summary>
        /// SQL条件
        /// "AND `{key}` = '{value}'"
        /// </summary>
        public string RetrieveSql { get; set; } = "AND `{key}` = '{value}'";
        /// <summary>
        /// 空值不更新
        /// </summary>
        public bool NullNotUpdate { get; set; } = false;
        /// <summary>
        /// 显示
        /// </summary>
        public bool Display { get; set; } = true;
        /// <summary>
        /// 数据对象
        /// </summary>
        public DateTime?[] DateTimeRange { get; set; } = new DateTime?[2];
        /// <summary>
        /// 数据对象
        /// </summary>
        public DateTime? DateTime { get; set; }
        /// <summary>
        /// 数据对象
        /// </summary>
        public object Object { get; set; }
        /// <summary>
        /// 数据对象
        /// </summary>
        public List<object> ObjectL { get; set; } = new List<object>();
        public List<int> IntL { get; set; } = new List<int>();
        public List<string> StringL { get; set; } = new List<string>();
        public Dictionary<string, object> StringObjectD { get; set; } = new Dictionary<string, object>();
        public Dictionary<string, string> StringStringD { get; set; } = new Dictionary<string, string>();
        public Dictionary<int, string> IntStringSD { get; set; } = new Dictionary<int, string>();
        public Dictionary<int, int> IntIntD { get; set; } = new Dictionary<int, int>();
        public Dictionary<string, int> StringIntD { get; set; } = new Dictionary<string, int>();
        public List<Dictionary<string, object>> StringObjectDL { get; set; } = new List<Dictionary<string, object>>();
        public List<Dictionary<string, string>> StringStringDL { get; set; } = new List<Dictionary<string, string>>();
        public List<Dictionary<int, string>> IntSDL { get; set; } = new List<Dictionary<int, string>>();
        public List<Dictionary<int, int>> IntIntDL { get; set; } = new List<Dictionary<int, int>>();
        public List<Dictionary<string, int>> StringIntDL { get; set; } = new List<Dictionary<string, int>>();

        #endregion

    }
}
