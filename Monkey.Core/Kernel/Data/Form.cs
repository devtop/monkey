﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Monkey.Utility;
using Newtonsoft.Json.Linq;

namespace Monkey.Kernel.Data
{

    public class Form
    {
        public Form()
        {

        }

        public Field this[string name]
        {
            get { return ColumnFields.FirstOrDefault(m => m.Name == name); }
        }

        public Form(HttpRequestBase request, HttpResponseBase response = null)
        {
            LoadForm(this, request, response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="form"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void LoadForm(Form form, HttpRequestBase request = null, HttpResponseBase response = null)
        {
            form.Request = request ?? form.Request;
            form.Response = response ?? form.Response;

            if (request == null)
            {
                throw new Exception("request not null");
            }

            foreach (var name in request.QueryString.AllKeys.Where(m => !string.IsNullOrEmpty(m)))
            {
                form.NameValueCollection[name] = request.QueryString[name];
                form.NameValuesCollection[name] = request.QueryString.GetValues(name);
            }

            foreach (var name in request.Form.AllKeys.Where(m => !string.IsNullOrEmpty(m)))
            {
                form.NameValueCollection[name] = request.Form[name];
                form.NameValuesCollection[name] = request.Form.GetValues(name);
            }
        }

        public ActionResult GetHookResult()
        {
            var hook = NameValueCollection.GetValue("__hook", "");
            switch (hook.ToLower())
            {
                case "linkagefield":
                    var hookfieldName = NameValueCollection.GetValue("__field_name", "");
                    var hookfieldType = NameValueCollection.GetValue("__field_type", "");
                    var hookfieldValue = NameValueCollection.GetValue("__field_value", "");
                    var fields = new List<Field>();
                    switch (hookfieldType)
                    {
                        case "raw":
                            fields = RawFields;
                            break;
                        case "column":
                            fields = ColumnFields;
                            break;
                        case "filter":
                            fields = FilterFields;
                            break;

                    }
                    var hookfield = fields.FirstOrDefault(field => field.Name == hookfieldName);

                    if (hookfield?.LinkageValue == null)
                    {
                        return ResultConvert.Respond(101, "参数错误");
                    }

                    var linkageResult = hookfield.LinkageValue(hookfield, new LinkageValueArgs
                    {
                        Value = hookfieldValue
                    });
                    //var name = linkageResult[0].Name;
                    //var item = linkageResult[0].Items;
                    //var _name = Regex.Match("[name='parentid']", "\\[name='(.*?)'\\]").Groups[1].Value;
                    //var _namefield = fields.FirstOrDefault(field => field.Name == _name);
                    //var items = "";
                    //foreach (LinkageValueItem valueItem in item)
                    //{
                    //    items += $"{valueItem.Value}:{valueItem.Text}";
                    //}
                    //_namefield.StringStringD = items.ToDict();
                    return ResultConvert.Respond(obj: linkageResult);

            }
            return null;
        }

        #region 属性

        public HttpRequestBase Request { get; set; }

        public HttpResponseBase Response { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public string PrimaryKey { get; set; } = "id";

        /// <summary>
        /// 标题标题
        /// </summary>
        public string PrimaryTitle { get; set; }

        /// <summary>
        /// 主要标题风格
        /// </summary>
        public string PrimaryStyleClass { get; set; } = "col-2";

        /// <summary>
        /// 主要按钮文本
        /// </summary>
        public string PrimaryButtonText { get; set; } = "确定";

        public Dictionary<string, string> NameValueCollection { get; set; } = new Dictionary<string, string>();

        public Dictionary<string, string[]> NameValuesCollection { get; set; } = new Dictionary<string, string[]>();

        public Dictionary<string, string> ColumnNameValueCollection { get; set; } = new Dictionary<string, string>();

        public Dictionary<string, string> FilterNameValueCollection { get; set; } = new Dictionary<string, string>();

        public List<Field> RawFields { get; set; } = new List<Field>();

        public List<Field> ColumnFields { get; set; } = new List<Field>();

        public List<Field> FilterFields { get; set; } = new List<Field>();

        /// <summary>
        /// ToDo
        /// </summary>
        public Dictionary<string, Todo> TodoCollection { get; set; } = new Dictionary<string, Todo>();

        /// <summary>
        /// Popup
        /// </summary>
        public Dictionary<string, Popup> PopupCollection { get; set; } = new Dictionary<string, Popup>();

        #endregion

        #region 方法

        /// <summary>
        /// 判断请求类型
        /// </summary>
        public bool IsHttpMethod(string method = "POST")
        {
            return Request.HttpMethod == method.ToUpper();
        }


        /// <summary>
        /// 添加字段
        /// </summary>
        public Field AddField(
            string name,
            string text = "",
            FieldType type = FieldType.Text,
            string value = "",
            bool required = false,
            string pattern = "",
            string placeholder = "",
            string config = "",
            int? height = null,
            int? width = null,
            List<Field> fields = null)
        {
            var field = new Field(name, type: type);
            field.Text = text;
            field.Type = type;
            field.Required = required;
            field.Pattern = pattern;
            field.Placeholder = placeholder;
            field.Width = width ?? field.Width;
            field.Height = height ?? field.Height;
            field.StringStringD = config.ToDict();
            field.Init();
            field.SetValue(value);

            fields = fields ?? RawFields;
            fields.Add(field);

            return field;
        }

        /// <summary>
        /// 复制字段
        /// </summary>
        public Field CopyField(
            string name,
            string text = null,
            FieldType? type = null,
            string value = null,
            bool? required = null,
            string pattern = null,
            string placeholder = null,
            string config = null,
            int? height = null,
            int? width = null,
            List<Field> fields = null)
        {
            fields = fields ?? ColumnFields;
            var field = RawFields.FirstOrDefault(m => m.Name == name);
            field = field ?? (type != null ? new Field(name, type: (FieldType)type) : new Field(name));
            field = field.Clone();
            field.Type = type ?? field.Type;
            field.Text = text ?? field.Text;
            field.Width = width ?? field.Width;
            field.Height = height ?? field.Height;
            field.Pattern = pattern ?? field.Pattern;
            field.Placeholder = placeholder ?? field.Placeholder;
            field.Required = required ?? field.Required;
            field.Init();

            if (!string.IsNullOrEmpty(value))
            {
                field.SetValue(value);
            }

            if (!string.IsNullOrEmpty(config))
            {
                field.StringStringD = config.ToDict();
            }

            #region 覆盖字段

            if (fields != null)
            {
                var oldfield = fields.FirstOrDefault(m => m.Name == name);
                if (oldfield != null)
                {
                    var index = fields.IndexOf(oldfield);
                    fields.Remove(oldfield);
                    fields.Insert(index, field);
                }
                else
                {
                    fields.Add(field);
                }
            }

            #endregion

            return field;
        }


        /// <summary>
        /// 批量复制字段
        /// </summary>
        public void CopyFields(List<Field> fields, bool? required = null, params string[] names)
        {
            fields = fields ?? ColumnFields;
            foreach (var name in names)
            {
                CopyField(name, required: required, fields: fields);
            }
        }

        /// <summary>
        /// 获取字段
        /// </summary>
        public Field GetField(string key, string type = "column")
        {
            return GetFields(type).FirstOrDefault(m => m.Name == key);
        }

        /// <summary>
        /// 获取字段
        /// </summary>
        public List<Field> GetFields(string type = "column")
        {
            var fields = type == "column" ? ColumnFields : null;
            fields = fields ?? (type == "filter" ? FilterFields : null);
            fields = fields ?? (type == "raw" ? RawFields : null);
            return fields;
        }

        /// <summary>
        /// 验证字段
        /// </summary>
        /// <returns></returns>
        public bool ValidateField(string name, string type = "column", bool notNullOrEmpty = true)
        {
            var field = GetField(name, type);
            if (field == null) return false;
            var value = NameValueCollection.GetValue(field.Name);
            var values = NameValuesCollection.GetValue(field.Name);
            field.SetValue(value, values);
            return notNullOrEmpty ? field.Valid && !string.IsNullOrEmpty(field.Value) : field.Valid;
        }

        /// <summary>
        /// 获取验证字段错误
        /// </summary>
        public string GetValidateFieldError(string s, string type = "column")
        {
            var field = GetField(s, type);
            if (field == null) return "";
            return $"{field.Text} {field.Error}";
        }

        /// <summary>
        ///  获取字段数据
        /// </summary>
        public dynamic GetFieldData(string key, string type = "column")
        {
            return GetField(key, type)?.GetValue();
        }

        /// <summary>
        /// 加载字段数据
        /// </summary>
        public void LoadFieldData(string type = "column")
        {
            List<Field> fields;
            switch (type)
            {
                case "column":
                    fields = ColumnFields;
                    break;
                case "filter":
                    fields = FilterFields;
                    break;
                case "raw":
                    fields = RawFields;
                    break;
                default:
                    throw new Exception("错误的类型");
            }

            foreach (var field in fields)
            {
                var value = NameValueCollection.GetValue(field.Name, field.Value);
                var values = NameValuesCollection.GetValue(field.Name, null);
                field.SetValue(value, values);
            }
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <returns></returns>
        public bool Validate(string type = "column")
        {
            return GetFields(type).All(m => m.Valid);
        }

        /// <summary>
        /// 获取验证错误信息
        /// </summary>
        public string GetValidateError(string type = "column")
        {
            var field = GetFields(type).FirstOrDefault(m => !m.Valid);
            return field == null ? "" : $"{field.Text} {field.Error}";
        }


        #endregion

        #region convert column


        public string GetString(string k, string d = null)
        {
            var field = ColumnFields.FirstOrDefault(m => m.Name == k);
            return string.IsNullOrEmpty(field?.Value) ? d : field.Value;
        }

        public short GetShort(string k, short d = 0)
        {
            var field = ColumnFields.FirstOrDefault(m => m.Name == k);
            return string.IsNullOrEmpty(field?.Value) ? d : Convert.ToInt16(field.Value);
        }

        public int GetInt(string k, int d = 0)
        {
            var field = ColumnFields.FirstOrDefault(m => m.Name == k);
            return string.IsNullOrEmpty(field?.Value) ? d : Convert.ToInt32(field.Value);
        }

        public bool GetBool(string k, bool d = false)
        {
            var field = ColumnFields.FirstOrDefault(m => m.Name == k);
            return string.IsNullOrEmpty(field?.Value) ? d : Convert.ToBoolean(field.Value);
        }

        public decimal GetDecimal(string k, decimal d = 0)
        {
            var field = ColumnFields.FirstOrDefault(m => m.Name == k);
            return string.IsNullOrEmpty(field?.Value) ? d : Convert.ToDecimal(field.Value);
        }

        public float GetFloat(string k, float d = 0)
        {
            var field = ColumnFields.FirstOrDefault(m => m.Name == k);
            return string.IsNullOrEmpty(field?.Value) ? d : Convert.ToSingle(field.Value);
        }

        public DateTime GetDateTime(string k)
        {
            return Convert.ToDateTime(ColumnNameValueCollection[k]);
        }

        public JObject GetJObject(string k)
        {
            return (JObject)ColumnFields.Single(m => m.Name == k).Object;
        }

        public JArray GetJArray(string k)
        {
            return (JArray)ColumnFields.Single(m => m.Name == k).Object;
        }

        #endregion

    }
}
