namespace Monkey.Kernel.Data
{
    public class GetValueAfterEventArgs
    {
        public dynamic DynamicValue { get; set; }
        public string StringValue { get; set; }
    }
}