﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sharper.ExtensionMethods;

namespace Monkey.Kernel
{
    public class Hook
    {
        /// <summary>
        /// 事件集合
        /// </summary>
        private static Dictionary<string, List<HookEventHandler>> HookEventCollection { get; set; } = new Dictionary<string, List<HookEventHandler>>();

        /// <summary>
        /// 绑定事件
        /// </summary>
        public static void Binding(object point, HookEventHandler handler)
        {
            var eventPoint = point.ToString();

            if (!HookEventCollection.ContainsKey(eventPoint))
            {
                HookEventCollection[eventPoint] = new List<HookEventHandler>();
            }

            HookEventCollection[eventPoint].Add(handler);
        }

        /// <summary>
        /// 处理事件
        /// </summary>
        public static bool Hander(object point, object sender, HookEventArgs args, bool defaultValue = true)
        {
            var eventPoint = point.ToString();
            return !HookEventCollection.ContainsKey(eventPoint) ? defaultValue : HookEventCollection[eventPoint].All(handler => handler(sender, args));
        }

        /// <summary>
        /// 重定向事件
        /// </summary>
        public static string RedirectEvent(Controller ctrl, object metadata = null)
        {
            var eventArgs = new HookEventArgs
            {
                RouteData = ctrl.RouteData,
                Response = ctrl.Response,
                Request = ctrl.Request,
                MetaData = metadata?.PropertyToDict(),
            };
            Hander(HookEventPoint.RedirectUrl, ctrl, eventArgs);
            return eventArgs.RedirectUrl;
        }
    }
}
