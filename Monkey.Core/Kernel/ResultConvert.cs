﻿using System.Configuration;
using System.Diagnostics;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Monkey.Kernel
{
    public static class ResultConvert
    {
        private static string Template { get; set; } = ConfigurationManager.AppSettings["template"];

        private static string TemplateName => string.IsNullOrEmpty(Template) ? "v2" : Template;

        public static string T(string viewName, bool tpl = true)
        {
            return tpl ? $"/tpl/{TemplateName}/views/{viewName.TrimStart('/')}" : viewName;
        }

        public static ActionResult ToJsonResult(this object obj)
        {
            return Json(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static ActionResult Content(string content)
        {

            return new ContentResult { Content = content };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static ActionResult Json(object obj)
        {

            var content = new ContentResult { Content = JsonConvert.SerializeObject(obj) };
            return content;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static ActionResult Respond(int code = 0, string msg = "", object obj = null)
        {
            return Json(new
            {
                code,
                msg,
                obj = obj ?? new object(),
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="viewData"></param>
        /// <param name="tpl"></param>
        /// <returns></returns>
        public static ActionResult View(string viewName = null, dynamic viewData = null, bool tpl = true)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                var trace = new StackTrace();
                var type = trace.GetFrame(1).GetMethod().DeclaringType;
                var method = trace.GetFrame(1).GetMethod();
                viewName = $"{type.Name.Replace("Controller", "")}/{method.Name}.cshtml".ToLower();
            }

            viewName = viewName.EndsWith(".cshtml") ? viewName : $"{viewName}.cshtml";
            viewData = viewData ?? new ViewDataDictionary();
            return new ViewResult
            {
                ViewName = tpl ? $"/tpl/{TemplateName}/views/{viewName.TrimStart('/')}" : viewName,
                ViewData = viewData is ViewDataDictionary ? viewData : new ViewDataDictionary(viewData),
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static ActionResult File(byte[] bytes, string contentType = "application/octet-stream")
        {
            return new FileContentResult(bytes, contentType);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static ActionResult Redirect(string url)
        {
            return new RedirectResult(url);
        }
    }
}
