﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Monkey.Kernel.Data;
using Monkey.Kernel.Quick;
using Monkey.Utility;

namespace Monkey.Kernel
{
    public class CURD : Form
    {

        #region Handler

        /// <summary>
        /// 检索
        /// </summary>
        public RetrieveEventHandler Retrieve { get; set; }

        /// <summary>
        /// 检索项
        /// </summary>
        public RetrieveItemEventHandler RetrieveItem { get; set; }

        /// <summary>
        /// 创建
        /// </summary>
        public CreateEventHandler Create { get; set; }

        /// <summary>
        /// 删除
        /// </summary>
        public DeleteEventHandler Delete { get; set; }

        /// <summary>
        /// 详细
        /// </summary>
        public DisplayEventHandler Display { get; set; }

        /// <summary>
        /// 编辑
        /// </summary>
        public UpdateEventHandler Update { get; set; }

        /// <summary>
        /// 检索项过滤
        /// </summary>
        public RetrieveItemFilterHandler RetrieveItemFilter { get; set; }


        #endregion

        #region 对象

        /// <summary>
        /// 检索对象
        /// </summary>
        public RetrieveForm RetrieveObj { get; set; }

        /// <summary>
        /// 创建对象
        /// </summary>
        public CreateForm CreateObj { get; set; }

        /// <summary>
        /// 检索对象
        /// </summary>
        public DeleteForm DeleteObj { get; set; }

        /// <summary>
        /// 检索对象
        /// </summary>
        public DisplayForm DisplayObj { get; set; }

        /// <summary>
        /// 检索对象
        /// </summary>
        public UpdateForm UpdateObj { get; set; }

        /// <summary>
        /// 自动完成
        /// </summary>
        public QuickComplete Quick { get; set; }

        /// <summary>
        /// 默认模板
        /// </summary>
        public Dictionary<string, string> DefaultViews { get; set; } = new Dictionary<string, string>
        {
            {"U", $"core/curd.formpopup.cshtml"},
            {"C", $"core/curd.formpopup.cshtml"},
            {"D", $"core/curd/detail.cshtml"},
            {"R", $"core/curd.retrieve.cshtml"},
        };


        #endregion

        public CURD(HttpRequestBase request, HttpResponseBase response, string method = null) : base(request, response)
        {
            NameValueCollection["method"] = method ?? "";
            CreateObj = new CreateForm();
            Copy(CreateObj);
            DeleteObj = new DeleteForm();
            Copy(DeleteObj);
            UpdateObj = new UpdateForm();
            Copy(UpdateObj);
            RetrieveObj = new RetrieveForm();
            Copy(RetrieveObj);
            DisplayObj = new DisplayForm { ColumnFields = RawFields };
            Copy(DisplayObj);

            Request = request;
            Response = response;

        }

        public ActionResult GetRetrieveResult()
        {
            var hookResult = RetrieveObj.GetHookResult();
            if (hookResult != null)
            {
                return hookResult;
            }

            switch (RetrieveObj.GetCurrentMode())
            {
                case RetrieveMode.Export:
                    {
                        var filename = $"export_attachment_{DateTime.Now.Ticks}.csv";
                        Response.BufferOutput = false;
                        Response.AddHeader("content-disposition", $"attachment;filename={filename}");
                        Response.BinaryWrite(new byte[] { 0xEF, 0xBB, 0xBF });

                        foreach (var columnfield in RetrieveObj.ColumnFields)
                        {
                            var displayName = string.IsNullOrEmpty(columnfield.Text) ? columnfield.Name : columnfield.Text;
                            Response.Write($"\"{displayName}\",");
                        }

                        Response.Write("\r\n");
                        RetrieveObj.Export(RetrieveObj, new ExportEventArgs
                        {
                            WriteLine = item =>
                            {
                                RetrieveItemFilter?.Invoke(item);
                                foreach (var columnfield in RetrieveObj.ColumnFields)
                                {
                                    var raw = item[columnfield.Name]?.ToString();
                                    var value = string.IsNullOrEmpty(raw) ? "" : columnfield.GetValue(false, raw);
                                    if (Regex.IsMatch($"{value}", "^\\d{14,}$"))
                                    {
                                        value = $"`{value}";
                                    }
                                    Response.Write($"\"{value}\",");
                                }
                                Response.Write("\r\n");
                            }
                        });

                        Response.Close();
                        return null;
                    }


                case RetrieveMode.Header:
                    {
                        RetrieveObj.ColumnFields.Insert(0, new Field
                        {
                            type = "checkbox",
                            @fixed = "left"
                        });

                        var toolbarWidth = RetrieveObj.TodoCollection.Where(m => m.Value.Single).Sum(m => m.Value.SingleWidth);
                        toolbarWidth += RetrieveObj.PopupCollection.Where(m => m.Value.Single).Sum(m => m.Value.SingleWidth);
                        RetrieveObj.ColumnFields.Add(new Field
                        {
                            @fixed = "right",
                            Text = "操作",
                            toolbar = "#table-toolbar-row",
                            Width = toolbarWidth
                        });
                        return new
                        {
                            code = 0,
                            msg = "",
                            header = RetrieveObj.ColumnFields.Select(field => new
                            {
                                field = field.Name,
                                title = field.Text,
                                width = field.Width == 0 ? null : (object)field.Width,
                                minWidth = field.minWidth,
                                type = field.type,
                                @fixed = field.@fixed,
                                hide = field.hide,
                                totalRow = field.totalRow,
                                totalRowText = field.totalRowText,
                                sort = field.sort,
                                unresize = field.unresize,
                                edit = field.edit,
                                @event = field.@event,
                                style = field.style,
                                align = field.align,
                                colspan = field.colspan,
                                rowspan = field.rowspan,
                                templet = field.templet,
                                toolbar = field.toolbar,
                            }),
                        }.ToJsonResult();
                    }

                case RetrieveMode.Body:
                    {
                        var checkResult = RetrieveObj.Check(RetrieveObj, RetrieveObj.FilterFields);
                        if (checkResult != null)
                        {
                            return checkResult;
                        }
                        var retrieveEventArgs = new RetrieveEventArgs();
                        var retrieveResult = Retrieve(RetrieveObj, retrieveEventArgs);
                        foreach (var item in retrieveResult)
                        {
                            RetrieveItemFilter?.Invoke(item);

                            foreach (var field in RetrieveObj.ColumnFields)
                            {
                                switch (field.Type)
                                {
                                    case FieldType.Select:
                                    case FieldType.Radio:
                                    case FieldType.TreeSelect:
                                        var fieldValue = $"{item[field.Name]}";
                                        item[field.Name] = !string.IsNullOrEmpty(fieldValue) && field.StringStringD.ContainsKey(fieldValue) ? field.StringStringD[fieldValue] : fieldValue;
                                        break;
                                    case FieldType.Date:
                                    case FieldType.DateTime:
                                        if (item[field.Name] != null)
                                        {
                                            item[field.Name] = ((DateTime)item[field.Name]).ToString("yyyy-MM-dd HH:mm:ss");
                                        }
                                        break;
                                }

                            }
                        }
                        return new
                        {

                            code = 0,
                            msg = "",
                            count = RetrieveObj.Pagination.Total,
                            data = retrieveResult,
                        }.ToJsonResult();
                    }

                case RetrieveMode.View:
                default:
                    {
                        return ResultConvert.View(DefaultViews["R"], RetrieveObj);
                    }

            }
        }

        public ActionResult View(RenderCompleteHandler renderComplete = null)
        {
            if (RetrieveObj.Check == null)
            {
                RetrieveObj.Check = (sender, fields) => sender.FilterValidate() ? null : ResultConvert.Respond(1, RetrieveObj.GetFilterValidateError());
            }

            #region Quick 初始化

            if (Quick != null)
            {
                foreach (var support in Quick.Options["support"])
                {
                    switch ($"{support}".ToUpper())
                    {
                        case "R":
                            RetrieveItem = RetrieveItem ?? Quick.RetrieveSingle;
                            Retrieve = Retrieve ?? Quick.Retrieve;
                            break;
                        case "D":
                            Delete = Delete ?? Quick.Delete;
                            break;
                        case "C":
                            Create = Create ?? Quick.Create;
                            break;
                        case "U":
                            Update = Update ?? Quick.Update;
                            break;
                        case "E":
                            RetrieveObj.Export = Quick.Export;
                            break;
                    }
                }
            }

            #endregion

            #region ToolBox 初始化

            RetrieveObj.LoadFieldData("column");
            RetrieveObj.LoadFieldData("filter");
            RetrieveObj.Init();
            RegisterInsideToolBox();

            renderComplete?.Invoke(this, new RenderCompleteArgs());

            #region 

            if (TodoCollection.ContainsKey(NameValueCollection["method"]))
            {
                NameValueCollection["ids"] = NameValueCollection.GetValue("ids") ?? "";
                var todo = TodoCollection[NameValueCollection["method"]];
                todo.IDs = NameValueCollection["ids"].Split(',').Where(m => !string.IsNullOrEmpty(m)).ToArray();
                todo.ID = todo.IDs.FirstOrDefault();
                if (todo.MustSelect && !todo.IDs.Any())
                {
                    return ResultConvert.Respond(1, "请至少选择一项");
                }
                var result = todo.Callback(todo);
                return result ?? ResultConvert.Respond();
            }

            #endregion


            #region 

            if (PopupCollection.ContainsKey(NameValueCollection["method"]))
            {
                NameValueCollection["ids"] = NameValueCollection.GetValue("ids") ?? "";
                var popup = PopupCollection[NameValueCollection["method"]];
                popup.IDs = NameValueCollection["ids"].Split(',').Where(m => !string.IsNullOrEmpty(m)).ToArray();
                popup.ID = popup.IDs.FirstOrDefault();
                return popup.Callback(popup);
            }

            #endregion


            #endregion

            return GetRetrieveResult();

        }

        public void Copy(Form curd)
        {
            curd = curd ?? new Form(Request, Response);
            curd.Request = Request;
            curd.NameValueCollection = NameValueCollection;
            curd.NameValuesCollection = NameValuesCollection;
            curd.ColumnNameValueCollection = NameValueCollection;
            curd.FilterNameValueCollection = NameValueCollection;
            curd.TodoCollection = TodoCollection;
            curd.PopupCollection = PopupCollection;
        }

        public Field Copy(
            string source,
            string name,
            string text = null,
            FieldType? type = null,
            string value = null,
            bool? required = null,
            string pattern = null,
            string placeholder = null,
            string config = null,
            int? height = null,
            int? width = null)
        {
            List<Field> fields = null;
            switch (source.ToUpper())
            {
                case "C":
                    fields = CreateObj.ColumnFields;
                    break;
                case "U":
                    fields = UpdateObj.ColumnFields;
                    break;
                case "R":
                    fields = RetrieveObj.ColumnFields;
                    break;
                case "F":
                    fields = RetrieveObj.FilterFields;
                    break;
                case "D":
                    fields = DeleteObj.ColumnFields;
                    break;

            }
            return CopyField(name: name, text: text, type: type, value: value, required: required, pattern: pattern, placeholder: placeholder, config: config, height: height, width: width, fields: fields);
        }

        public void RetrieveItemData(string itemId, List<Field> fields)
        {
            var item = RetrieveItem(itemId);
            RetrieveItemFilter?.Invoke(item);
            var retrieveFields = fields.Where(field => item.ContainsKey(field.Name)).ToList();
            foreach (var dataField in retrieveFields)
            {
                string value;
                var data = item[dataField.Name];
                switch (dataField.Type)
                {
                    case FieldType.Date:
                        value = (data as DateTime?)?.ToString("yyyy-MM-dd");
                        break;
                    case FieldType.DateTime:
                        value = (data as DateTime?)?.ToString("yyyy-MM-dd HH:mm:ss");
                        break;
                    default:
                        value = data?.ToString();
                        break;
                }
                dataField.SetValue(value);
            }
        }

        private int PopupHeight(int i)
        {
            var popupMaxHeight = M.GetConfig("system", "popup_max_height", 600);
            var popupMinHeight = M.GetConfig("system", "popup_min_height", 100);
            if (i < popupMinHeight) return popupMinHeight;
            if (i > popupMaxHeight) return popupMaxHeight;
            return i;
        }

        private int PopupWidth(int i)
        {
            var popupMaxWidth = M.GetConfig("system", "popup_max_width", 800);
            var popupMinWidth = M.GetConfig("system", "popup_min_width", 300);
            if (i < popupMinWidth) return popupMinWidth;
            if (i > popupMaxWidth) return popupMaxWidth;
            return i;
        }

        private void RegisterInsideToolBox()
        {

            var popupFormWidth = M.GetConfig("system", "popup_form_width", 768);
            var popupFormFieldHeight = M.GetConfig("system", "popup_form_field_height", 15);
            var popupFormIframeHeight = M.GetConfig("system", "popup_form_iframe_height", 130);

            var popupDetailWidth = M.GetConfig("system", "popup_detail_width", 768);
            var popupDetailFieldHeight = M.GetConfig("system", "popup_detail_field_height", 12);
            var popupDetailIframeHeight = M.GetConfig("system", "popup_detail_iframe_height", 90);


            #region 删除

            if (Delete != null)
            {
                TodoCollection["delete"] = new Todo("删除", todo =>
                {
                    var form = new DeleteForm
                    {
                        Request = Request,
                        NameValueCollection = NameValueCollection,
                        IDs = todo.IDs
                    };
                    try
                    {
                        var retobj = Delete(form, new DeleteEventArgs());
                        return retobj ?? ResultConvert.Respond(0, "删除成功", new { reload = true }); ;
                    }
                    catch (Exception ex)
                    {
                        return ResultConvert.Respond(1, ex.ToString());
                    }
                })
                {
                    MultiIcon = "icon-close",
                    Single = true,
                    SingleText = "删除"
                };
            }

            #endregion


            #region 创建

            if (Create != null)
            {
                var fieldHeight = CreateObj.ColumnFields.Sum(m => m.Height + popupFormFieldHeight);
                fieldHeight = PopupHeight(fieldHeight);
                PopupCollection["create"] = new Popup("添加", popup =>
                {
                    CreateObj.PrimaryButtonText = "添加";
                    CreateObj.LoadFieldData();

                    var hookResult = CreateObj.GetHookResult();
                    if (hookResult != null)
                    {
                        return hookResult;
                    }

                    if (CreateObj.IsHttpMethod())
                    {
                        if (!CreateObj.Validate())
                        {
                            return ResultConvert.Respond(1, CreateObj.GetValidateError());
                        }
                        var args = new CreateEventArgs();
                        var result = Create(CreateObj, args);
                        return result ?? ResultConvert.Respond(0, "添加成功", new { close = true, reload = true });
                    }

                    return ResultConvert.View(DefaultViews["C"], CreateObj);

                })
                {
                    MultiIcon = "icon-plus",
                    Width = $"{popupFormWidth}px",
                    Height = $"{fieldHeight + popupFormIframeHeight}px"
                };
            }

            #endregion


            #region 编辑

            if (Update != null)
            {
                var fieldHeight = UpdateObj.ColumnFields.Sum(m => m.Height + popupFormFieldHeight);
                fieldHeight = PopupHeight(fieldHeight);
                PopupCollection["edit"] = new Popup("编辑", popup =>
                {

                    UpdateObj.PrimaryButtonText = "编辑";
                    UpdateObj.LoadFieldData();
                    UpdateObj.ID = popup.ID;

                    var hookResult = UpdateObj.GetHookResult();
                    if (hookResult != null)
                    {
                        return hookResult;
                    }

                    if (UpdateObj.IsHttpMethod())
                    {
                        if (!UpdateObj.Validate())
                        {
                            return ResultConvert.Respond(1, UpdateObj.GetValidateError());
                        }
                        var args = new UpdateEventArgs();
                        var result = Update(UpdateObj, args);
                        return result ?? ResultConvert.Respond(0, "编辑成功", new { close = true, reload = true });
                    }

                    RetrieveItemData(UpdateObj.ID, UpdateObj.ColumnFields);
                    return ResultConvert.View(DefaultViews["U"], UpdateObj);

                })
                {
                    Multi = false,
                    Single = true,
                    SingleText = "编辑",
                    Width = $"{popupFormWidth}px",
                    Height = $"{fieldHeight + popupFormIframeHeight}px"
                };
            }

            #endregion


            #region 详细

            if (Display != null)
            {
                var fieldHeight = DisplayObj.ColumnFields.Sum(m => m.Height + popupDetailFieldHeight);
                fieldHeight = PopupHeight(fieldHeight);
                PopupCollection["detail"] = new Popup("详细", popup =>
                {
                    DisplayObj.LoadFieldData();
                    DisplayObj.ID = popup.ID;
                    RetrieveItemData(DisplayObj.ID, DisplayObj.ColumnFields);
                    return ResultConvert.View(DefaultViews["D"], DisplayObj);
                })
                {
                    Multi = false,
                    Single = true,
                    SingleText = "详细",
                    Width = $"{popupDetailWidth}px",
                    Height = $"{fieldHeight + popupDetailIframeHeight}px",
                };
            }

            #endregion

        }

    }
}
