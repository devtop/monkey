﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Monkey.Kernel
{
    /// <summary>
    /// 路由配置
    /// </summary>
    public class RouteConfig
    {

        /// <summary>
        /// 路由表
        /// </summary>
        public static List<RouteItem> Routes { get; set; } = new List<RouteItem>();

        /// <summary>
        /// 路由表排除名单
        /// </summary>
        public static Dictionary<string, bool> ExcludeRouteRules { get; set; } = new Dictionary<string, bool>();

        /// <summary>
        /// 路由表忽略名单
        /// </summary>
        public static Dictionary<string, bool> IgnoreRouteRules { get; set; } = new Dictionary<string, bool>();

        /// <summary>
        /// 注册路由表
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="method"></param>
        /// <param name="description"></param>
        /// <param name="level"></param>
        public static void RegisterRoute(string controller, string action, string method = "", string description = "", int level = 1)
        {
            var route = new RouteItem();
            route.Controller = controller;
            route.Action = action;
            route.Method = method;
            route.Description = description;
            route.Level = level;
            route.FullPath = $"/{route.Controller}/{route.Action}";
            route.FullPath += string.IsNullOrEmpty(route.FullPath) ? "" : $"/{route.Method}";
            route.FullPath = route.FullPath.ToLower().TrimEnd('/');
            Routes.Add(route);
        }

        /// <summary>
        /// 注册路由表 - 根据Assembly导入
        /// </summary>
        /// <param name="asm"></param>
        public static void RegisterRoutes(Assembly asm)
        {
            foreach (var t in asm.GetTypes())
            {
                if (t.IsPublic && t.BaseType == typeof(Controller) && t.Name.EndsWith("Controller"))
                {
                    //是否继承 IMonkeySite 接口
                    if (t.GetInterfaces().Contains(typeof(IControllerFilter)))
                    {
                        if (t.FullName != null)
                        {
                            var site = asm.CreateInstance(t.FullName) as IControllerFilter;
                            site?.Init();
                        }
                    }

                    var controllerName = t.Name.Replace("Controller", "");
                    var controllerDescription = t.GetCustomAttribute<DescriptionAttribute>()?.Description ?? controllerName;

                    foreach (var action in t.GetMethods(BindingFlags.Instance | BindingFlags.Public))
                    {
                        if (action.ReturnType == typeof(ActionResult))
                        {
                            var actionName = action.Name;
                            var actionDescription = action.GetCustomAttribute<DescriptionAttribute>()?.Description ?? actionName;
                            if (actionDescription.Split(',').Length == 1)
                            {
                                //处理路由表忽略名单
                                if (!IsMatchRouteRules(IgnoreRouteRules, controllerName, actionName))
                                {
                                    RegisterRoute(controllerName, action.Name, description: $"/{controllerDescription}/{actionDescription}");
                                }
                            }
                            else
                            {
                                var methods = actionDescription.Split(',');
                                foreach (var method in methods.Skip(1).ToList())
                                {
                                    var sc = method.Split(':');
                                    var methodName = sc[0];
                                    var methodText = sc.Length > 1 ? sc[1] : "";
                                    var routepath = $"{controllerName}/{actionName}/{methodName}".ToLower().Trim('/');

                                    //处理路由表忽略名单
                                    if (IgnoreRouteRules.Any(b => b.Value && Regex.IsMatch(routepath, b.Key) || !b.Value && routepath == b.Key))
                                    {
                                        continue;
                                    }

                                    RegisterRoute(controllerName, actionName, methodName, $"/{controllerDescription}/{methods[0]}/{methodText}", level: 2);
                                }
                            }
                        }
                        else if (action.IsPublic)
                        {
                            //处理白名单
                            ExcludeRouteRules[$"{controllerName}/{action.Name}".ToLower()] = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeRules"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="methodName"></param>
        /// <returns></returns>
        public static bool IsMatchRouteRules(Dictionary<string, bool> routeRules, string controllerName, string actionName, string methodName = "")
        {
            //处理路由表规则
            var routepath = $"{controllerName}/{actionName}/{methodName ?? ""}".ToLower().Trim('/');
            return routeRules.Any(b => b.Value && Regex.IsMatch(routepath, b.Key) || !b.Value && routepath == b.Key);
        }

        /// <summary>
        /// 初始化路映射规则
        /// </summary>
        /// <param name="routes"></param>
        public static void InitRouteMapRules(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("controller/action/method", "{controller}/{action}/{method}", new { controller = "Home", action = "Index", method = UrlParameter.Optional });
        }

    }
}
