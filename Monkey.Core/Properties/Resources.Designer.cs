﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Monkey.Properties {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Monkey.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   重写当前线程的 CurrentUICulture 属性
        ///   重写当前线程的 CurrentUICulture 属性。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 /*
        ///Navicat MySQL Data Transfer
        ///
        ///Source Server         : localhost_3306
        ///Source Server Version : 50639
        ///Source Host           : localhost:3306
        ///Source Database       : monkey
        ///
        ///Target Server Type    : MYSQL
        ///Target Server Version : 50639
        ///File Encoding         : 65001
        ///
        ///Date: 2019-01-07 19:18:09
        ///*/
        ///
        ///SET FOREIGN_KEY_CHECKS=0;
        ///
        ///-- ----------------------------
        ///-- Table structure for `monkey_access_log`
        ///-- ----------------------------
        ///DROP TABLE IF EXISTS `monkey_access_log`;
        ///CREATE TABLE `monkey_a [字符串的其余部分被截断]&quot;; 的本地化字符串。
        /// </summary>
        internal static string INIT_SQL {
            get {
                return ResourceManager.GetString("INIT_SQL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 &lt;!DOCTYPE html&gt;
        ///&lt;html&gt;
        ///&lt;head&gt;
        ///    &lt;meta charset=&quot;utf-8&quot; /&gt;
        ///    &lt;script src=&quot;https://cdn.vaptcha.com/v.js&quot;&gt;&lt;/script&gt;
        ///    &lt;script src=&quot;/captcha/vaptcha?js=1.0&quot;&gt;&lt;/script&gt;
        ///&lt;/head&gt;
        ///&lt;body&gt;
        ///    &lt;div&gt;
        ///        &lt;div id=&quot;captcha-container&quot;&gt;&lt;/div&gt;
        ///        &lt;div id=&quot;captcha-msg&quot;&gt;&lt;/div&gt;
        ///    &lt;/div&gt;
        ///&lt;/body&gt;
        ///&lt;/html&gt; 的本地化字符串。
        /// </summary>
        internal static string vaptchav_html {
            get {
                return ResourceManager.GetString("vaptchav_html", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 var vid, challenge;
        ///var GetVaptchaUrl = &quot;/captcha/GetVaptchaChallenge&quot;;
        ///var ValidateUrl = &quot;/captcha/VaptchaValidate&quot;;
        ///var OutAgeUrl = &quot;/captcha/GetVaptchaDownTime&quot;;
        ///
        /////初始化浮动式
        ///window.onload = vaptchaInit = function() {
        ///    try {
        ///        var ajaxGet = new AjaxClass();
        ///        ajaxGet.Async = true;
        ///        ajaxGet.Url = GetVaptchaUrl;
        ///        ajaxGet.CallBack = function(res) {
        ///            var r = JSON.parse(res);
        ///            vid = r.id;
        ///            challenge = r.challenge;
        ///            //验证参数对象
        /// [字符串的其余部分被截断]&quot;; 的本地化字符串。
        /// </summary>
        internal static string vaptchav_js {
            get {
                return ResourceManager.GetString("vaptchav_js", resourceCulture);
            }
        }
    }
}
