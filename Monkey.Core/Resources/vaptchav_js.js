﻿var vid, challenge;
var GetVaptchaUrl = "/captcha/GetVaptchaChallenge";
var ValidateUrl = "/captcha/VaptchaValidate";
var OutAgeUrl = "/captcha/GetVaptchaDownTime";

//初始化浮动式
window.onload = vaptchaInit = function() {
    try {
        var ajaxGet = new AjaxClass();
        ajaxGet.Async = true;
        ajaxGet.Url = GetVaptchaUrl;
        ajaxGet.CallBack = function(res) {
            var r = JSON.parse(res);
            vid = r.id;
            challenge = r.challenge;
            //验证参数对象
            var options = {
                vid: vid,
                //站点id ,string,必选
                challenge: challenge,
                //验证流水号 ,string,必选
                container: document.getElementById("captcha-container"),
                //验证码容器,element,必选
                type: "float",
                //验证图显示方式,string,可选择float,popup
                https: false,
                //协议类型,boolean,可选true,false
                color: "#57ABFF",
                //点击式按钮的背景颜色,string
                outage: OutAgeUrl,
                success: function(token, challenge) {
                    //当验证成功时执行回调,function,参数token为string,必选
                    var ajax = new AjaxClass();
                    ajax.Async = true;
                    ajax.Method = "POST";
                    ajax.Url = ValidateUrl;
                    ajax.Arg = JSON.stringify({
                        "token": token,
                        "challenge": challenge
                    });
                    ajax.CallBack = function(result) {
                        document.getElementById("captcha-msg").textContent = result;
                    }
                    ajax.Send();
                },
                fail: function() { //验证失败时执行回调  
                },
                close: function() { //隐藏式关闭时执行回调
                }
            }
            window.vaptcha(options,
                function(obj) {
                    obj.init();
                });
        }
        ajaxGet.Send();
    } catch (e) {
        //
    }
};
//ajax类
function AjaxClass() {
    var xmlHttp = false;
    try {
        xmlHttp = new XMLHttpRequest();
        //FireFox专有  

    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("MSXML2.XMLHTTP");
        } catch (e2) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e3) {
                alert("你的浏览器不支持XMLHTTP对象，请升级到IE6以上版本！");
                xmlHttp = false;
            }
        }
    }

    var me = this;
    this.Method = "GET";
    this.Url = "";
    this.Async = false;
    this.Arg = "";
    this.CallBack = function () { }
        ;
    this.Loading = function () { }
        ;

    this.Send = function () {
        if (this.Url === "") {
            return false;
        }
        if (!xmlHttp) {
            return iframePost();
        }

        xmlHttp.open(this.Method, this.Url, this.Async);
        if (this.Method === "POST") {
            xmlHttp.setRequestHeader("Content-Type", "application/json");
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4) {
                var result = false;
                if (xmlHttp.status === 200) {
                    result = xmlHttp.responseText;

                }
                xmlHttp = null;

                me.CallBack(result);
            } else {
                me.Loading();
            }
        }
        if (this.Method === "POST") {
            xmlHttp.send(this.Arg);
        } else {
            xmlHttp.send(null);
        }
    }
        ;

    //Iframe方式提交  
    function iframePost() {
        var Num = 0;
        var obj = document.createElement("iframe");
        obj.attachEvent("onload", function () {
            me.CallBack(obj.contentWindow.document.body.innerHTML);
            obj.removeNode();
        });
        obj.attachEvent("onreadystatechange", function () {
            if (Num >= 5) {
                alert(false);
                obj.removeNode();
            }
        });
        obj.src = me.Url;
        obj.style.display = 'none';
        document.body.appendChild(obj);
    }
}
