/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : monkey

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2019-02-21 22:19:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `test`
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CheckBox` varchar(255) DEFAULT NULL,
  `Chosen` text,
  `ChosenTreeView` text,
  `Date` datetime DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Decimal` decimal(18,4) DEFAULT NULL,
  `File` text,
  `FroalaWYSIWYG` text,
  `Hidden` text,
  `Image` text,
  `MultiChosen` text,
  `MultiFile` text,
  `Number` int(11) DEFAULT NULL,
  `Password` text,
  `Radio` text,
  `Select` text,
  `Spinner` text,
  `Switch` text,
  `Tags` text,
  `Text` text,
  `Textarea` text,
  `Toff` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------
