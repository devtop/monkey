/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : monkey

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2019-02-21 21:47:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `monkey_access_log`
-- ----------------------------
DROP TABLE IF EXISTS `monkey_access_log`;
CREATE TABLE `monkey_access_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `request_url` text NOT NULL,
  `request_data` longtext NOT NULL,
  `request_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monkey_access_log
-- ----------------------------
INSERT INTO `monkey_access_log` VALUES ('55', '500', 'System.Data.EntityCommandExecutionException: 执行命令定义时出错。有关详细信息，请参阅内部异常。 ---> MySql.Data.MySqlClient.MySqlException: Unknown column \'Extent1.term_count\' in \'field list\'\r\n   在 MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   在 MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   在 MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   在 MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   在 MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   在 MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   在 MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   在 System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   在 System.Data.EntityClient.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)\r\n   --- 内部异常堆栈跟踪的结尾 ---\r\n   在 System.Data.EntityClient.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)\r\n   在 System.Data.Objects.Internal.ObjectQueryExecutionPlan.Execute[TResultType](ObjectContext context, ObjectParameterCollection parameterValues)\r\n   在 System.Data.Objects.ObjectQuery`1.GetResults(Nullable`1 forMergeOption)\r\n   在 System.Data.Objects.ObjectQuery`1.System.Collections.Generic.IEnumerable<T>.GetEnumerator()\r\n   在 System.Linq.Enumerable.FirstOrDefault[TSource](IEnumerable`1 source)\r\n   在 System.Data.Objects.ELinq.ObjectQueryProvider.<>c__11`1.<GetElementFunction>b__11_1(IEnumerable`1 sequence)\r\n   在 System.Data.Objects.ELinq.ObjectQueryProvider.ExecuteSingle[TResult](IEnumerable`1 query, Expression queryRoot)\r\n   在 System.Data.Objects.ELinq.ObjectQueryProvider.System.Linq.IQueryProvider.Execute[S](Expression expression)\r\n   在 System.Data.Entity.Internal.Linq.DbQueryProvider.Execute[TResult](Expression expression)\r\n   在 System.Linq.Queryable.FirstOrDefault[TSource](IQueryable`1 source, Expression`1 predicate)\r\n   在 Monkey.M.<>c__DisplayClass25_0`1.<GetConfig>b__0() 位置 D:\\dev\\workspace\\c#\\src\\monkey\\Monkey.Core\\M.cs:行号 155\r\n   在 Monkey.Utility.MemCache.Get[T](String key, MemCacheNonExistEventHandler handler, String groupName, Int32 expire) 位置 D:\\dev\\workspace\\c#\\src\\monkey\\Monkey.Utility\\MemCache.cs:行号 41\r\n   在 Monkey.M.GetConfig[T](String termname, String metakey, T metavalue, Int32 expire) 位置 D:\\dev\\workspace\\c#\\src\\monkey\\Monkey.Core\\M.cs:行号 151\r\n   在 Monkey.Kernel.RouteFilterAttribute.OnActionExecuting(ActionExecutingContext content) 位置 D:\\dev\\workspace\\c#\\src\\monkey\\Monkey.Core\\Kernel\\RouteFilterAttribute.cs:行号 35\r\n   在 System.Web.Mvc.Async.AsyncControllerActionInvoker.AsyncInvocationWithFilters.InvokeActionMethodFilterAsynchronouslyRecursive(Int32 filterIndex)\r\n   在 System.Web.Mvc.Async.AsyncControllerActionInvoker.AsyncInvocationWithFilters.InvokeActionMethodFilterAsynchronouslyRecursive(Int32 filterIndex)\r\n   在 System.Web.Mvc.Async.AsyncControllerActionInvoker.<>c__DisplayClass33.<BeginInvokeActionMethodWithFilters>b__31(AsyncCallback asyncCallback, Object asyncState)\r\n   在 System.Web.Mvc.Async.AsyncResultWrapper.WrappedAsyncResult`1.CallBeginDelegate(AsyncCallback callback, Object callbackState)\r\n   在 System.Web.Mvc.Async.AsyncResultWrapper.WrappedAsyncResultBase`1.Begin(AsyncCallback callback, Object state, Int32 timeout)\r\n   在 System.Web.Mvc.Async.AsyncControllerActionInvoker.BeginInvokeActionMethodWithFilters(ControllerContext controllerContext, IList`1 filters, ActionDescriptor actionDescriptor, IDictionary`2 parameters, AsyncCallback callback, Object state)\r\n   在 System.Web.Mvc.Async.AsyncControllerActionInvoker.<>c__DisplayClass21.<BeginInvokeAction>b__19(AsyncCallback asyncCallback, Object asyncState)', '/b/config/rbac?method=get_usergroup_route&id=1', '[]', '2019-02-21 21:46:20');

-- ----------------------------
-- Table structure for `monkey_account_log`
-- ----------------------------
DROP TABLE IF EXISTS `monkey_account_log`;
CREATE TABLE `monkey_account_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `before_change` decimal(18,2) NOT NULL,
  `change_amount` decimal(18,2) NOT NULL,
  `after_change` decimal(18,2) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `relateder` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `other1` varchar(200) DEFAULT NULL,
  `other2` varchar(200) DEFAULT NULL,
  `other3` varchar(200) DEFAULT NULL,
  `other4` varchar(200) DEFAULT NULL,
  `other5` varchar(200) DEFAULT NULL,
  `other6` text,
  `other7` text,
  `other8` text,
  `other9` text,
  `other10` text,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `type` (`type`),
  KEY `status` (`status`),
  KEY `create_time` (`create_time`),
  KEY `other1` (`other1`),
  KEY `other2` (`other2`),
  KEY `other3` (`other3`),
  KEY `other4` (`other4`),
  KEY `other5` (`other5`),
  CONSTRAINT `monkey_account_log_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `monkey_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monkey_account_log
-- ----------------------------

-- ----------------------------
-- Table structure for `monkey_term`
-- ----------------------------
DROP TABLE IF EXISTS `monkey_term`;
CREATE TABLE `monkey_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `term_type` varchar(50) NOT NULL,
  `term_name` varchar(50) NOT NULL,
  `term_text` varchar(200) DEFAULT NULL,
  `term_order` int(11) NOT NULL,
  `term_parent` int(11) NOT NULL,
  `term_desc` longtext,
  `term_count` int(11) NOT NULL,
  `term_group` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `term_type` (`term_type`),
  KEY `term_name` (`term_name`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monkey_term
-- ----------------------------
INSERT INTO `monkey_term` VALUES ('1', 'usergroup', 'administrator', '管理员', '0', '0', null, '0', '');
INSERT INTO `monkey_term` VALUES ('2', 'usergroup', 'anonymous', '匿名', '0', '0', null, '0', '');
INSERT INTO `monkey_term` VALUES ('129', 'config', 'system', null, '0', '0', null, '0', '');

-- ----------------------------
-- Table structure for `monkey_termmeta`
-- ----------------------------
DROP TABLE IF EXISTS `monkey_termmeta`;
CREATE TABLE `monkey_termmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termid` int(11) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` longtext,
  `meta_desc` varchar(200) DEFAULT NULL,
  `meta_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `termid` (`termid`),
  KEY `meta_key` (`meta_key`),
  KEY `meta_type` (`meta_type`),
  CONSTRAINT `monkey_termmeta_ibfk_1` FOREIGN KEY (`termid`) REFERENCES `monkey_term` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monkey_termmeta
-- ----------------------------
INSERT INTO `monkey_termmeta` VALUES ('24', '2', 'routes', '{\"/basic\":false,\"/basic/index\":true,\"/basic/signin\":true,\"/basic/signup\":true,\"/basic/forgotpwd\":true,\"/basic/forgotpwd/send\":true,\"/basic/forgotpwd/reset\":true,\"/basic/logout\":true,\"/basic/uploadfile\":true,\"/basic/deletefile\":true,\"/basic/info\":true,\"/basic/info/explorer\":true,\"/basic/info/tasklist\":true,\"/basic/page\":true,\"/basic/userprofile\":true,\"/basic/usersettings\":true,\"/basic/usersettings/modifypassword\":true,\"/basic/usersettings/modifymeta\":true,\"/basic/useraccount\":true,\"/basic/useraccount/retrieve\":true,\"/basic/useraccount/delete\":true,\"/basic/useraccount/create\":true,\"/basic/useraccount/edit\":true,\"/basic/useraccount/detail\":true,\"/basic/useraccountrecord\":true,\"/basic/useraccountrecord/retrieve\":true,\"/basic/useraccountrecord/delete\":true,\"/basic/useraccountrecord/create\":true,\"/basic/useraccountrecord/edit\":true,\"/basic/useraccountrecord/detail\":true,\"/basic/usermanager\":true,\"/basic/usermanager/retrieve\":true,\"/basic/usermanager/delete\":true,\"/basic/usermanager/create\":true,\"/basic/usermanager/edit\":true,\"/basic/usermanager/detail\":true,\"/basic/configrolebasedaccesscontrol\":true,\"/basic/configrolebasedaccesscontrol/delete_usergroup\":true,\"/basic/configrolebasedaccesscontrol/add_usergroup\":true,\"/basic/configrolebasedaccesscontrol/get_usergroup_route\":true,\"/basic/configrolebasedaccesscontrol/save_usergroup_route\":true,\"/basic/configsetup\":true,\"/basic/configsetup/delete\":true,\"/basic/configsetup/save\":true,\"/basic/configmanager\":true,\"/basic/configmanager/retrieve\":true,\"/basic/configmanager/delete\":true,\"/basic/configmanager/create\":true,\"/basic/configmanager/edit\":true,\"/basic/configmanager/detail\":true,\"/basic/tooldbprocesslist\":true,\"/basic/tooldbprocesslist/retrieve\":true,\"/basic/tooldbprocesslist/kill\":true}', null, 'json');
INSERT INTO `monkey_termmeta` VALUES ('113', '129', 'ignore_request_url', '[\"^__[a-zA-Z0-9_\\\\-]\"]', null, 'json');
INSERT INTO `monkey_termmeta` VALUES ('114', '1', 'routes', '{}', null, 'json');

-- ----------------------------
-- Table structure for `monkey_user`
-- ----------------------------
DROP TABLE IF EXISTS `monkey_user`;
CREATE TABLE `monkey_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `displayname` varchar(50) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `usergroup` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `signin_date` datetime DEFAULT NULL,
  `signin_addr` varchar(50) DEFAULT NULL,
  `signup_date` datetime DEFAULT NULL,
  `appid` int(11) DEFAULT NULL,
  `discount` decimal(5,4) DEFAULT '0.0000',
  `balance` decimal(12,2) DEFAULT '0.00',
  `recommender` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monkey_user
-- ----------------------------
INSERT INTO `monkey_user` VALUES ('1', 'admin', null, 'admin', 'lSxOqEZCF4Z0wbcaJxrmOAPTSGA2mzpyqbjE3sMhvW3n6oljdEE4Niuf7K5jDP+F3dKT/snmKuwWcfqqeBwJAg==', 'administrator', 'available', '2018-09-11 17:31:11', '::1', '2017-04-21 10:35:15', '6', '1.0000', '9900.00', null);
INSERT INTO `monkey_user` VALUES ('2', 'anonymous', null, 'anonymous', 'Ym3+RxhYZPVwEEL9D7PhQ/KSJXtgEc7/afAY9e98vbwAoXeqn0OsTDvxNuDuL8DWu5G6sZQXn/2O5j+MODX/hw==', 'anonymous', 'available', '2017-08-14 11:15:38', '', '2017-05-27 10:30:01', '6', '1.0000', '0.00', null);

-- ----------------------------
-- Table structure for `monkey_usermeta`
-- ----------------------------
DROP TABLE IF EXISTS `monkey_usermeta`;
CREATE TABLE `monkey_usermeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_key` (`userid`,`meta_key`) USING BTREE,
  CONSTRAINT `monkey_usermeta_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `monkey_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monkey_usermeta
-- ----------------------------
