﻿using System.Diagnostics;
using Microsoft.VisualBasic.Devices;
using Monkey.Component;
using Monkey.Kernel.AutoTask;

namespace Monkey.Tasks
{
    public class UpdateCPUInfoTasker : AutoTasker
    {

        public ComputerInfo Computer { get; set; }

        public PerformanceCounter Counter { get; set; }

        public UpdateCPUInfoTasker() : base("更新CPU信息")
        {
            Computer = new ComputerInfo();
            Counter = new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName);
            Interval = 3;
        }

        public override void Execute()
        {
            if (!M.GetConfig("system", "EnableCPURAMInfoTasker", false)) return;
            var cpu = Counter.NextValue();
            var ram = ((double)Process.GetCurrentProcess().WorkingSet64 / Computer.TotalPhysicalMemory) * 100;
            MonkeyInfo.RAMWorkingSet64 = (double)Process.GetCurrentProcess().WorkingSet64 / 1024 / 1024;
            MonkeyInfo.SetHistoryData(cpu, ram);
        }

        public override void Kill()
        {

        }
    }
}
