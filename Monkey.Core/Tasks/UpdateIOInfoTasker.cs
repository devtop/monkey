using System.Collections.Generic;
using System.IO;
using System.Linq;
using Monkey.Component;
using Monkey.Kernel.AutoTask;

namespace Monkey.Tasks
{
    public class UpdateIOInfoTasker : AutoTasker
    {

        public UpdateIOInfoTasker() : base("更新磁盘空间信息")
        {
            Interval = 3600;
        }


        public static string[] AudioExtensions { get; set; } = "mp3 wma rm wav midi ape flac".Split(' ');
        public static string[] ImageExtensions { get; set; } = "bmp jpg jpeg png gif".Split(' ');
        public static string[] VideoExtensions { get; set; } = "avi rmvb rm asf divx mpg mpeg mpe wmv mp4 mkv vob".Split(' ');

        ///  <summary> 
        /// 获取指定驱动器的空间总大小(单位为MB) 
        ///  </summary> 
        ///  <param name="hardDiskName">只需输入代表驱动器的字母即可 （大写）</param> 
        ///  <returns> </returns> 
        public static double GetHardDiskSpace(string hardDiskName)
        {
            hardDiskName = hardDiskName + ":\\";
            var totalSize = 0d;
            var drives = System.IO.DriveInfo.GetDrives();
            foreach (var drive in drives)
            {
                if (drive.Name == hardDiskName)
                {
                    totalSize = (double)drive.TotalSize / 1024 / 1024 / 1024;
                }
            }
            return totalSize;
        }

        ///  <summary> 
        /// 获取指定驱动器的剩余空间总大小(单位为B) 
        ///  </summary> 
        ///  <param name="hardDiskName">只需输入代表驱动器的字母即可 </param> 
        ///  <returns> </returns> 
        public static double GetHardDiskFreeSpace(string hardDiskName)
        {
            hardDiskName = hardDiskName + ":\\";
            var freeSpace = 0d;
            var drives = System.IO.DriveInfo.GetDrives();
            foreach (var drive in drives)
            {
                if (drive.Name == hardDiskName)
                {
                    freeSpace = (double)drive.TotalFreeSpace / 1024 / 1024 / 1024;
                }
            }
            return freeSpace;
        }

        public override void Execute()
        {
            if (!M.GetConfig("system", "EnableIOInfoTasker", false)) return;

            var homeHardDisk = M.GetConfig("system", "HomeHardDisk", "C");
            var homePath = M.GetConfig("system", "HomePath", "");
            var uploadFileDir = M.GetConfig("system", "UploadFileDir", "/uploads");
            var uploadPath = $"{homePath.TrimEnd('/')}/{uploadFileDir.TrimStart('/')}";
            var uploadFileStat = new Dictionary<string, long>();

            MonkeyInfo.TotalSize = GetHardDiskSpace(homeHardDisk);
            MonkeyInfo.TotalFreeSpace = GetHardDiskFreeSpace(homeHardDisk);
            MonkeyInfo.TotalFreeSpacePercent = (MonkeyInfo.TotalFreeSpace / MonkeyInfo.TotalSize) * 100;

            uploadFileStat["audio_count"] = 0;
            uploadFileStat["image_count"] = 0;
            uploadFileStat["video_count"] = 0;
            uploadFileStat["other_count"] = 0;

            uploadFileStat["audio_size"] = 0;
            uploadFileStat["image_size"] = 0;
            uploadFileStat["video_size"] = 0;
            uploadFileStat["other_size"] = 0;

            if (Directory.Exists(uploadPath))
            {
                foreach (var file in Directory.GetFiles(uploadPath, "*.*", SearchOption.AllDirectories))
                {
                    var fileInfo = new FileInfo(file);

                    var extension = fileInfo.Extension.TrimStart('.').ToLower();
                    var length = fileInfo.Length;

                    if (AudioExtensions.Any(m => m == extension))
                    {
                        uploadFileStat["audio_count"]++;
                        uploadFileStat["audio_size"] += length;
                        continue;
                    }

                    if (ImageExtensions.Any(m => m == extension))
                    {
                        uploadFileStat["image_count"]++;
                        uploadFileStat["image_size"] += length;
                        continue;
                    }

                    if (VideoExtensions.Any(m => m == extension))
                    {
                        uploadFileStat["video_count"]++;
                        uploadFileStat["video_size"] += length;
                        continue;
                    }

                    uploadFileStat["other_count"]++;
                    uploadFileStat["other_size"] += length;

                }
            }
            M.SetConfig("system", "UploadFileStat", uploadFileStat);
        }

        public override void Kill()
        {

        }
    }


}