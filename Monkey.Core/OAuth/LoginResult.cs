﻿using Monkey.Controllers;

namespace Monkey.OAuth
{
    public class LoginResult
    {
        public int code { get; set; }
        public string channel { get; set; }
        public UserInfo user { get; set; }
    }
}