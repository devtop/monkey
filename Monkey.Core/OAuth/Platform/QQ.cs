﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Monkey.Utility;
using Newtonsoft.Json.Linq;

namespace Monkey.OAuth.Platform
{
    public class QQ : LoginBase
    {
        public static string AuthorizeUrl { get; set; }

        public static string OauthUrl { get; set; }

        public static string OpenidUrl { get; set; }

        public static string UserInfoUrl { get; set; }

        public AuthorizeResult Authorize()
        {
            try
            {
                var code = AuthorizeCode;

                if (string.IsNullOrEmpty(code))
                {
                    HttpContext.Current.Response.Redirect(AuthorizeUrl + RedirectUri, true);

                    HttpContext.Current.Response.End();

                    return null;
                }

                if (!string.IsNullOrEmpty(code))
                {
                    var errorMsg = string.Empty;

                    var token = Accesstoken(code, ref errorMsg);

                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        var access_token = token["access_token"];

                        var user = UserInfo(access_token, ref errorMsg);

                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            return new AuthorizeResult { code = 0, result = user, token = access_token };
                        }

                        return new AuthorizeResult { code = 3, error = errorMsg, token = access_token };
                    }

                    return new AuthorizeResult { code = 2, error = errorMsg };
                }
            }

            catch (Exception ex)
            {
                return new AuthorizeResult { code = 1, error = ex.Message };
            }

            return null;
        }

        private Dictionary<string, string> Accesstoken(string code, ref string errMsg)
        {
            var redirect_uri = HttpContext.Current.Request.GetXUri().AbsoluteUri.Split('?')[0];
            var data = new SortedDictionary<string, string>();
            data.Add("client_id", LoginProvider.QQClientId);
            data.Add("client_secret", LoginProvider.QQClientSecret);
            data.Add("grant_type", "authorization_code");
            data.Add("code", code);
            data.Add("redirect_uri", redirect_uri);

            var Params = string.Join("&", data.Select(x => x.Key + "=" + x.Value).ToArray());

            var AccessTokenUrl = OauthUrl + Params;

            using (var wb = new WebClient())
            {
                try
                {
                    var result = wb.DownloadString(AccessTokenUrl);

                    var kvs = result.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

                    var dic = new Dictionary<string, string>();

                    foreach (var v in kvs)
                    {
                        var kv = v.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                        dic.Add(kv[0], kv[1]);
                    }

                    return dic;
                }
                catch (Exception ex)
                {
                    errMsg = ex.Message;

                    return null;
                }
            }
        }

        private JObject UserInfo(string token, ref string errMsg)
        {
            try
            {
                var result = string.Empty;

                using (var wc = new WebClient() { Encoding = Encoding.UTF8 })
                {
                    result = wc.DownloadString(OpenidUrl + token);
                }

                result = result.Replace("callback(", string.Empty).Replace(");", string.Empty).Trim();

                var openid = Deserialize(result).Value<string>("openid");

                using (var wc = new WebClient() { Encoding = Encoding.UTF8 })
                {
                    result = wc.DownloadString(string.Format(UserInfoUrl, openid, token));
                }

                var user = Deserialize(result);

                user.Add("openid", openid);

                return user;

            }
            catch (Exception ex)
            {
                errMsg = ex.Message;

                return null;
            }
        }

        public QQ(LoginProvider provider) : base(provider)
        {
            AuthorizeUrl = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=" + LoginProvider.QQClientId + "&redirect_uri=";
            OauthUrl = "https://graph.qq.com/oauth2.0/token?";
            OpenidUrl = "https://graph.qq.com/oauth2.0/me?access_token=";
            UserInfoUrl = "https://graph.qq.com/user/get_user_info?format=json&oauth_consumer_key=" + LoginProvider.QQClientId + "&openid={0}&access_token={1}";
        }
    }
}
