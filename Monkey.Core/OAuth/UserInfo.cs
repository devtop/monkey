﻿namespace Monkey.OAuth
{
    public class UserInfo
    {
        public string uid { get; set; }
        public string name { get; set; }
        public string figureurl { get; set; }
        public string token { get; set; }
    }
}