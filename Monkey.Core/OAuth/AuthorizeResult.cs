﻿using Newtonsoft.Json.Linq;

namespace Monkey.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthorizeResult
    {
        public int code { get; set; }

        public string error { get; set; }

        public JObject result { get; set; }

        public string token { get; set; }
    }
}