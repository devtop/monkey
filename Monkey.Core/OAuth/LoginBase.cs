﻿using System.Web;
using Monkey.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Monkey.OAuth
{
    public class LoginBase
    {
        protected LoginProvider LoginProvider { get; set; }

        public LoginBase(LoginProvider provider)
        {
            this.LoginProvider = provider;
        }

        /// <summary>
        /// 
        /// </summary>
        protected string AuthorizeCode => HttpContext.Current.Request.QueryString["code"];

        /// <summary>
        /// 授权码回调地址
        /// </summary>
        protected string RedirectUri => HttpContext.Current.Request.GetXUri().AbsoluteUri.Split('?')[0];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objStr"></param>
        /// <returns></returns>
        protected JObject Deserialize(string objStr)
        {
            return JsonConvert.DeserializeObject<JObject>(objStr);
        }

    }
}
