﻿namespace Monkey.OAuth
{
    public class LoginProvider
    {
        public string RedirectUrl { get; set; } = string.Empty;
        public string QQClientId { get; set; } = string.Empty;
        public string QQClientSecret { get; set; } = string.Empty;
        public string WeiboClientId { get; set; } = string.Empty;
        public string WeiboClientSecret { get; set; } = string.Empty;
        public string WechatClientId { get; set; } = string.Empty;
        public string WechatClientSecret { get; set; } = string.Empty;
    }
}
