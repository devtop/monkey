﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using Monkey.Kernel;
using Monkey.Properties;
using VaptchaSDK;

namespace Monkey.Controllers
{
    public class CaptchaController : Controller, IControllerFilter
    {
        private static Vaptcha CaptchaVaptcha { get; set; }

        private Dictionary<string, string> GetParms()
        {
            try
            {
                System.IO.Stream s = Request.InputStream;
                int count = 0;
                byte[] buffer = new byte[1024];
                StringBuilder builder = new StringBuilder();
                while ((count = s.Read(buffer, 0, 1024)) > 0)
                {
                    builder.Append(Encoding.UTF8.GetString(buffer, 0, count));
                }

                s.Flush();
                s.Close();
                s.Dispose();
                var str = builder.ToString();
                Dictionary<string, string> parms = new Dictionary<string, string>();
                var index1 = str.IndexOf("\"token\":\"");
                var index2 = str.IndexOf("\"challenge\":\"");
                var index3 = str.IndexOf("\"sceneId\":\"");
                if (index1 != -1 && str[index1 + 9] != '"')
                {
                    var index11 = str.IndexOf('"', index1 + 9);
                    parms.Add("token", str.Substring(index1 + 9, index11 - index1 - 9));
                }
                else
                    parms.Add("token", "");

                if (index2 != -1 && str[index2 + 13] != '"')
                {
                    var index21 = str.IndexOf('"', index2 + 13);
                    parms.Add("challenge", str.Substring(index2 + 13, index21 - index2 - 13));
                }
                else
                    parms.Add("challenge", "");

                if (index3 != -1 && str[index3 + 11] != '"')
                {
                    var index31 = str.IndexOf('"', index3 + 11);
                    parms.Add("sceneId", str.Substring(index3 + 11, index31 - index3 - 11));
                }
                else
                    parms.Add("sceneId", "");

                return parms;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        /// <summary>
        /// Vaptcha
        /// </summary>
        public void Init()
        {
            RouteConfig.ExcludeRouteRules["^captcha"] = true;
            RouteConfig.IgnoreRouteRules["^captcha"] = true;

            var vid = M.GetConfig("vaptcha", "vid", "");
            var key = M.GetConfig("vaptcha", "key", "");

            CaptchaVaptcha = new Vaptcha(vid, key);
        }

        /// <summary>
        /// Vaptcha 宕机
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public ActionResult GetVaptchaDownTime(string data, string callback = "")
        {
            var dto = CaptchaVaptcha.DownTime(data);
            if (string.IsNullOrEmpty(callback))
            {
                switch (dto.state)
                {
                    case DwonTimeState.DownTime:
                        return dto.downtime.ToJsonResult();
                    case DwonTimeState.Signature:
                        return dto.signature.ToJsonResult();
                    case DwonTimeState.DownTimeCheck:
                        return dto.downtimecheck.ToJsonResult();
                    default:
                        return dto.error.ToJsonResult();
                }
            }

            switch (dto.state)
            {
                case DwonTimeState.Signature:
                    return ResultConvert.Content($"{callback}({dto.signature.ToJsonResult()})");
                case DwonTimeState.DownTime:
                    return ResultConvert.Content($"{callback}({dto.downtime.ToJsonResult()})");
                case DwonTimeState.DownTimeCheck:
                    return ResultConvert.Content($"{callback}({dto.downtimecheck.ToJsonResult()})");
                default:
                    return ResultConvert.Content($"{callback}({dto.error.ToJsonResult()})");
            }
        }

        /// <summary>
        /// Vaptcha 获取
        /// </summary>
        /// <returns></returns>
        public ActionResult GetVaptchaChallenge()
        {
            var dto = CaptchaVaptcha.GetChallenge();
            return dto.isdowntime ? dto.downtime.ToJsonResult() : dto.vaptcha.ToJsonResult();
        }

        /// <summary>
        /// Vaptcha 验证
        /// </summary>
        /// <returns></returns>
        public ActionResult VaptchaValidate()
        {
            //获取post json数据
            var context = GetParms();
            if (context == null)
            {
                return ResultConvert.Respond(102, "参数异常");
            }
            //获取流水号字段
            string challenge = context["challenge"];
            //获取token字段
            string token = context["token"];
            //获取场景字段
            string sceneId = context["sceneId"];
            //二次验证获取结果
            var result = CaptchaVaptcha.Validate(challenge, token, sceneId);
            Session["captchapass"] = result;
            Session["captchatoken"] = token;
            return result ? ResultConvert.Respond() : ResultConvert.Respond(101, "验证失败");
        }

        /// <summary>
        /// Vaptcha 资源
        /// </summary>
        /// <returns></returns>
        public ActionResult Vaptcha(string js = null, string html = null)
        {
            if (!string.IsNullOrEmpty(html))
            {
                return ResultConvert.Content(Resources.vaptchav_html);
            }

            if (!string.IsNullOrEmpty(js))
            {
                return ResultConvert.Content(Resources.vaptchav_js);
            }

            return HttpNotFound();
        }

    }
}
