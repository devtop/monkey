﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Donkey.Data;
using Monkey.Component;
using Monkey.Kernel;
using Monkey.Kernel.AutoTask;
using Monkey.Kernel.Data;
using Monkey.Kernel.Quick;
using Monkey.Model;
using Monkey.Properties;
using Monkey.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sharper.ExtensionMethods;

namespace Monkey.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Description("基础页面")]
    public class BasicController : Controller, IControllerFilter
    {
        public void Init()
        {
            Navigation.Link("导航", "/admin/homepage1", "主页一", icon: "icon-paper-plane", path: "/");
            Navigation.Link("导航", "/admin/homepage2", "主页二", icon: "icon-paper-plane", path: "/");

            Navigation.Link("系统", "/b/user", "用户设置", icon: "icon-user");
            Navigation.Link("系统", "/b/system", "系统设置", icon: "icon-settings");
            Navigation.Link("系统", "/b/tool", "系统工具", icon: "icon-wrench");

            Navigation.Link("系统", "/b/user/manager", "用户管理", "/b/user", path: "/Basic/UserManager");
            Navigation.Link("系统", "/b/user/accountrecord", "资金记录", "/b/user", path: "/Basic/UserAccountRecord");
            Navigation.Link("系统", "/b/config/rbac", "权限控制", "/b/system", path: "/Basic/ConfigRoleBasedAccessControl");
            Navigation.Link("系统", "/b/config/manager", "配置参数", "/b/system", path: "/Basic/ConfigManager");
            Navigation.Link("系统", "/b/tool/dbprocesslist", "数据连接", "/b/tool", path: "/Basic/ToolDbProcessList");
        }


        [Description("首页")]
        [Route("admin")]
        public ActionResult Index()
        {
            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            return ResultConvert.View("index");
        }

        [Description("控制台")]
        [Route("admin/console")]
        public ActionResult console()
        {
            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            return ResultConvert.View("ui/page.console");
        }

        [Description("主页一")]
        [Route("admin/homepage1")]
        public ActionResult homepage1()
        {
            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            return ResultConvert.View("ui/page.homepage1");
        }

        [Description("主页二")]
        [Route("admin/homepage2")]
        public ActionResult homepage2()
        {
            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            return ResultConvert.View("ui/page.homepage2");
        }

        [Description("登入")]
        [Route("signin")]
        public ActionResult Signin()
        {
            Session["loguser"] = null;

            var eventArgs = new HookEventArgs();
            if (Request.HttpMethod == "POST")
            {
                #region 

                var form = new WebForm(Request);
                form.ColumnFields.Add(new Field("username", "用户名或邮箱", required: true));
                form.ColumnFields.Add(new Field("password", "密码", required: true));
                form.LoadFieldData();
                if (!form.Validate())
                {
                    return ResultConvert.Respond(101, form.GetValidateError());
                }

                var signinaddr = Request.GetXUserHostAddress();

                eventArgs = new HookEventArgs();
                eventArgs.DP.form = form.ColumnNameValueCollection;
                eventArgs.DP.addr = signinaddr;
                eventArgs.DP.code = 0;
                eventArgs.DP.msg = "";

                if (!Hook.Hander(HookEventPoint.Signin, this, eventArgs))
                {
                    return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg);
                }

                var preuser = MonkeyUser.Get(form.GetString("username"));
                if (preuser == null)
                {
                    return ResultConvert.Respond(102, "账号或密码错误");
                }

                var loginErrorCount = MonkeyUser.Count(preuser.id, "loginErrorCount");
                var needcaptcha = M.GetConfig("system", "signin", true) && loginErrorCount > 3;
                if (needcaptcha && Session["captchapass"] == null)
                {
                    return ResultConvert.Respond(104, "请进行人机验证", new { captcha = true });
                }
                if (needcaptcha && !(bool)Session["captchapass"])
                {
                    return ResultConvert.Respond(105, "人机验证错误", new { captcha = true });
                }

                Session["captchapass"] = null;

                var loguser = MonkeyUser.Signin(form.GetString("username"), form.GetString("password"), signinaddr);
                if (loguser == null)
                {
                    MonkeyUser.Count(preuser.id, "loginErrorCount", 1);
                    return ResultConvert.Respond(102, "账号或密码错误", new { captcha = needcaptcha });
                }

                if (loguser.status != "available")
                {
                    return ResultConvert.Respond(103, "账号被禁用，请联系管理员");
                }

                MonkeyUser.ResetCount(preuser.id, "loginErrorCount");

                eventArgs = new HookEventArgs();
                eventArgs.DP.user = loguser;
                eventArgs.DP.redirect = Request["ref"] ?? M.GetConfig("system", "signined_redirect", "/admin");
                eventArgs.DP.code = 0;
                eventArgs.DP.msg = "";

                if (!Hook.Hander(HookEventPoint.Signined, this, eventArgs))
                {
                    return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg);
                }

                Session[$"{loguser.id}"] = loguser;
                Session["loguser"] = loguser;
                Session["uid"] = loguser.id;
                Session["username"] = loguser.username;
                Session["usergroup"] = loguser.usergroup;

                return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg, obj: new
                {
                    redirect = eventArgs.DP.redirect
                });

                #endregion
            }

            var oauth = Request["oauth"];
            var timestamp = Request["timestamp"];
            var sign = Request["sign"];
            if (!(string.IsNullOrEmpty(oauth) || string.IsNullOrEmpty(timestamp) || string.IsNullOrEmpty(sign)))
            {
                try
                {
                    var json = Encoding.UTF8.GetString(Convert.FromBase64String(oauth));
                    var vsign = $"{oauth}{timestamp}".ComputePassHash();
                    if (vsign != sign)
                    {
                        return ResultConvert.Respond(105, "oauth签名错误");
                    }

                    if (DateTime.Now.AddHours(-1).GetUnixTimestamp() > Convert.ToInt32(timestamp))
                    {
                        return ResultConvert.Respond(106, "oauth 超时");
                    }

                    var oauthobj = JsonConvert.DeserializeObject<OAuth.AuthorizeResult>(json);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            #region 

            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            #endregion

            return ResultConvert.View("ui/page.login.cshtml");
        }

        [Description("注册")]
        [Route("signup")]
        public ActionResult Signup()
        {
            var eventArgs = new HookEventArgs();
            if (Request.HttpMethod == "POST")
            {
                #region 

                var form = new WebForm(Request);
                form.ColumnFields.Add(new Field("email", "邮箱", required: true, pattern: "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$"));
                form.ColumnFields.Add(new Field("password", "密码", required: true));
                form.ColumnFields.Add(new Field("username", "用户名", required: true));
                form.LoadFieldData();

                if (!form.Validate())
                {
                    return ResultConvert.Respond(1, form.GetValidateError());
                }

                var signinaddr = Request.GetXUserHostAddress();

                eventArgs = new HookEventArgs();
                eventArgs.DP.form = form.ColumnNameValueCollection;
                eventArgs.DP.addr = signinaddr;
                eventArgs.DP.code = 0;
                eventArgs.DP.msg = "";

                if (!Hook.Hander(HookEventPoint.Signup, this, eventArgs))
                {
                    return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg);
                }

                var needcaptcha = M.GetConfig("system", "signup", true);
                if (needcaptcha && Session["captchapass"] == null)
                {
                    return ResultConvert.Respond(104, "请进行人机验证", new { captcha = true });
                }
                if (needcaptcha && !(bool)Session["captchapass"])
                {
                    return ResultConvert.Respond(105, "人机验证错误", new { captcha = true });
                }
                Session["captchapass"] = null;


                if (MonkeyUser.Exist(email: form.GetString("email")))
                {
                    return ResultConvert.Respond(102, "邮箱重复");
                }

                if (MonkeyUser.Exist(form.GetString("username")))
                {
                    return ResultConvert.Respond(103, "用户名重复");
                }

                eventArgs = new HookEventArgs();
                eventArgs.DP.form = form.NameValueCollection;
                eventArgs.DP.redirect = "/signin";
                eventArgs.DP.code = 0;
                eventArgs.DP.msg = "";

                if (!Hook.Hander(HookEventPoint.Signuped, this, eventArgs))
                {
                    return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg);
                }

                var usergroup = M.GetConfig("system", "SignupUsergroup", "user");
                MonkeyUser.Signup(form.GetString("username"), form.GetString("email"), form.GetString("password"), usergroup);

                return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg, obj: new { redirect = eventArgs.DP.redirect });

                #endregion
            }

            #region 

            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            #endregion

            return ResultConvert.View("core/basic/signup");
        }

        [Description("重置密码,send:发送验证码,reset:重置请求")]
        [Route("forgotpwd")]
        public ActionResult Forgotpwd(string method)
        {
            var eventArgs = new HookEventArgs();
            if (Request.HttpMethod == "POST")
            {
                if (method == "send")
                {
                    var form = new WebForm(Request);
                    form.ColumnFields.Add(new Field("username", "用户名或邮箱", required: true));
                    form.LoadFieldData();
                    if (!form.Validate())
                    {
                        return ResultConvert.Respond(1, form.GetValidateError());
                    }

                    var needcaptcha = M.GetConfig("system", "forgotpwd", true);

                    if (needcaptcha && Session["captchapass"] == null)
                    {
                        return ResultConvert.Respond(104, "请进行人机验证", new { captcha = true });
                    }

                    if (needcaptcha && !(bool)Session["captchapass"])
                    {
                        return ResultConvert.Respond(105, "人机验证错误", new { captcha = true });
                    }

                    Session["captchapass"] = null;

                    var username = (string)form["username"].GetValue();
                    var token = (string)Session["captchatoken"];

                    using (var db = new MonkeyEntities())
                    {
                        var user = db.monkey_user.FirstOrDefault(m => m.username == username);
                        if (user == null)
                        {
                            return ResultConvert.Respond(106, "用户不存在", new { captcha = needcaptcha });
                        }

                        var phone = MonkeyUser.GetMeta(user.id, "", "");
                        if (string.IsNullOrEmpty(phone))
                        {
                            return ResultConvert.Respond(106, "暂未绑定手机号码，请联系客服", new { captcha = needcaptcha });
                        }

                        var code = Vaptcha.VerifyCode(token, phone);
                        code = code == 200 ? 0 : code;
                        return ResultConvert.Respond(code);

                    }


                }

                if (method == "reset")
                {
                    var form = new WebForm(Request);
                    form.ColumnFields.Add(new Field("username", "用户名或邮箱", required: true));
                    form.ColumnFields.Add(new Field("vcode", "验证码", required: true, pattern: "^[a-zA-Z0-9]{4,6}$"));
                    form.ColumnFields.Add(new Field("password", "密码", required: true, pattern: "^.{6,32}$"));
                    form.ColumnFields.Add(new Field("password2", "密码", required: true));
                    form.LoadFieldData();
                    if (!form.Validate())
                    {
                        return ResultConvert.Respond(1, form.GetValidateError());
                    }

                    var needcaptcha = M.GetConfig("system", "forgotpwd", true);

                    if (needcaptcha && Session["captchapass"] == null)
                    {
                        return ResultConvert.Respond(104, "请进行人机验证", new { captcha = true });
                    }

                    if (needcaptcha && !(bool)Session["captchapass"])
                    {
                        return ResultConvert.Respond(105, "人机验证错误", new { captcha = true });
                    }

                    Session["captchapass"] = null;

                    var username = (string)form["username"].GetValue();
                    var vcode = (string)form["vcode"].GetValue();
                    var password = (string)form["password"].GetValue();
                    var password2 = (string)form["password2"].GetValue();

                    if (password != password2)
                    {
                        return ResultConvert.Respond(106, "两次密码输入错误");
                    }

                    using (var db = new MonkeyEntities())
                    {
                        var user = db.monkey_user.FirstOrDefault(m => m.username == username);
                        if (user == null)
                        {
                            return ResultConvert.Respond(106, "用户不存在", new { captcha = needcaptcha });

                        }


                        var phone = MonkeyUser.GetMeta(user.id, "", "");
                        if (string.IsNullOrEmpty(phone))
                        {
                            return ResultConvert.Respond(106, "暂未绑定手机号码，请联系客服", new { captcha = needcaptcha });
                        }

                        if (!Vaptcha.Verify(phone, vcode))
                        {
                            return ResultConvert.Respond(106, "验证码输入错误");
                        }

                        user.password = password.ComputePassHash();
                        db.SaveChanges();
                    }

                    return ResultConvert.Respond();
                }

                return null;
            }

            #region 

            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            #endregion

            return ResultConvert.View("core/basic/forgotpwd");
        }

        [Description("登出")]
        [Route("logout")]
        public ActionResult Logout()
        {
            var eventArgs = new HookEventArgs();
            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            eventArgs = new HookEventArgs();
            eventArgs.DP.user = Session["loguser"] as monkey_user;
            eventArgs.DP.redirect = "/signin";
            if (Hook.Hander(HookEventPoint.Logout, this, eventArgs))
            {
                Session["loguser"] = null;
            }
            return new RedirectResult(eventArgs.DP.redirect);
        }

        [Description("上传文件")]
        [Route("uploadfile")]
        public ActionResult UploadFile(bool rc = false, bool scratchfile = false, bool base64 = false, string fname = "", string fcontent = "")
        {
            var uploadFileLengthLimited = M.GetConfig("system", "UploadFileLengthLimited", 4194304);
            var uploadFileExtensionLimited = M.GetConfig("system", "UploadFileExtensionLimited", "zip,7z,rar,bmp,jpg,png,tiff,gif,pcx,tga,exif,fpx,svg,psd,cdr,pcd,dxf,ufo,eps,ai,raw,wmf".Split(','));
            var uploadFileBaseDir = M.GetConfig("system", "UploadFileBaseDir", "");
            var uploadFileBaseUrl = M.GetConfig("system", "UploadFileBaseUrl", "");
            var uploadFileDir = M.GetConfig("system", "UploadFileDir", "/uploads");
            var time = DateTime.Now;
            var tmpdir = $"{uploadFileDir}/{time:yy/MM/dd}";
            var pname = $"{fname}{Guid.NewGuid()}{time.Ticks}".ToHash();
            var fdir = string.IsNullOrEmpty(uploadFileBaseDir) ? Request.MapPath(tmpdir) : $"{uploadFileBaseDir}{tmpdir}";
            var fkey = "";
            var fextension = "";
            var flength = 0L;

            if (!Directory.Exists(fdir))
            {
                Directory.CreateDirectory(fdir);
            }

            if (base64)
            {
                #region


                fextension = fname.Split('.').LastOrDefault()?.ToLower();
                if (uploadFileExtensionLimited.All(m => m != fextension))
                {
                    return ResultConvert.Json(new { code = 103, status = 103, msg = "文件类型暂不支持" });
                }
                if (fcontent.Length > uploadFileLengthLimited)
                {
                    return ResultConvert.Json(new { code = 104, status = 104, msg = "文件大小超过限制" });
                }

                pname = $"{pname}.{fextension}";
                var filepath = Path.Combine(fdir, pname);
                System.IO.File.WriteAllBytes(filepath, Convert.FromBase64String(fcontent));
                var fileinfo = new FileInfo(filepath);
                flength = fileinfo.Length;

                #endregion
            }
            else
            {
                #region 

                if (Request.Files.Count == 0)
                {
                    return ResultConvert.Json(new { code = 101, status = 101, msg = "上传的文件呢？" });
                }

                var file = Request.Files[0];
                if (string.IsNullOrEmpty(file?.FileName))
                {
                    return ResultConvert.Json(new { code = 102, status = 102, msg = "文件类型暂不支持" });
                }

                fextension = file.FileName?.Split('.').LastOrDefault()?.ToLower();
                if (uploadFileExtensionLimited.All(m => m != fextension))
                {
                    return ResultConvert.Json(new { code = 103, status = 103, msg = "文件类型暂不支持" });
                }

                if (file.ContentLength > uploadFileLengthLimited)
                {
                    return ResultConvert.Json(new { code = 104, status = 104, msg = "文件大小超过限制" });
                }
                pname = $"{pname}.{fextension}";
                var filepath = Path.Combine(fdir, pname);
                file.SaveAs(filepath);

                fname = file.FileName;
                flength = file.ContentLength;
                fkey = Request.Files.AllKeys[0];

                #endregion
            }

            var result = new FileItem
            {
                key = $"{fkey}",
                name = $"{fname}",
                src = $"{tmpdir}/{pname}",
                link = $"{uploadFileBaseUrl}{tmpdir}/{pname}",
                extension = fextension,
                length = flength
            };
            return rc ? ResultConvert.Json(new { code = 0, status = 0, obj = result, }) : ResultConvert.Json(result);
        }

        [Description("删除文件")]
        [Route("deletefile")]
        public ActionResult DeleteFile(string src, bool rc = false)
        {
            var uploadFileBaseDir = M.GetConfig("system", "UploadFileBaseDir", "");
            var filename = string.IsNullOrEmpty(uploadFileBaseDir) ? Request.MapPath($"~/{src.TrimStart('/')}") : $"{uploadFileBaseDir.TrimEnd('/')}/{src.TrimStart('/')}";
            if (System.IO.File.Exists(filename))
            {
                System.IO.File.Delete(filename);
                return ResultConvert.Respond();
            }
            return ResultConvert.Respond(101, "文件删除失败");
        }

        [Description("信息,explorer:资源,tasklist:任务列表")]
        [Route("b/info/{method}")]
        public ActionResult Info(string method)
        {
            switch (method)
            {
                case "explorer":
                    var uploadFileStat = MonkeyInfo.UploadFileStat;
                    var totalUseSpace = Math.Round(MonkeyInfo.TotalSize - MonkeyInfo.TotalFreeSpace, 2);
                    var totalFreeSpace = Math.Round(MonkeyInfo.TotalFreeSpace, 2);
                    return ResultConvert.Respond(obj: new
                    {
                        chart = MonkeyInfo.GetHistoryData(),
                        tasks = AutoTaskDispatcher.AutoTasks.OrderBy(m => m.NextExecuteTime).Select(m => new
                        {
                            Name = m.Name,
                            Enable = m.Enable,
                            ExecuteCount = m.ExecuteCount,
                            ExecuteError = m.ExecuteError,
                            ExecuteTime = m.ExecuteTime,
                            Interval = m.Interval,
                            MaxExecuteTime = m.MaxExecuteTime,
                            LastExecuteTime = m.LastExecuteTime.ToString("yy-MM-dd HH:mm:ss"),
                            NextExecuteTime = m.NextExecuteTime.ToString("yy-MM-dd HH:mm:ss"),
                        }).ToArray(),
                        stat = new
                        {
                            upload = uploadFileStat,
                            useSpace = totalUseSpace,
                            freeSpace = totalFreeSpace,
                        }
                    });
                case "tasklist":
                    return ResultConvert.Respond(obj: AutoTaskDispatcher.AutoTasks);
                default:
                    return ResultConvert.Respond(101);
            }
        }

        [Description("页面")]
        [Route("b/{name}")]
        public ActionResult Page(string name)
        {
            return ResultConvert.View($"core/basic/page/{name}");
        }

        [Description("用户主页")]
        [Route("b/user/profile")]
        public ActionResult UserProfile()
        {
            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            return ResultConvert.View("core/basic/user/profile");
        }

        [Description("用户设置,modifypassword:修改密码,modifymeta:修改信息")]
        [Route("b/user/setting/{method?}")]
        public ActionResult UserSettings(string method = "")
        {
            var uid = (int)Session["uid"];
            var eventArgs = new HookEventArgs();

            eventArgs.DP.uid = uid;
            eventArgs.DP.url = "";

            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            var forms = new List<WebForm>();
            var avatar = MonkeyUser.GetMeta<JObject>(uid, "avatar");

            var metaform = new WebForm(Request);
            metaform.PrimaryTitle = "修改信息";
            metaform.PrimaryButtonText = "保存";
            metaform.ColumnFields.Add(new Field("phone", "手机", FieldType.Number, required: false, value: MonkeyUser.GetMeta(uid, "phone", "")));
            metaform.ColumnFields.Add(new Field("identitycard", "身份证", FieldType.Text, required: false, value: MonkeyUser.GetMeta(uid, "identitycard", "")));
            metaform.ColumnFields.Add(new Field("avatar", "头像", FieldType.Image, required: false, value: JsonConvert.SerializeObject(avatar))
            {
                StringStringD = new Dictionary<string, string>
                {
                    {"aspect_ratio","1" },
                },
            });
            metaform.NameValueCollection["method"] = "modifymeta";
            forms.Add(metaform);

            var passform = new WebForm(Request);
            passform.PrimaryTitle = "修改密码";
            passform.PrimaryButtonText = "保存";
            passform.ColumnFields.Add(new Field("oldpassword", "原始密码", FieldType.Password, required: true));
            passform.ColumnFields.Add(new Field("newpassword", "新密码", FieldType.Password, required: true));
            passform.ColumnFields.Add(new Field("newpassword2", "确认密码", FieldType.Password, required: true));
            passform.NameValueCollection["method"] = "modifypassword";
            forms.Add(passform);


            switch (method)
            {
                case "modifypassword":

                    #region 

                    passform.LoadFieldData();
                    if (!passform.Validate())
                    {
                        return ResultConvert.Respond(101, passform.GetValidateError());
                    }

                    var oldpassword = passform.GetString("oldpassword");
                    var newpassword = passform.GetString("newpassword");
                    var newpassword2 = passform.GetString("newpassword2");

                    if (newpassword != newpassword2)
                    {
                        return ResultConvert.Respond(102, "两次密码不一致");
                    }

                    eventArgs = new HookEventArgs();
                    eventArgs.DP.user = Session["loguser"] as monkey_user;
                    eventArgs.DP.form = metaform.ColumnNameValueCollection;
                    eventArgs.DP.code = 0;
                    eventArgs.DP.msg = "";
                    if (!Hook.Hander(HookEventPoint.ModifyUserPassword, this, eventArgs))
                    {
                        return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg);
                    }
                    oldpassword = oldpassword.ComputePassHash();
                    newpassword = newpassword.ComputePassHash();
                    using (var db = new MonkeyEntities())
                    {
                        var user = db.monkey_user.Single(m => m.id == uid);
                        if (user.password != oldpassword)
                        {
                            return ResultConvert.Respond(102, "原始密码错误");
                        }
                        user.password = newpassword;
                        db.SaveChanges();
                        return ResultConvert.Respond(0, "修改成功");
                    }

                #endregion

                case "modifymeta":

                    #region 

                    metaform.LoadFieldData();
                    if (!metaform.Validate())
                    {
                        return ResultConvert.Respond(101, metaform.GetValidateError());
                    }

                    eventArgs = new HookEventArgs();
                    eventArgs.DP.user = Session["loguser"] as monkey_user;
                    eventArgs.DP.form = metaform.ColumnNameValueCollection;
                    eventArgs.DP.code = 0;
                    eventArgs.DP.msg = "";
                    if (!Hook.Hander(HookEventPoint.ModifyUserMeta, this, eventArgs))
                    {
                        return ResultConvert.Respond(eventArgs.DP.code, eventArgs.DP.msg);
                    }

                    MonkeyUser.SetMeta(uid, "phone", metaform.GetString("phone"));
                    MonkeyUser.SetMeta(uid, "identitycard", metaform.GetString("identitycard"));
                    MonkeyUser.SetMeta(uid, "avatar", (JObject)metaform.GetFieldData("avatar"));
                    return ResultConvert.Respond(0, "修改成功");

                    #endregion

            }

            ViewData["forms"] = forms;
            return ResultConvert.View("core/basic/user/setting", ViewData);
        }

        [Description("用户资金,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        [Route("b/user/account/{method?}")]
        public ActionResult UserAccount(string method)
        {
            var uid = (int)Session["uid"];

            var eventArgs = new HookEventArgs();
            eventArgs.DP.uid = uid;
            eventArgs.DP.url = "";

            var redirectUrl = Hook.RedirectEvent(this);
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            var curd = new CURD(Request, Response, method);
            curd.AddField("id", "编号");
            curd.AddField("type", "类型", FieldType.Select, required: true, config: ":请选择,p:收入,r:支出");
            curd.AddField("change_amount", "金额");
            curd.AddField("before_change", "变动前");
            curd.AddField("after_change", "余额");
            curd.AddField("title", "标题", width: 200);
            curd.AddField("content", "内容", width: 400);
            curd.AddField("status", "状态", FieldType.Select, required: true, config: ":请选择,fail:失败,success:成功");
            curd.AddField("create_time", "时间", width: 200, type: FieldType.DateTime);
            curd.AddField("other1", "信息1", width: 200);
            curd.AddField("other2", "信息2", width: 200);
            curd.AddField("other3", "信息3", width: 200);

            curd.Copy("R", "id");
            curd.Copy("R", "type");
            curd.Copy("R", "title");
            curd.Copy("R", "change_amount");
            curd.Copy("R", "after_change");
            curd.Copy("R", "status");
            curd.Copy("R", "other1");
            curd.Copy("R", "other2");
            curd.Copy("R", "other3");
            curd.Copy("R", "content");
            curd.Copy("R", "create_time");

            curd.Copy("F", "type", required: false);
            curd.Copy("F", "title", required: false);
            curd.Copy("F", "content", required: false);
            curd.Copy("F", "status", required: false);
            curd.Copy("F", "change_amount", required: false);
            curd.Copy("F", "create_time", required: false, type: FieldType.DateRanges);
            curd.Copy("F", "other1");
            curd.Copy("F", "other2");
            curd.Copy("F", "other3");

            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_account_log", support: "RE");
            curd.Quick.MustQueryWhere = $"WHERE `uid` = {uid}";
            curd.Quick.RetrieveBefore = (sender, args) =>
            {
                using (var db = new MonkeyEntities())
                {
                    var changeAmount = db.Database.SqlQuery<decimal?>($"SELECT SUM(change_amount) {args.Sql}").FirstOrDefault();
                    args.RetrieveObj.Stat.Add($"合计金额：{changeAmount ?? 0}");
                }
            };
            return curd.View();
        }

        [Description("资金记录,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        [Route("b/user/accountrecord/{method?}")]
        public ActionResult UserAccountRecord(string method)
        {
            var uid = (int)Session["uid"];

            var curd = new CURD(Request, Response, method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_account_log", support: "R");
            curd.Quick.RetrieveBefore = (sender, args) =>
            {
                using (var db = new MonkeyEntities())
                {
                    var changeAmount = db.Database.SqlQuery<decimal?>($"SELECT SUM(change_amount) {args.Sql}").FirstOrDefault();
                    args.RetrieveObj.Stat.Add($"合计金额：{changeAmount ?? 0}");
                }
            };

            curd.Quick.RetrieveItemBefore = (sender, args) =>
            {
                if (args.field.Name == "uid")
                {
                    var v = args.field.GetValue();
                    var kv = args.field.StringStringD.FirstOrDefault(m => m.Value == v);
                    var tsql = args.field.RetrieveSql.Replace("{key}", args.field.Name).Replace("{value}", kv.Key);
                    return tsql;
                }
                return args.Sql;
            };

            curd.AddField("id", "编号");
            curd.AddField("uid", "用户");
            curd.AddField("type", "类型", FieldType.Text, required: true);
            curd.AddField("change_amount", "金额");
            curd.AddField("before_change", "变动前");
            curd.AddField("after_change", "余额");
            curd.AddField("title", "标题", width: 200);
            curd.AddField("content", "内容", width: 400);
            curd.AddField("status", "状态", FieldType.Select, required: true, config: ":请选择,fail:失败,success:成功");
            curd.AddField("create_time", "时间", width: 200, type: FieldType.DateTime);
            curd.AddField("other1", "信息1", width: 200);
            curd.AddField("other2", "信息2", width: 200);

            curd.Copy("R", "id");
            curd.Copy("R", "uid");
            curd.Copy("R", "type");
            curd.Copy("R", "title");
            curd.Copy("R", "change_amount");
            curd.Copy("R", "after_change");
            curd.Copy("R", "status");
            curd.Copy("R", "other1");
            curd.Copy("R", "other2");
            curd.Copy("R", "content");
            curd.Copy("R", "create_time");

            curd.Copy("F", "uid", required: false, type: FieldType.Text);
            curd.Copy("F", "type", required: false);
            curd.Copy("F", "title", required: false);
            curd.Copy("F", "content", required: false);
            curd.Copy("F", "status", required: false);
            curd.Copy("F", "change_amount", required: false);
            curd.Copy("F", "create_time", required: false, type: FieldType.DateRanges);
            curd.Copy("F", "other1");
            curd.Copy("F", "other2");


            return curd.View();
        }

        [Description("用户管理,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        [Route("b/user/manager/{method?}")]
        public ActionResult UserManager(string method)
        {
            var curd = new CURD(Request, Response, method: method);

            curd.AddField("id", "用户编号");
            curd.AddField("username", "登录账号", required: true);
            curd.AddField("email", "邮箱", required: true);
            curd.AddField("password", "登录密码", FieldType.Password, required: true);
            curd.AddField("usergroup", "用户组", FieldType.SelectAny, required: true, config: MonkeyTerm.GetList("usergroup").Aggregate(":请选择,", (x, y) => $"{x},{y.term_name}:{y.term_text}"));
            curd.AddField("status", "状态", FieldType.Select, required: true, config: ":请选择,available:可用,disable:禁用");
            curd.AddField("signin_addr", "登入地址");
            curd.AddField("signin_date", "登入时间", width: 200, type: FieldType.DateTime);
            curd.AddField("signup_date", "注册时间", width: 200, type: FieldType.DateTime);

            //设置检索字段
            curd.Copy("R", "id", required: false);
            curd.Copy("R", "username", required: false);
            curd.Copy("R", "email", required: false);
            curd.Copy("R", "usergroup", required: false);
            curd.Copy("R", "status", required: false);
            curd.Copy("R", "signin_date", required: false);
            curd.Copy("R", "signup_date", required: false);

            //设置检索过滤字段
            curd.Copy("F", "email", required: false);
            curd.Copy("F", "username", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "usergroup", required: false);
            curd.Copy("F", "status", required: false);
            curd.Copy("F", "signin_date", type: FieldType.DateRanges, required: false);
            curd.Copy("F", "signup_date", type: FieldType.DateRanges, required: false);

            //设置添加字段
            curd.Copy("C", "usergroup", required: true);
            curd.Copy("C", "username", required: true);
            curd.Copy("C", "email", required: true);
            curd.Copy("C", "password", required: true).GetValueAfter = (sender, args) =>
            {
                args.StringValue = args.StringValue.ComputePassHash();
                args.DynamicValue = args.StringValue;
            };
            curd.Copy("C", "status", required: true);

            //设置编辑字段
            curd.Copy("U", "usergroup", required: true);
            curd.Copy("U", "status", required: true);
            var passwordField = curd.Copy("U", "password", required: false);
            passwordField.NullNotUpdate = true;
            passwordField.GetValueAfter = (sender, args) =>
            {
                args.StringValue = args.StringValue.ComputePassHash();
                args.DynamicValue = args.StringValue;
            };

            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_user");

            //var custom_fields = M.GetConfig("system", "manageuser_custom_fields", new List<Field>());
            //设置添加字段
            //curd.CopyFields(curd.CreateObject.ColumnFields, null, "username", "email", "password", "usergroup", "status");
            //curd.CreateObject.ColumnFields.AddRange(custom_fields);

            return curd.View();
        }

        [Description("权限控制,get_usergroup:获取用户组,delete_usergroup:删除用户组,add_usergroup:添加用户组,get_usergroup_route:读取路由,save_usergroup_route:保存路由")]
        [Route("b/config/rbac/{method?}")]
        public ActionResult ConfigRoleBasedAccessControl(string method)
        {
            var form = new WebForm(Request);
            using (var db = new MonkeyEntities())
            {

                switch (method)
                {
                    case "save_usergroup_route":
                        {
                            form.ColumnFields.Add(new Field("id", "用户组编号", required: true));
                            form.ColumnFields.Add(new Field("routes", "路由规则", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            else
                            {
                                var routes = new Dictionary<string, bool>();
                                foreach (var route in JsonConvert.DeserializeObject<Dictionary<string, bool>>(form.GetString("routes")))
                                {
                                    var sc = route.Key.Trim('/').Split('/');
                                    var baseroute = sc.Take(sc.Length - 1).Aggregate("", (x, y) => $"{x}/{y}");
                                    if (!routes.ContainsKey(baseroute))
                                    {
                                        routes[baseroute] = baseroute.Trim('/').Contains('/');
                                    }
                                    routes[route.Key] = route.Value;
                                }

                                MonkeyTerm.SetMeta(form.GetInt("id"), "routes", routes);
                                return ResultConvert.Respond(0, "操作成功");
                            }
                        }

                    case "get_usergroup_route":
                        {
                            form.ColumnFields.Add(new Field("id", "用户组编号", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            else
                            {
                                var routes = RouteConfig.Routes;
                                var checkedroutes = MonkeyTerm.GetMeta(form.GetInt("id"), "routes", new Dictionary<string, bool>());
                                //var checkeds = new List<string>();
                                var routeMap = new List<Dictionary<string, object>>();

                                foreach (var route in routes)
                                {
                                    var path = route.FullPath.Trim('/').Split('/');
                                    var desc = route.Description.Trim('/').Split('/');



                                    if (path.Length < 1)
                                    {
                                        continue;
                                    }

                                    var p0 = "/".Join(path.Take(1).ToArray());
                                    var d0 = desc[0];
                                    var t0 = routeMap.FirstOrDefault(m => (string)m["id"] == p0);
                                    if (t0 == null)
                                    {
                                        t0 = new Dictionary<string, object>
                                    {
                                        {"id",p0},
                                        {"label",d0},
                                        {"children",new List<Dictionary<string, object>>()},
                                    };
                                        routeMap.Add(t0);
                                    }

                                    if (path.Length < 2)
                                    {
                                        continue;
                                    }

                                    var children1 = (List<Dictionary<string, object>>)t0["children"];
                                    var p1 = "/".Join(path.Take(2).ToArray());
                                    var d1 = desc[1];
                                    var t1 = children1.FirstOrDefault(m => (string)m["id"] == p1);
                                    if (t1 == null)
                                    {
                                        t1 = new Dictionary<string, object>
                                    {
                                        {"id",p1},
                                        {"label",d1},
                                        {"checked",path.Length < 3 && checkedroutes.Any(m => m.Key == route.FullPath)},
                                        {"children",new List<Dictionary<string, object>>()},
                                    };
                                        children1.Add(t1);
                                    }

                                    if (path.Length < 3)
                                    {
                                        continue;
                                    }

                                    var children2 = (List<Dictionary<string, object>>)t1["children"];
                                    var p2 = "/".Join(path.Take(3).ToArray());
                                    var d2 = desc[2];
                                    var t2 = children2.FirstOrDefault(m => (string)m["id"] == p2);
                                    if (t2 == null)
                                    {
                                        t2 = new Dictionary<string, object>
                                    {
                                        {"id",p2},
                                        {"label",d2},
                                        {"checked",checkedroutes.Any(m => m.Key == route.FullPath)},
                                        {"children",new List<Dictionary<string, object>>()},
                                    };
                                        children2.Add(t2);
                                    }
                                }

                                return ResultConvert.Json(new { code = 0, data = routeMap });
                            }
                        }

                    case "delete_usergroup":
                        {
                            form.ColumnFields.Add(new Field("id", "用户组编号", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            var ret = db.Database.ExecuteSqlCommand($"DELETE FROM monkey_term WHERE id = {form.GetInt("id")} AND term_name NOT IN ('administrator','anonymous')");
                            if (ret > 0)
                            {
                                return ResultConvert.Respond(0, "删除成功", new { reload = true });
                            }
                            else
                            {
                                return ResultConvert.Respond(1, "删除失败", new { reload = true });
                            }
                        }

                    case "add_usergroup":
                        {
                            form.ColumnFields.Add(new Field("usergroup_name", "用户组名称", required: true));
                            form.ColumnFields.Add(new Field("usergroup_text", "用户组描述", required: true));
                            form.ColumnFields.Add(new Field("usergroup_group", "用户组分组", required: false));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            if (MonkeyTerm.Exist("usergroup", form.GetString("usergroup_name")))
                            {
                                return ResultConvert.Respond(1, "用户组名称重复");
                            }
                            var termitem = new monkey_term
                            {
                                term_type = "usergroup",
                                term_name = form.GetString("usergroup_name"),
                                term_text = form.GetString("usergroup_text"),
                                term_group = form.GetString("usergroup_group"),
                            };
                            db.monkey_term.Add(termitem);
                            db.SaveChanges();
                            return ResultConvert.Respond(0, "添加成功", new { reload = true });
                        }
                    case "get_usergroup":
                        {
                            var querySql = "SELECT * FROM monkey_term WHERE term_type = 'usergroup'";
                            var termgroup = Regex.Replace(Request["term_group"] ?? "", "['\"]+", "").Trim();
                            if (!string.IsNullOrEmpty(termgroup))
                            {
                                querySql += $" AND term_group = '{termgroup}'";
                            }
                            var usergroups = db.monkey_term.SqlQuery(querySql).ToList().Select(m => new
                            {
                                m.id,
                                m.term_name,
                                m.term_text,
                                m.term_order,
                                m.term_group,
                            }).ToList();
                            return ResultConvert.Json(new
                            {
                                code = 0,
                                msg = "",
                                count = 0,
                                data = usergroups
                            });
                        }

                    default:
                        {
                            var quick = new QuickComplete(db);
                            var fielddata = quick.Query("SELECT term_group as `key`, term_group as `value` FROM monkey_term WHERE term_type = 'usergroup' GROUP BY term_group").ToList();
                            fielddata.Insert(0, new Dictionary<string, object> { { "key", "" }, { "value", "请选择" } });
                            var termGroupField = new Field("term_group", "分组", FieldType.Select, config: fielddata.ToDictionary(m => (string)m["key"], m => (string)m["value"]));
                            ViewData["term_group_field"] = termGroupField;
                            break;
                        }
                }
            }

            return ResultConvert.View("core/basic.rbac.cshtml", ViewData);
        }

        [Description("配置选项,delete:删除,save:保存")]
        [Route("b/config/setup/{method?}")]
        public ActionResult ConfigSetup(string method)
        {
            if (Request.HttpMethod == "POST")
            {
                var form = new WebForm(Request);
                switch (method)
                {
                    case "delete":
                        form.ColumnFields.Add(new Field("metaid", required: true));
                        form.LoadFieldData();
                        if (!form.Validate())
                        {
                            return ResultConvert.Json(new { status = 1, msg = form.GetValidateError() });
                        }
                        using (var db = new MonkeyEntities())
                        {
                            var metaid = form.GetInt("metaid");
                            var termmeta = db.monkey_termmeta.FirstOrDefault(m => m.id == metaid);
                            if (termmeta == null)
                            {
                                return ResultConvert.Respond(1, "编号错误");
                            }
                            db.monkey_termmeta.Remove(termmeta);
                            db.SaveChanges();
                            return ResultConvert.Respond(0, "删除成功", obj: new { reload = true });
                        }

                    case "save":
                        form.ColumnFields.Add(new Field("term_name", required: true));
                        form.ColumnFields.Add(new Field("meta_key", required: true));
                        form.ColumnFields.Add(new Field("meta_type", required: true));
                        form.ColumnFields.Add(new Field("meta_value", required: true));
                        form.LoadFieldData();
                        if (!form.Validate())
                        {
                            return ResultConvert.Json(new { status = 1, msg = form.GetValidateError() });
                        }
                        var term_name = form.GetString("term_name");
                        var meta_key = form.GetString("meta_key");
                        var meta_value = Convert.ToBoolean(Request["stringtype"]) ? form.GetString("meta_value") : JsonConvert.DeserializeObject(form.GetString("meta_value"));
                        M.SetConfig(term_name, meta_key, meta_value);
                        return ResultConvert.Json(new { status = 0, msg = "保存成功" });
                }
            }
            return ResultConvert.View("core/basic/config/setup");
        }

        [Description("配置参数,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        [Route("b/config/manager/{method?}")]
        public ActionResult ConfigManager(string method)
        {
            var curd = new CURD(Request, Response, method: method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_termmeta", sort: "ORDER BY `meta_key`")
            {
            };

            curd.AddField("id", "编号");
            curd.AddField("termid", "分类", FieldType.Select, required: true, config: curd.Quick.GetSelectStringData("monkey_term", seed: "", valueField: "term_name", appendSql: " AND term_type = 'config'"));
            curd.AddField("meta_type", "类型", FieldType.Select, required: true, config: "json:JSON,xml:XML");
            curd.AddField("meta_key", "键", FieldType.Text, required: true);
            curd.AddField("meta_value", "值", FieldType.Textarea, required: true);
            curd.AddField("meta_desc", "描述", FieldType.Textarea, required: true);

            //设置检索字段
            curd.Copy("R", "id", required: false);
            curd.Copy("R", "termid", required: false);
            curd.Copy("R", "meta_type", required: false);
            curd.Copy("R", "meta_key", required: false);
            curd.Copy("R", "meta_value", required: false);
            curd.Copy("R", "meta_desc", required: false);

            //设置检索过滤字段
            curd.Copy("F", "termid", required: false);
            curd.Copy("F", "meta_type", required: false);
            curd.Copy("F", "meta_key", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "meta_value", type: FieldType.Text, required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "meta_desc", type: FieldType.Text, required: false).RetrieveSql = "AND `{key}` like '%{value}%'";

            //设置添加字段
            curd.Copy("C", "termid", required: true);
            curd.Copy("C", "meta_type", required: true);
            curd.Copy("C", "meta_key", required: true);
            curd.Copy("C", "meta_value", required: true);
            curd.Copy("C", "meta_desc", required: false);

            //设置编辑字段
            curd.Copy("U", "termid", required: true);
            curd.Copy("U", "meta_type", required: true);
            curd.Copy("U", "meta_key", required: true);
            curd.Copy("U", "meta_value", required: true);
            curd.Copy("U", "meta_desc", required: false);

            return curd.View();
        }

        [Description("数据库进程列表,retrieve:检索,kill:杀死")]
        [Route("b/tool/dbprocesslist/{method?}")]
        public ActionResult ToolDbProcessList(string method)
        {
            var curd = new CURD(Request, Response, method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "`information_schema`.`PROCESSLIST`", support: "RE");
            curd.RetrieveObj.PrimaryKey = "ID";

            curd.AddField("ID", "编号");
            curd.AddField("USER", "用户");
            curd.AddField("HOST", "主机");
            curd.AddField("DB", "数据库");
            curd.AddField("COMMAND", "命令");
            curd.AddField("TIME", "时间");
            curd.AddField("STATE", "状态");
            curd.AddField("INFO", "信息", width: 200);

            curd.Copy("R", "ID");
            curd.Copy("R", "USER");
            curd.Copy("R", "HOST");
            curd.Copy("R", "DB");
            curd.Copy("R", "COMMAND");
            curd.Copy("R", "TIME");
            curd.Copy("R", "STATE");
            curd.Copy("R", "INFO");

            curd.Copy("F", "USER", required: false);
            curd.Copy("F", "HOST", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "DB", required: false);
            curd.Copy("F", "COMMAND", required: false);
            curd.Copy("F", "TIME", required: false).RetrieveSql = "AND `{key}` >= '{value}'";
            curd.Copy("F", "STATE", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "INFO", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";

            curd.TodoCollection["kill"] = new Todo("杀死", todo =>
            {
                foreach (var id in todo.IDs)
                {
                    try
                    {
                        curd.Quick.ExecuteNonQuery($"KILL {id}");
                    }
                    catch (Exception ex)
                    {
                        return ResultConvert.Respond(1, ex.ToString());
                    }
                }
                return ResultConvert.Respond(0, "成功");
            });

            return curd.View();
        }

        [Description("安装")]
        [Route("b/install")]
        public ActionResult Install()
        {
            var lockfile = Server.MapPath("install.lock");
            if (System.IO.File.Exists(lockfile))
            {
                return ResultConvert.Respond(101, "Please delete root 'install.lock'");
            }

            using (var db = new MonkeyEntities())
            {
                db.Database.ExecuteSqlCommand(Resources.INIT_SQL);
                System.IO.File.Create(lockfile);
                return ResultConvert.Respond();
            }
        }

    }
}