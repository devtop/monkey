﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Donkey.Data;
using Monkey.Kernel;
using Monkey.OAuth;
using Monkey.OAuth.Platform;
using Monkey.Utility;
using Newtonsoft.Json;
using QRCoder;
using Sharper.ExtensionMethods;

namespace Monkey.Controllers
{
    public class OAuthController : Controller, IControllerFilter
    {
        public void Init()
        {
            RouteConfig.ExcludeRouteRules["^oauth"] = true;
            RouteConfig.IgnoreRouteRules["^oauth"] = true;
        }

        private static byte[] GetQRCodeImage(string content, int size = 10)
        {
            var generator = new QRCodeGenerator();
            var qrCodeData = generator.CreateQrCode(content, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCode(qrCodeData);
            using (var bmp = qrCode.GetGraphic(size))
            using (var ms = new MemoryStream())
            {
                bmp.Save(ms, ImageFormat.Png);
                return ms.GetBuffer();
            }
        }

        private LoginProvider GetLoginProvider(string id)
        {
            var key = $"loginprovider_{id}";
            if (!M.CheckConfig($"oauth", key) && Request["make"] == null) return null;
            var provider = M.GetConfig($"oauth", key, new LoginProvider());
            return provider;
        }

        private RedirectResult RedirectResult(string redirectUrl, object data)
        {
            var json = JsonConvert.SerializeObject(data);
            var timestamp = DateTime.Now.GetUnixTimestamp();
            var oauth = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
            var sign = $"{json}{timestamp}".ComputePassHash();
            var url = $"{redirectUrl}?oauth={HttpUtility.UrlEncode(oauth)}&timestamp={timestamp}&sign={HttpUtility.UrlEncode(sign)}";
            return Redirect(url);
        }

        [Route("oauth/wechatqrcode/{id}")]
        public ActionResult WechatQRCode(string id, int size = 8)
        {
            var uri = Request.Url;
            var url = $"{uri.Scheme}://{uri.Authority}/oauth/wechat/{id}?_={new Random().NextDouble()}";
            var buff = GetQRCodeImage(url, size);
            return File(buff, "image/png");
        }

        [Route("oauth/qq/{id}")]
        public ActionResult QQ(string id)
        {
            var provider = GetLoginProvider(id);
            if (provider == null) return HttpNotFound();

            var res = new QQ(provider).Authorize();
            if (res != null && res.code == 0)
            {
                return RedirectResult(provider.RedirectUrl, new LoginResult
                {
                    code = 0,
                    channel = "qq",
                    user = new UserInfo
                    {
                        uid = res.result.Value<string>("openid"),
                        name = res.result.Value<string>("nickname"),
                        figureurl = res.result.Value<string>("figureurl"),
                        token = res.token
                    }
                });
            }

            return Content("");
        }

        [Route("oauth/wechat/{id}")]
        public ActionResult Wechat(string id)
        {
            var provider = GetLoginProvider(id);
            if (provider == null) return HttpNotFound();

            var res = new Wechat(provider).Authorize();
            if (res != null && res.code == 0)
            {
                return RedirectResult(provider.RedirectUrl, new LoginResult
                {
                    channel = "wechat",
                    code = 0,
                    user = new UserInfo
                    {
                        uid = res.result.Value<string>("uid"),
                        name = res.result.Value<string>("nickname"),
                        figureurl = res.result.Value<string>("headimgurl"),
                        token = res.token
                    }
                });
            }

            return Content("");
        }

        [Route("oauth/weibo/{id}")]
        public ActionResult Weibo(string id)
        {
            var provider = GetLoginProvider(id);
            if (provider == null) return HttpNotFound();

            var res = new Weibo(provider).Authorize();
            if (res != null && res.code == 0)
            {
                return RedirectResult(provider.RedirectUrl, new LoginResult
                {
                    channel = "weibo",
                    code = 0,
                    user = new UserInfo
                    {
                        uid = res.result.Value<string>("idstr"),
                        name = res.result.Value<string>("name"),
                        figureurl = res.result.Value<string>("profile_image_url"),
                        token = res.token
                    }
                });
            }

            return Content("");
        }

    }
}
