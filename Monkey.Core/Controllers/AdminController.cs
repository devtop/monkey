﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Monkey.Component;
using Monkey.Kernel;
using Monkey.Kernel.Data;
using Monkey.Kernel.Quick;
using Monkey.Model;
using Monkey.Utility;
using Newtonsoft.Json;

namespace Monkey.Controllers
{
    [Description("系统管理")]
    public class AdminController : Controller, IControllerFilter
    {
        public void Init()
        {
            Navigation.Link("导航", "/admin/index", "仪表板", icon: "icon-paper-plane");
            Navigation.Link("系统", "/admin/user", "用户设置", icon: "icon-user");
            Navigation.Link("系统", "/admin/system", "系统设置", icon: "icon-settings");
            Navigation.Link("系统", "/admin/tool", "系统工具", icon: "icon-wrench");

            Navigation.Link("系统", "/admin/usermanagement", "用户管理", "/admin/user");
            Navigation.Link("系统", "/admin/useraccountlog", "资金日志", "/admin/user");
            Navigation.Link("系统", "/admin/rolecontrol", "权限控制", "/admin/system");
            Navigation.Link("系统", "/admin/setup", "参数设置", "/admin/system");
            Navigation.Link("系统", "/admin/configurations", "参数配置", "/admin/system");
            Navigation.Link("系统", "/admin/processlist", "数据连接", "/admin/tool");
        }

        [Description("仪表盘")]
        public ActionResult Index()
        {
            return ResultConvert.View("core/admin/index");
        }

        [Description("用户管理,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        public ActionResult UserManagement(string method)
        {
            var curd = new CURD(Request,Response, method: method);

            curd.AddField("id", "用户编号");
            curd.AddField("username", "登录账号", required: true);
            curd.AddField("email", "邮箱", required: true);
            curd.AddField("password", "登录密码", FieldType.Password, required: true);
            curd.AddField("usergroup", "用户组", FieldType.Select, required: true, config: MonkeyTerm.GetList("usergroup").Aggregate(":请选择,", (x, y) => $"{x},{y.term_name}:{y.term_text}"));
            curd.AddField("status", "状态", FieldType.Select, required: true, config: ":请选择,available:可用,disable:禁用");
            curd.AddField("signin_addr", "登入地址");
            curd.AddField("signin_date", "登入时间", width: 200, type: FieldType.DateTime);
            curd.AddField("signup_date", "注册时间", width: 200, type: FieldType.DateTime);

            //设置检索字段
            curd.Copy("R", "id", required: false);
            curd.Copy("R", "username", required: false);
            curd.Copy("R", "email", required: false);
            curd.Copy("R", "usergroup", required: false);
            curd.Copy("R", "status", required: false);
            curd.Copy("R", "signin_date", required: false);
            curd.Copy("R", "signup_date", required: false);

            //设置检索过滤字段
            curd.Copy("F", "email", required: false);
            curd.Copy("F", "username", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "usergroup", required: false);
            curd.Copy("F", "status", required: false);
            curd.Copy("F", "signin_date", type: FieldType.DateRanges, required: false);
            curd.Copy("F", "signup_date", type: FieldType.DateRanges, required: false);

            //设置添加字段
            curd.Copy("C", "username", required: true);
            curd.Copy("C", "email", required: true);
            curd.Copy("C", "password", required: true).GetValueAfter = (sender, args) =>
            {
                args.StringValue = args.StringValue.ComputePassHash();
                args.DynamicValue = args.StringValue;
            };
            curd.Copy("C", "status", required: true);
            curd.Copy("C", "usergroup", required: true);

            //设置编辑字段
            curd.Copy("U", "usergroup", required: true);
            curd.Copy("U", "status", required: true);
            var passwordField = curd.Copy("U", "password", required: false);
            passwordField.NullNotUpdate = true;
            passwordField.GetValueAfter = (sender, args) =>
            {
                args.StringValue = args.StringValue.ComputePassHash();
                args.DynamicValue = args.StringValue;
            };

            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_user");

            //var custom_fields = M.GetConfig("system", "manageuser_custom_fields", new List<Field>());
            //设置添加字段
            //curd.CopyFields(curd.CreateObject.ColumnFields, null, "username", "email", "password", "usergroup", "status");
            //curd.CreateObject.ColumnFields.AddRange(custom_fields);

            return curd.View();
        }

        [Description("资金日志,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        public ActionResult UserAccountLog(string method)
        {
            var uid = (int)Session["uid"];

            var curd = new CURD(Request, Response, method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_account_log", support: "R");
            curd.Quick.RetrieveBefore = (sender, args) =>
            {
                using (var db = new MonkeyEntities())
                {
                    var changeAmount = db.Database.SqlQuery<decimal?>($"SELECT SUM(change_amount) {args.Sql}").FirstOrDefault();
                    args.RetrieveObj.Stat.Add($"合计金额：{changeAmount ?? 0}");
                }
            };

            curd.Quick.RetrieveItemBefore = (sender, args) =>
            {
                if (args.field.Name == "uid")
                {
                    var v = args.field.GetValue();
                    var kv = args.field.StringStringD.FirstOrDefault(m => m.Value == v);
                    var tsql = args.field.RetrieveSql.Replace("{key}", args.field.Name).Replace("{value}", kv.Key);
                    return tsql;
                }
                return args.Sql;
            };

            curd.AddField("id", "编号");
            curd.AddField("uid", "用户");
            curd.AddField("type", "类型", FieldType.Text, required: true);
            curd.AddField("change_amount", "金额");
            curd.AddField("before_change", "变动前");
            curd.AddField("after_change", "余额");
            curd.AddField("title", "标题", width: 200);
            curd.AddField("content", "内容", width: 400);
            curd.AddField("status", "状态", FieldType.Select, required: true, config: ":请选择,fail:失败,success:成功");
            curd.AddField("create_time", "时间", width: 200, type: FieldType.DateTime);
            curd.AddField("other1", "信息1", width: 200);
            curd.AddField("other2", "信息2", width: 200);

            curd.Copy("R", "id");
            curd.Copy("R", "uid");
            curd.Copy("R", "type");
            curd.Copy("R", "title");
            curd.Copy("R", "change_amount");
            curd.Copy("R", "after_change");
            curd.Copy("R", "status");
            curd.Copy("R", "other1");
            curd.Copy("R", "other2");
            curd.Copy("R", "content");
            curd.Copy("R", "create_time");

            curd.Copy("F", "uid", required: false, type: FieldType.Text);
            curd.Copy("F", "type", required: false);
            curd.Copy("F", "title", required: false);
            curd.Copy("F", "content", required: false);
            curd.Copy("F", "status", required: false);
            curd.Copy("F", "change_amount", required: false);
            curd.Copy("F", "create_time", required: false, type: FieldType.DateRanges);
            curd.Copy("F", "other1");
            curd.Copy("F", "other2");


            return curd.View();
        }

        [Description("角色控制,delete_usergroup:删除用户组,add_usergroup:添加用户组,get_usergroup_route:读取路由,save_usergroup_route:保存路由")]
        public ActionResult RoleControl(string method)
        {
            var form = new WebForm(Request);
            if (Request.HttpMethod == "POST")
            {
                using (var db = new MonkeyEntities())
                {
                    switch (method)
                    {
                        case "save_usergroup_route":
                            form.ColumnFields.Add(new Field("id", "用户组编号", required: true));
                            form.ColumnFields.Add(new Field("routes", "路由规则", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            else
                            {
                                var routes = new Dictionary<string, bool>();
                                foreach (var route in JsonConvert.DeserializeObject<Dictionary<string, bool>>(form.GetString("routes")))
                                {
                                    var sc = route.Key.Trim('/').Split('/');
                                    var baseroute = sc.Take(sc.Length - 1).Aggregate("", (x, y) => $"{x}/{y}");
                                    if (!routes.ContainsKey(baseroute))
                                    {
                                        routes[baseroute] = baseroute.Trim('/').Contains('/');
                                    }
                                    routes[route.Key] = route.Value;
                                }

                                MonkeyTerm.SetMeta(form.GetInt("id"), "routes", routes);
                                return ResultConvert.Respond(0, "操作成功");
                            }

                        case "get_usergroup_route":
                            form.ColumnFields.Add(new Field("id", "用户组编号", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            else
                            {
                                var routes = MonkeyTerm.GetMeta(form.GetInt("id"), "routes", new Dictionary<string, bool>());
                                return ResultConvert.Respond(0, "", new { routes = routes });
                            }

                        case "delete_usergroup":
                            form.ColumnFields.Add(new Field("id", "用户组编号", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            db.Database.ExecuteSqlCommand($"DELETE FROM monkey_term WHERE id = {form.GetInt("id")}");
                            return ResultConvert.Respond(0, "删除成功", new { reload = true });

                        case "add_usergroup":
                            form.ColumnFields.Add(new Field("usergroup_name", "用户组名称", required: true));
                            form.ColumnFields.Add(new Field("usergroup_text", "用户组描述", required: true));
                            form.LoadFieldData();
                            if (!form.Validate())
                            {
                                return ResultConvert.Respond(1, form.GetValidateError());
                            }
                            if (MonkeyTerm.Exist("usergroup", form.GetString("usergroup_name")))
                            {
                                return ResultConvert.Respond(1, "用户组名称重复");
                            }

                            var termitem = new monkey_term
                            {
                                term_type = "usergroup",
                                term_name = form.GetString("usergroup_name"),
                                term_text = form.GetString("usergroup_text")
                            };
                            db.monkey_term.Add(termitem);
                            db.SaveChanges();
                            return ResultConvert.Respond(0, "添加成功", new { reload = true });
                    }
                }
            }

            return ResultConvert.View("core/admin/rolecontrol");
        }

        [Description("设置,delete:删除,save:保存")]
        public ActionResult Setup(string method)
        {
            if (Request.HttpMethod == "POST")
            {
                var form = new WebForm(Request);
                switch (method)
                {
                    case "delete":
                        form.ColumnFields.Add(new Field("metaid", required: true));
                        form.LoadFieldData();
                        if (!form.Validate())
                        {
                            return ResultConvert.Json(new { status = 1, msg = form.GetValidateError() });
                        }
                        using (var db = new MonkeyEntities())
                        {
                            var metaid = form.GetInt("metaid");
                            var termmeta = db.monkey_termmeta.FirstOrDefault(m => m.id == metaid);
                            if (termmeta == null)
                            {
                                return ResultConvert.Respond(1, "编号错误");
                            }
                            db.monkey_termmeta.Remove(termmeta);
                            db.SaveChanges();
                            return ResultConvert.Respond(0, "删除成功", obj: new { reload = true });
                        }

                    case "save":
                        form.ColumnFields.Add(new Field("term_name", required: true));
                        form.ColumnFields.Add(new Field("meta_key", required: true));
                        form.ColumnFields.Add(new Field("meta_type", required: true));
                        form.ColumnFields.Add(new Field("meta_value", required: true));
                        form.LoadFieldData();
                        if (!form.Validate())
                        {
                            return ResultConvert.Json(new { status = 1, msg = form.GetValidateError() });
                        }
                        var term_name = form.GetString("term_name");
                        var meta_key = form.GetString("meta_key");
                        var meta_value = Convert.ToBoolean(Request["stringtype"]) ? form.GetString("meta_value") : JsonConvert.DeserializeObject(form.GetString("meta_value"));
                        M.SetConfig(term_name, meta_key, meta_value);
                        return ResultConvert.Json(new { status = 0, msg = "保存成功" });
                }
            }
            return ResultConvert.View("core/admin/setup");
        }

        [Description("配置,retrieve:检索,delete:删除,create:创建,edit:编辑,detail:详细")]
        public ActionResult Configurations(string method)
        {
            var curd = new CURD(Request, Response, method: method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "monkey_termmeta", sort: "ORDER BY `meta_key`")
            {
            };

            curd.AddField("id", "编号");
            curd.AddField("termid", "分类", FieldType.Select, required: true, config: curd.Quick.GetSelectStringData("monkey_term", seed: "", valueField: "term_name", appendSql: " AND term_type = 'config'"));
            curd.AddField("meta_type", "类型", FieldType.Select, required: true, config: "json:JSON,xml:XML");
            curd.AddField("meta_key", "键", FieldType.Text, required: true);
            curd.AddField("meta_value", "值", FieldType.Textarea, required: true);
            curd.AddField("meta_desc", "描述", FieldType.Textarea, required: true);

            //设置检索字段
            curd.Copy("R", "id", required: false);
            curd.Copy("R", "termid", required: false);
            curd.Copy("R", "meta_type", required: false);
            curd.Copy("R", "meta_key", required: false);
            curd.Copy("R", "meta_value", required: false);
            curd.Copy("R", "meta_desc", required: false);

            //设置检索过滤字段
            curd.Copy("F", "termid", required: false);
            curd.Copy("F", "meta_type", required: false);
            curd.Copy("F", "meta_key", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "meta_value", type: FieldType.Text, required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "meta_desc", type: FieldType.Text, required: false).RetrieveSql = "AND `{key}` like '%{value}%'";

            //设置添加字段
            curd.Copy("C", "termid", required: true);
            curd.Copy("C", "meta_type", required: true);
            curd.Copy("C", "meta_key", required: true);
            curd.Copy("C", "meta_value", required: true);
            curd.Copy("C", "meta_desc", required: false);

            //设置编辑字段
            curd.Copy("U", "termid", required: true);
            curd.Copy("U", "meta_type", required: true);
            curd.Copy("U", "meta_key", required: true);
            curd.Copy("U", "meta_value", required: true);
            curd.Copy("U", "meta_desc", required: false);

            return curd.View();
        }

        [Description("数据库连接,retrieve:检索,kill:杀死")]
        public ActionResult ProcessList(string method)
        {
            var curd = new CURD(Request, Response, method);
            curd.Quick = new QuickComplete(new MonkeyEntities(), "`information_schema`.`PROCESSLIST`", support: "RE");
            curd.RetrieveObj.PrimaryKey = "ID";

            curd.AddField("ID", "编号");
            curd.AddField("USER", "用户");
            curd.AddField("HOST", "主机");
            curd.AddField("DB", "数据库");
            curd.AddField("COMMAND", "命令");
            curd.AddField("TIME", "时间");
            curd.AddField("STATE", "状态");
            curd.AddField("INFO", "信息", width: 200);

            curd.Copy("R", "ID");
            curd.Copy("R", "USER");
            curd.Copy("R", "HOST");
            curd.Copy("R", "DB");
            curd.Copy("R", "COMMAND");
            curd.Copy("R", "TIME");
            curd.Copy("R", "STATE");
            curd.Copy("R", "INFO");

            curd.Copy("F", "USER", required: false);
            curd.Copy("F", "HOST", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "DB", required: false);
            curd.Copy("F", "COMMAND", required: false);
            curd.Copy("F", "TIME", required: false).RetrieveSql = "AND `{key}` >= '{value}'";
            curd.Copy("F", "STATE", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";
            curd.Copy("F", "INFO", required: false).RetrieveSql = "AND `{key}` like '%{value}%'";

            curd.TodoCollection["kill"] = new Todo("杀死", todo =>
            {
                foreach (var id in todo.IDs)
                {
                    try
                    {
                        curd.Quick.ExecuteNonQuery($"KILL {id}");
                    }
                    catch (Exception ex)
                    {
                        return ResultConvert.Respond(1, ex.ToString());
                    }
                }
                return ResultConvert.Respond(0, "成功");
            });

            return curd.View();
        }

    }
}