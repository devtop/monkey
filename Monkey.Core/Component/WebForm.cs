﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Monkey.Kernel;
using Monkey.Kernel.Data;
using Monkey.Utility;

namespace Monkey.Component
{
    public class WebForm : Form
    {
        public WebForm(HttpRequestBase request, HttpResponseBase response = null) : base(request, response)
        {
            this.NameValueCollection = NameValueCollection;
            this.ColumnNameValueCollection = NameValueCollection; 
            this.FilterNameValueCollection = NameValueCollection;
            this.TodoCollection = TodoCollection;
            this.PopupCollection = PopupCollection;
        }

        public Field AddField(
            string name,
            string text = "",
            FieldType type = FieldType.Text,
            string value = "",
            bool required = false,
            string pattern = "",
            string placeholder = "",
            string config = "",
            int? height = null,
            int width = 100,
            List<Field> fields = null)
        {
            var field = new Field(name, type: type);
            field.Text = text;
            field.Type = type;
            field.Required = required;
            field.Pattern = pattern;
            field.Placeholder = placeholder;
            field.Width = width;
            field.Height = height ?? field.Height;
            field.StringStringD = config.ToDict();
            field.SetValue(value);

            fields = fields ?? ColumnFields;
            fields.Add(field);
            return field;
        }

        public ActionResult View(dynamic viewData = null, string viewName = "core/curd/form.cshtml")
        {
            return ResultConvert.View(viewName, viewData ?? this);
        }
    }
}
