﻿using System;
using System.Linq;
using Donkey.Data.MySql;
using Monkey.Model;
using Monkey.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Monkey.Component
{
    /// <summary>
    /// 
    /// </summary>
    public class MonkeyUser
    {

        /// <summary>
        /// 登入
        /// </summary>
        public static monkey_user Signin(string nameOrEmail, string password, string signinaddr = "")
        {
            password = password.ComputePassHash();

            using (var db = new MonkeyEntities())
            {
                var user = db.monkey_user.FirstOrDefault(m => (m.username == nameOrEmail || m.email == nameOrEmail) && m.password == password);
                if (user == null) return null;
                user.signin_addr = signinaddr;
                user.signin_date = DateTime.Now;
                db.SaveChanges();
                return user;
            }
        }

        /// <summary>
        /// 注册
        /// </summary>
        public static int Signup(string username, string email, string password, string usergroup = "user")
        {
            using (var db = new MonkeyEntities())
            {
                if (db.monkey_user.Any(m => m.username == username || m.email == email))
                {
                    return 0;
                }
                var user = new monkey_user();
                user.status = "available";
                user.username = username;
                user.email = email;
                user.usergroup = usergroup;
                user.password = password.ComputePassHash();
                user.signup_date = DateTime.Now;
                db.monkey_user.Add(user);
                db.SaveChanges();
                return user.id;
            }
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        public static bool Exist(string username = null, string email = null, string password = null, int? userid = null)
        {
            using (var db = new MonkeyEntities())
            {
                if (string.IsNullOrEmpty(username) &&
                    string.IsNullOrEmpty(email) &&
                    userid == null)
                {
                    return false;
                }

                var queryobj = db.monkey_user.Where(m => true);
                if (!string.IsNullOrEmpty(username)) queryobj = queryobj.Where(m => m.username == username);
                if (!string.IsNullOrEmpty(email)) queryobj = queryobj.Where(m => m.email == email);
                if (!string.IsNullOrEmpty(password)) queryobj = queryobj.Where(m => m.password == password);
                if (userid != null) queryobj = queryobj.Where(m => m.id == userid);
                return queryobj.Any();
            }
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        public static monkey_user Get(string username = null, string email = null, string password = null, int? userid = null)
        {
            using (var db = new MonkeyEntities())
            {
                if (string.IsNullOrEmpty(username) &&
                    string.IsNullOrEmpty(email) &&
                    userid == null)
                {
                    return null;
                }

                var queryobj = db.monkey_user.Where(m => true);
                if (!string.IsNullOrEmpty(username)) queryobj = queryobj.Where(m => m.username == username);
                if (!string.IsNullOrEmpty(email)) queryobj = queryobj.Where(m => m.email == email);
                if (!string.IsNullOrEmpty(password)) queryobj = queryobj.Where(m => m.password == password);
                if (userid != null) queryobj = queryobj.Where(m => m.id == userid);
                return queryobj.FirstOrDefault();
            }
        }

        /// <summary>
        /// 获取属性
        /// </summary>
        public static T GetMeta<T>(int uid, string metaKey, T metaValue = default(T), int cache = 0)
        {
            return MemCache.Get<T>($"{uid}{metaKey}", () =>
            {
                using (var db = new MonkeyEntities())
                {
                    var meta = db.monkey_usermeta.FirstOrDefault(m => m.userid == uid && m.meta_key == metaKey);
                    if (meta?.meta_value == null)
                    {
                        if (metaValue != null)
                        {
                            SetMeta(uid, metaKey, metaValue);
                        }
                        return metaValue;
                    }
                    var cacheValue = JsonConvert.DeserializeObject<T>(meta.meta_value);
                    return cacheValue;
                }
            }, "usermeta", cache);
        }

        /// <summary>
        /// 设置属性
        /// </summary>
        public static void SetMeta<T>(int uid, string metaKey, T metaValue)
        {
            using (var db = new MonkeyEntities())
            {
                var json = JsonConvert.SerializeObject(metaValue);
                var meta = db.monkey_usermeta.FirstOrDefault(m => m.userid == uid && m.meta_key == metaKey);
                if (meta == null)
                {
                    meta = new monkey_usermeta
                    {
                        userid = uid,
                        meta_key = metaKey,
                        meta_value = json
                    };
                    db.monkey_usermeta.Add(meta);
                }
                else
                {
                    meta.meta_value = json;
                }
                db.SaveChanges();
            }
            MemCache.Delete($"{uid}{metaKey}", "usermeta");
        }

        /// <summary>
        /// 获取/更新计数
        /// </summary>
        public static int Count(int uid, string metaKey = "count", int amount = 0)
        {
            var count = GetMeta(uid, metaKey, 0);
            if (amount != 0)
            {
                SetMeta(uid, metaKey, count += amount);
            }
            return count;
        }

        /// <summary>
        /// 重置计数
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="metaKey"></param>
        public static void ResetCount(int uid, string metaKey = "count")
        {
            SetMeta(uid, metaKey, 0);
        }

        /// <summary>
        /// 获取用户头像
        /// </summary>
        public static string GetAvatarUrl(int? uid)
        {
            var defaultAvatarUrl = M.GetConfig("site", "defaultAvatarUrl", "/tpl/default/static/core/img/a0.jpg");
            if (uid == null)
            {
                return defaultAvatarUrl;
            }
            var avatar = GetMeta<JObject>((int)uid, "avatar");
            return avatar != null ? (string)avatar["link"] : defaultAvatarUrl;
        }

        /// <summary>
        /// 获取用户头像
        /// </summary>
        public static string GetAvatarUrl(string username)
        {
            using (var db = new MonkeyEntities())
            {
                var user = db.monkey_user.FirstOrDefault(m => m.username == username);
                return GetAvatarUrl(user?.id);
            }
        }

        /// <summary>
        /// 获取余额
        /// </summary>
        public static decimal GetBalance(int uid)
        {
            return GetMeta(uid, "balance", Convert.ToDecimal(0), 0);
        }

        /// <summary>
        /// 获取角色编号
        /// </summary>
        public static monkey_term GetUserGroup(int uid)
        {
            var user = Get(userid: uid);
            return MonkeyTerm.GetList("usergroup").FirstOrDefault(m => m.term_name == user.usergroup);
        }

        /// <summary>
        /// 获取角色编号
        /// </summary>
        public static monkey_term GetUserGroup(string username)
        {
            var user = Get(username: username);
            return MonkeyTerm.GetList("usergroup").FirstOrDefault(m => m.term_name == user.usergroup);
        }

        /// <summary>
        /// 获取角色编号
        /// </summary>
        public static int? GetUserGroupId(int uid)
        {
            var user = Get(userid: uid);
            var role = MonkeyTerm.GetList("usergroup").FirstOrDefault(m => m.term_name == user.usergroup);
            return role?.id;
        }

        /// <summary>
        /// 获取角色编号
        /// </summary>
        public static int? GetUserGroupId(string username)
        {
            var user = Get(username: username);
            var role = MonkeyTerm.GetList("usergroup").FirstOrDefault(m => m.term_name == user.usergroup);
            return role?.id;
        }

        /// <summary>
        /// 更新用户余额
        /// </summary>
        public static bool ChangeBalance(string type, int uid, decimal changed, string title, string content, int relateder, params object[] others)
        {
            using (var db = new MySqlClient(M.ConnectionString))
            {
                db.KeepAlive = true;
                others = others ?? new object[] { };
                db.Transaction();
                try
                {
                    if (!db.Any($"SELECT meta_value FROM monkey_usermeta WHERE userid = {uid} AND meta_key = 'balance'"))
                    {
                        db.ExecuteInsert("monkey_usermeta", new { userid = uid, meta_key = "balance", meta_value = 0 });
                    }
                    var balance = db.GetValue<decimal>($"SELECT meta_value FROM monkey_usermeta WHERE userid = {uid} AND meta_key = 'balance' FOR UPDATE;");

                    #region 

                    var before_change = balance;
                    var change_amount = changed;
                    var after_change = balance + changed;
                    var create_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    var status = after_change < 0 ? "fail" : "success";
                    for (var i = 0; i < others.Length; i++) others[i] = db.ConvertUnsafeChar(others[i].ToString());
                    var other1 = others.Length > 0 ? others[0].ToString() : null;
                    var other2 = others.Length > 1 ? others[1].ToString() : null;
                    var other3 = others.Length > 2 ? others[2].ToString() : null;
                    var other4 = others.Length > 3 ? others[3].ToString() : null;
                    var other5 = others.Length > 4 ? others[4].ToString() : null;
                    var other6 = others.Length > 5 ? others[5].ToString() : null;
                    var other7 = others.Length > 6 ? others[6].ToString() : null;
                    var other8 = others.Length > 7 ? others[7].ToString() : null;
                    var other9 = others.Length > 8 ? others[8].ToString() : null;
                    var other10 = others.Length > 9 ? others[9].ToString() : null;

                    #endregion

                    db.ExecuteInsert("monkey_account_log", new
                    {
                        uid = uid,
                        type = type,
                        status = status,
                        before_change = before_change,
                        change_amount = change_amount,
                        after_change = after_change,
                        title = db.ConvertUnsafeChar(title),
                        content = db.ConvertUnsafeChar(content),
                        relateder = relateder,
                        create_time = create_time,
                        other1 = other1,
                        other2 = other2,
                        other3 = other3,
                        other4 = other4,
                        other5 = other5,
                        other6 = other6,
                        other7 = other7,
                        other8 = other8,
                        other9 = other9,
                        other10 = other10,
                    });
                    if (status == "success")
                    {
                        db.ExecuteNonQuery($"UPDATE monkey_usermeta SET meta_value = '{after_change}' WHERE userid = {uid} AND meta_key = 'balance'");
                    }
                    db.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    db.Rollback();
                }
            }

            return false;
        }
    }
}
