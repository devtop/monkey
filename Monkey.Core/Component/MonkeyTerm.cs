﻿using System.Collections.Generic;
using System.Linq;
using Monkey.Model;
using Monkey.Utility;
using Newtonsoft.Json;

namespace Monkey.Component
{
    public class MonkeyTerm
    {
        public static monkey_term Get(string termName)
        {
            using (var db = new MonkeyEntities())
            {
                return db.monkey_term.FirstOrDefault(m => m.term_name == termName);
            }
        }

        public static List<monkey_term> GetList(string termtype)
        {
            using (var db = new MonkeyEntities())
            {
                return db.monkey_term.Where(m => m.term_type == termtype).ToList();
            }
        }

        public static List<monkey_termmeta> GetMetaList(int termid)
        {
            using (var db = new MonkeyEntities())
            {
                return db.monkey_termmeta.Where(m => m.termid == termid).ToList();
            }
        }

        public static bool Exist(string termtype, string termname)
        {
            using (var db = new MonkeyEntities())
            {
                return db.monkey_term.Any(m => m.term_type == termtype && m.term_name == termname);
            }
        }

        /// <summary>
        /// 获取属性
        /// </summary>
        public static T GetMeta<T>(int termid, string metakey, T metavalue = default(T), string metadesc = null)
        {
            return MemCache.Get<T>($"{termid}{metakey}", () =>
            {
                using (var db = new MonkeyEntities())
                {
                    var metaitem = db.monkey_termmeta.FirstOrDefault(m => m.termid == termid && m.meta_key == metakey);
                    if (metaitem == null)
                    {
                        if (metavalue != null)
                        {
                            SetMeta(termid, metakey, metavalue);
                        }
                        return metavalue;
                    }
                    var cache_value = JsonConvert.DeserializeObject<T>(metaitem.meta_value);
                    return cache_value;
                }
            }, "termmeta", 120);
        }

        /// <summary>
        /// 设置属性集合
        /// </summary>
        public static void SetMeta<T>(int termid, string metakey, T metavalue, string metadesc = null)
        {
            using (var db = new MonkeyEntities())
            {
                var metajson = JsonConvert.SerializeObject(metavalue);
                var metaitem = db.monkey_termmeta.FirstOrDefault(m => m.termid == termid && m.meta_key == metakey);
                if (metaitem == null)
                {
                    metaitem = new monkey_termmeta
                    {
                        termid = termid,
                        meta_key = metakey,
                        meta_value = metajson,
                        meta_type = "json",
                    };
                    metaitem.meta_desc = metadesc ?? metaitem.meta_desc;
                    db.monkey_termmeta.Add(metaitem);
                }
                else
                {
                    metaitem.meta_desc = metadesc ?? metaitem.meta_desc;
                    metaitem.meta_value = metajson;
                }
                db.SaveChanges();
            }
            MemCache.Delete($"{termid}{metakey}", "termmeta");
        }

    }
}
