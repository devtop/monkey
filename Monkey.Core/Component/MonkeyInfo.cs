﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Monkey.Component
{
    public class MonkeyInfo
    {
        private static object olock = new object();

        public static Version Version => Assembly.GetExecutingAssembly().GetName().Version;
        public static double RAM { get; set; }
        public static double CPU { get; set; }
        public static double RAMWorkingSet64 { get; set; }

        public static int MaximumHistoryCount { get; set; } = 512;
        public static int Counter { get; set; } = 1;
        private static List<KeyValuePair<string, double[]>> HistoryData { get; set; } = new List<KeyValuePair<string, double[]>>();
        public static double TotalFreeSpace { get; set; }
        public static double TotalSize { get; set; }
        public static double TotalFreeSpacePercent { get; set; }

        public static Dictionary<string, long> UploadFileStat => M.GetConfig("system", "UploadFileStat", new Dictionary<string, long>(), 30);

        public static void SetHistoryData(double cpu, double ram)
        {
            RAM = ram;
            CPU = cpu;
            lock (olock)
            {
                if (!HistoryData.Any())
                {
                    for (int i = 0; i < MaximumHistoryCount; i++)
                    {
                        HistoryData.Add(new KeyValuePair<string, double[]>($"{Counter++}", new double[] { 0, 0 }));
                    }
                }

                HistoryData.Add(new KeyValuePair<string, double[]>($"{Counter++}", new double[] { CPU, RAM }));

                if (HistoryData.Count > MaximumHistoryCount)
                {
                    HistoryData.RemoveAt(0);
                }
            }
        }

        public static object GetHistoryData()
        {
            lock (olock)
            {

                var cpu = new Dictionary<string, object>
                {
                    {"data",new List<object[]>() },
                };
                var ram = new Dictionary<string, object>
                {
                    {"data",new List<object[]>() },
                };
                foreach (var pair in HistoryData)
                {
                    ((List<object[]>)cpu["data"]).Add(new object[] { pair.Key, Math.Round(pair.Value[0], 2) });
                    ((List<object[]>)ram["data"]).Add(new object[] { pair.Key, Math.Round(pair.Value[1], 2) });
                }


                cpu["label"] = "CPU";
                ram["label"] = "RAM";
                /*cpu["points"] = new { show = true, radius = 1 };
                cpu["splines"] = new { show = true, tension = 0.4, lineWidth = 1, fill = 0.8 };
                ram["points"] = new { show = true, radius = 1 };
                ram["splines"] = new { show = true, tension = 0.4, lineWidth = 1, fill = 0.8 };*/

                return new
                {
                    max = HistoryData.Max(m => m.Value.Max()),
                    min = HistoryData.Min(m => m.Value.Min()),
                    data = new[] { cpu, ram }
                };
            }
        }

    }
}
