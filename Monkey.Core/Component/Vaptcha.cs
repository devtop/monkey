﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Donkey.Data;
using Monkey.Model;
using Newtonsoft.Json;
using Sharper.ExtensionMethods;
using Sharper.Http;

namespace Monkey.Component
{
    /// <summary>
    /// 
    /// </summary>
    public class Vaptcha
    {
        /// <summary>
        /// 后端发起
        /// </summary>
        /// <returns></returns>
        public static int SendCode(string token, string phone, string code)
        {

            var vid = M.GetConfig("vaptcha", "vid", "");
            var smskey = M.GetConfig("vaptcha", "smskey", "");
            var label = M.GetConfig("vaptcha", "label", "monkey");

            var url = "https://smsapi.vaptcha.com/sms/sendcode";

            var body = JsonConvert.SerializeObject(new
            {
                vid = vid,
                code = code,
                countrycode = "86",
                label = label,
                expiretime = "10",
                phone = phone,
                smskey = smskey,
                time = $"{DateTime.Now.GetUnixTimestamp(1000)}",
                token = token,
                version = "1.0",
            });
            var responseCode = HttpClient.Request(url, "POST", body, requestHeader: new Dictionary<string, string>
            {
                {"Content-Type","application/json; charset=utf-8"},
            }).GetHtml().ToInt();

            return responseCode;

        }

        /// <summary>
        /// 后端发起
        /// </summary>
        /// <returns></returns>
        public static int VerifyCode(string token, string phone)
        {
            var url = "https://smsapi.vaptcha.com/sms/verifycode";
            var vid = M.GetConfig("vaptcha", "vid", "");
            var smskey = M.GetConfig("vaptcha", "smskey", "");
            var label = M.GetConfig("vaptcha", "label", "monkey");
            var body = JsonConvert.SerializeObject(new
            {
                vid = vid,
                countrycode = "86",
                label = label,
                expiretime = "10",
                phone = phone,
                smskey = smskey,
                time = $"{DateTime.Now.GetUnixTimestamp(1000)}",
                token = token,
                version = "1.0",
            });
            var responseCode = HttpClient.Request(url, "POST", body, requestHeader: new Dictionary<string, string>
            {
                {"Content-Type","application/json; charset=utf-8"},
            }).GetHtml().ToInt();

            return responseCode;

        }

        /// <summary>
        /// 后端发起
        /// </summary>
        /// <returns></returns>
        public static bool Verify(string phone, string code)
        {
            var url = "https://smsapi.vaptcha.com/sms/verify";
            var vid = M.GetConfig("vaptcha", "vid", "");
            var smskey = M.GetConfig("vaptcha", "smskey", "");

            var body = JsonConvert.SerializeObject(new
            {
                vid = vid,
                code = code,
                smskey = smskey,
                countrycode = "86",
                phone = phone,
                time = $"{DateTime.Now.GetUnixTimestamp(1000)}",
                version = "1.0"
            });
            var responseCode = HttpClient.Request(url, "POST", body, requestHeader: new Dictionary<string, string>
            {
                {"Content-Type","application/json; charset=utf-8"},
            }).GetHtml().ToInt();
            return responseCode == 200;
        }

    }
}
