﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monkey.Component
{
    public class MonkeyConfig
    {

        private string _termname { get; set; }

        public MonkeyConfig(string termname)
        {
            this._termname = termname;
        }

        /// <summary>
        /// 获取
        /// </summary>
        public T GetValue<T>(string metakey, T metavalue = default(T), int expire = 30)
        {
            return M.GetConfig(_termname, metakey, metavalue, expire);
        }

        /// <summary>
        /// 设置
        /// </summary>
        public void SetValue<T>(string metakey, T metavalue = default(T))
        {
            M.SetConfig(_termname, metakey, metavalue);
        }
    }
}
