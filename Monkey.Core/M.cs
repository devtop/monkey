﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Monkey.Component;
using Monkey.Kernel;
using Monkey.Kernel.AutoTask;
using Monkey.Model;
using Monkey.Utility;
using Sharper.ExtensionMethods;

namespace Monkey
{
    /// <summary>
    /// 
    /// </summary>
    public static class M
    {

        private static Assembly CallingAssembly { get; set; }
        private static Assembly ExecutingAssembly { get; set; }

        /// <summary>
        /// 初始化数据库实体 
        /// </summary>
        private static void InitializationOfDatabaseConnectionString()
        {
            using (var db = new MonkeyEntities())
            {
                ConnectionString = db.Database.Connection.ConnectionString;
            }
        }

        /// <summary>
        /// 初始化基础配置 
        /// </summary>
        private static void InitializeBasicConfiguration()
        {
            var homePath = ExecutingAssembly.CodeBase.Match("file:///(?<s>.*?)/bin/Monkey.Core.DLL");
            var homeHardDisk = homePath.Split('/')[0].TrimEnd(':');

            SetConfig("system", "HomePath", homePath);
            SetConfig("system", "HomeHardDisk", homeHardDisk);

            GetConfig("site", "home_url", "");
            GetConfig("site", "title", "Monkey");
            GetConfig("site", "keywords", "");
            GetConfig("site", "description", "");
            GetConfig("site", "signin_text", "欢迎回来");
            GetConfig("site", "copyright", "&copy; 2017 Copyright.");
        }

        /// <summary>
        /// 调试模式
        /// </summary>
        public static bool DebugMode { get; set; }

        /// <summary>
        /// 自由模式
        /// </summary>
        public static bool FreeMode { get; set; }

        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public static string ConnectionString { get; set; }

        /// <summary>
        /// 请求检查
        /// </summary>
        public static bool RequestAuth(monkey_term role, string path)
        {
            path = path.Split('?')[0].Trim().ToLower().TrimEnd('/');

            if (role.term_name == "administrator")
            {
                return true;
            }

            if (RouteConfig.ExcludeRouteRules.Any(b => (b.Value && Regex.IsMatch(path, b.Key)) || (!b.Value && path == b.Key)))
            {
                return true;
            }

            var routes = MonkeyTerm.GetMeta(role.id, "routes", new Dictionary<string, bool>());
            return routes.ContainsKey(path) && routes[path];
        }

        /// <summary>
        /// 请求检查
        /// </summary>
        public static string RequestAuth(HttpSessionStateBase session, string path)
        {
            if (FreeMode)
            {
                return "success";
            }

            path = path.Split('?')[0].TrimEnd('/').ToLower();

            #region 处理匿名用户

            var loguser = session["loguser"] as monkey_user;
            if (loguser == null)
            {
                loguser = MonkeyUser.Get(username: "anonymous");
                if (loguser == null)
                {
                    MonkeyUser.Signup("anonymous", "anonymous", "anonymous", "anonymous");
                    return "signin";
                }
                session["uid"] = loguser.id;
                session["username"] = loguser.username;
                session["loguser"] = loguser;
                session[$"{loguser.id}"] = loguser;
            }

            #endregion

            if (loguser.usergroup == "administrator") return "success";

            //处理白名单
            var input = path.Trim('/');
            if (RouteConfig.ExcludeRouteRules.Any(b => b.Value && Regex.IsMatch(input, b.Key) || (!b.Value && input == b.Key)))
            {
                return "success";
            }

            var roleId = MonkeyUser.GetUserGroupId(loguser.id);
            if (roleId == null)
            {
                return "signin";
            }

            var routes = MonkeyTerm.GetMeta((int)roleId, "routes", new Dictionary<string, bool>());
            return routes.ContainsKey(path) && routes[path] ? "success" : loguser.usergroup == "anonymous" ? "signin" : "fail";
        }

        /// <summary>
        /// 请求检查
        /// </summary>
        public static bool RequestAuth(int uid, string path)
        {
            var role = MonkeyUser.GetUserGroup(uid);
            return role != null && RequestAuth(role, path);
        }

        /// <summary>
        /// 请求检查
        /// </summary>
        public static bool RequestAuth(string username, string path)
        {
            var role = MonkeyUser.GetUserGroup(username);
            return role != null && RequestAuth(role, path);
        }

        /// <summary>
        /// 检查配置
        /// </summary>
        public static bool CheckConfig(string termname, string metakey)
        {
            termname = termname.UnderlineCase();
            metakey = metakey.UnderlineCase();
            using (var db = new MonkeyEntities())
            {
                var term = db.monkey_term.FirstOrDefault(m => m.term_type == "config" && m.term_name == termname);
                return term != null && db.monkey_termmeta.Any(m => m.termid == term.id && m.meta_key == metakey);
            }
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="termname"></param>
        /// <returns></returns>
        public static MonkeyConfig GetConfig(string termname)
        {
            return new MonkeyConfig(termname);
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        public static T GetConfig<T>(string termname, string metakey, T metavalue = default(T), int expire = 30)
        {
            termname = termname.UnderlineCase();
            metakey = metakey.UnderlineCase();
            return MemCache.Get<T>($"{termname}{metakey}", () =>
            {
                using (var db = new MonkeyEntities())
                {
                    var termitem = db.monkey_term.FirstOrDefault(m => m.term_type == "config" && m.term_name == termname);
                    if (termitem == null)
                    {
                        termitem = new monkey_term
                        {
                            term_type = "config",
                            term_name = termname,
                        };
                        db.monkey_term.Add(termitem);
                        db.SaveChanges();
                    }
                    return MonkeyTerm.GetMeta(termitem.id, metakey, metavalue);
                }
            }, "config", expire);
        }

        /// <summary>
        /// 设置配置
        /// </summary>
        public static void SetConfig<T>(string termname, string metakey, T metavalue = default(T))
        {
            termname = termname.UnderlineCase();
            metakey = metakey.UnderlineCase();

            using (var db = new MonkeyEntities())
            {
                var termitem = db.monkey_term.FirstOrDefault(m => m.term_type == "config" && m.term_name == termname);
                if (termitem == null)
                {
                    termitem = new monkey_term
                    {
                        term_type = "config",
                        term_name = termname,
                    };
                    db.monkey_term.Add(termitem);
                    db.SaveChanges();
                }
                MonkeyTerm.SetMeta(termitem.id, metakey, metavalue);
            }
            MemCache.Delete($"{termname}{metakey}", "config");
        }

        /// <summary>
        /// 设置路由白名单
        /// </summary>
        /// <param name="path"></param>
        public static void SetRouteWhiteList(string path)
        {
            RouteConfig.ExcludeRouteRules[path] = true;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public static void Init(Assembly callingAssembly = null, bool debug = false)
        {
            DebugMode = debug;

            ExecutingAssembly = Assembly.GetExecutingAssembly();
            CallingAssembly = callingAssembly ?? Assembly.GetCallingAssembly();

            RouteConfig.ExcludeRouteRules = GetConfig("system", "RouteWhiteMaps", new Dictionary<string, bool>
            {
                {"basic/signin",true},
                {"basic/logout",true},
                {"basic/page/error",true},
                {"basic/page/forbidden",true},
                {"basic/install",true},
            });

            //注册导航
            Navigation.Lable("导航", 1000);
            Navigation.Lable("视图", 0);
            Navigation.Lable("系统", -1000);

            //注册路由
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.MapMvcAttributeRoutes();
            RouteConfig.RegisterRoutes(ExecutingAssembly);
            RouteConfig.RegisterRoutes(CallingAssembly);
            RouteConfig.InitRouteMapRules(RouteTable.Routes);

            //过滤器
            GlobalFilters.Filters.Add(new RouteFilterAttribute());
            GlobalFilters.Filters.Add(new ExceptionFilterAttribute());

            //任务调配
            AutoTaskAttribute.Start(ExecutingAssembly, CallingAssembly);
            AutoTaskDispatcher.Start(ExecutingAssembly, CallingAssembly);

            //初始化
            InitializationOfDatabaseConnectionString();
            InitializeBasicConfiguration();
        }



    }
}
