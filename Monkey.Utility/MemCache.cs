﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sharper.ExtensionMethods;

namespace Monkey.Utility
{
    /// <summary>
    /// 
    /// </summary>
    public class MemCache
    {
        private static Dictionary<string, string> CacheGroup { get; set; } = new Dictionary<string, string>();

        private static System.Runtime.Caching.MemoryCache CacheObj { get; set; } = System.Runtime.Caching.MemoryCache.Default;

        /// <summary>
        /// 获取或创建
        /// </summary>
        public static T Get<T>(string key, MemCacheNonExistEventHandler handler, string groupName = "system", int expire = 60)
        {
            if (expire == 0) return (T)handler();
            var cacheKey = $"{groupName}.{key}".ToHash();
            if (!CacheGroup.ContainsKey(cacheKey))
            {
                CacheGroup[cacheKey] = groupName;
            }
            var cacheObject = CacheObj[cacheKey];
            if (cacheObject != null)
            {
                try
                {
                    return (T)cacheObject;
                }
                catch (Exception)
                {
                    Delete(key);
                    return (T)handler();
                }
            }
            cacheObject = handler();
            if (cacheObject == null)
            {
                return default(T);
            }
            CacheObj.Set(cacheKey, cacheObject, new DateTimeOffset(DateTime.Now.AddSeconds(expire)));
            return (T)cacheObject;
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        public static void Delete(string key = null, string groupName = "system")
        {
            if (string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(groupName))
            {
                foreach (var item in CacheGroup.Where(m => m.Value == groupName).Where(item => CacheObj[item.Key] != null))
                {
                    CacheObj.Remove(item.Key);
                }
            }

            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(groupName))
            {
                var cacheKey = $"{groupName}.{key}".ToHash();
                if (CacheObj[cacheKey] != null)
                {
                    CacheObj.Remove(cacheKey);
                }
            }

        }
    }
}
