﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Monkey.Model;
using Newtonsoft.Json;
using Sharper.ExtensionMethods;

namespace Monkey.Utility
{
    /// <summary>
    /// 
    /// </summary>
    public static class GlobalExtensionMethod
    {

        /// <summary>
        /// 密码编码
        /// </summary>
        public static string ComputePassHash(this string passwd)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            using (var sha512 = new SHA512CryptoServiceProvider())
            {
                return Convert.ToBase64String(sha512.ComputeHash(md5.ComputeHash(Encoding.UTF8.GetBytes(passwd))));
            }
        }

        /// <summary>
        /// 字典获取
        /// </summary>
        public static T GetValue<T>(this Dictionary<string, T> dict, string key, T value = default(T), bool nullorempty = false)
        {
            if (string.IsNullOrEmpty(key) || !dict.ContainsKey(key))
            {
                return value;
            }
            if (nullorempty)
            {
                if (dict[key] == null || string.IsNullOrEmpty(dict[key].ToString()))
                {
                    return value;
                }
            }
            return dict[key];
        }

        /// <summary>
        /// 对象转换字典集合
        /// </summary>
        public static List<Dictionary<string, object>> ToDictCollection<T>(this List<T> queryable)
        {
            return queryable.Select(st => st.PropertyToDict()).ToList();
        }

        /// <summary>
        /// 字符串转换字典 
        /// </summary>
        public static Dictionary<string, string> ToDict(this string str)
        {
            var dict = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(str))
            {
                foreach (var s in str.Split(','))
                {
                    var sc = s.Split(':');
                    if (string.IsNullOrEmpty(s.Trim()) || sc.Length != 2)
                    {
                        continue;
                    }
                    dict[sc[0]] = sc[1];
                }
            }
            return dict;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ToBeforeTime(this DateTime datetime)
        {
            var timestamp = (datetime.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            var timestamp2 = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            var s = timestamp2 - timestamp;
            if (s < 60)
            {
                return $"{s}秒以前";
            }
            if (s < 3600)
            {
                return $"{s / 60}分钟以前";
            }
            if (s < 72000)
            {
                return $"{s / 3600}小时以前";
            }
            if (s < 2160000)
            {
                return $"{s / 72000}天以前";
            }
            if (s < 25920000)
            {
                return $"{s / 2160000}月以前";
            }

            return $"{datetime:yyyy-MM-dd HH:mm:ss}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T DeserializeJson<T>(this string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return default(T);
            }
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T DeepClone<T>(this T obj)
        {
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                ms.Seek(0, SeekOrigin.Begin);
                var retval = bf.Deserialize(ms);
                ms.Close();
                return (T)retval;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Uri GetXUri(this HttpRequestBase request)
        {
            var host = request.Headers["x-host"];
            var rawUri = request.Url;
            if (!string.IsNullOrEmpty(host))
            {
                var port = request.Headers["x-port"].ToInt();
                var domain = $"{host}{(port == 80 || port == 443 ? "" : $":{port}")}";
                var url = $"{rawUri.Scheme}://{domain}{request.Url.PathAndQuery}";
                rawUri = new Uri(url);
            }

            return rawUri;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Uri GetXUri(this HttpRequest request)
        {
            var host = request.Headers["x-host"];
            var rawUri = request.Url;
            if (!string.IsNullOrEmpty(host))
            {
                var port = request.Headers["x-port"].ToInt();
                var domain = $"{host}{(port == 80 || port == 443 ? "" : $":{port}")}";
                var url = $"{rawUri.Scheme}://{domain}{request.Url.PathAndQuery}";
                rawUri = new Uri(url);
            }

            return rawUri;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetXUserHostAddress(this HttpRequestBase request)
        {
            if (!string.IsNullOrEmpty(request.Headers["x-real-ip"]))
            {
                return request.Headers["x-real-ip"];
            }
            return request.UserHostAddress;
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetFileItemProperty(this string json, string name = "link")
        {
            if (string.IsNullOrEmpty(json)) return null;
            var field = JsonConvert.DeserializeObject<FileItem>(json);
            return GetFileItemProperty(field);
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetFileItemProperty(this FileItem field, string name = "link")
        {
            if (field == null) return null;
            switch (name)
            {
                case "name": return field.name;
                case "link": return field.link;
                case "src": return field.src;
                case "extension": return field.extension;
            }
            return null;
        }

        ///<summary>
        /// 
        ///</summary>
        public static FileItem GetFileItem(this string json, string name = "link")
        {
            return string.IsNullOrEmpty(json) ? null : JsonConvert.DeserializeObject<FileItem>(json);
        }

    }
}
